// pages/sweat_record/sweat_record.js
var util = require('../../utils/util.js')
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hidden1: true,
    hidden2: false,
    hidden3: true,
    hidden4: true,
    hidden5: true,
    hidden6: true,
    currentUserInfo: ""
  },

  /**
   * 生命周期函数--监听页面加载
   * 生成画布
   */
  onLoad: function(options) {
    this.load();
    wx.setNavigationBarTitle({
      title: '我的汗水记录',
    })
  },
  canvasIdErrorCallback: function(e) {
    console.error(e.detail.errMsg)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  /**
   * 保存图片
   */
  sweat_save_button: function() {
    console.log("three")
    var that = this
    wx.canvasToTempFilePath({
      x: 0,
      y: 0,
      width: 400,
      height: 480,
      destWidth: 400,
      destHeight: 480,
      canvasId: 'shareCanvas',
      success: function(res) {
        console.log(res.tempFilePath);
        that.setData({
          prurl: res.tempFilePath,
          hidden: false
        })
      },
      fail: function(res) {
        console.log(res)
      }
    })
    var that = this
    wx.saveImageToPhotosAlbum({
      filePath: that.data.prurl,
      success(res) {
        wx.showModal({
          content: '图片已保存到相册，赶紧晒一下吧~',
          showCancel: false,
          confirmText: '好的',
          confirmColor: '#333',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定');
              that.setData({
                hidden: true
              })
            }
          }
        })
      }
    })
  },

  load: function() {
    let i = 1
    let promise1
    promise1 = new Promise(function(resolve, reject) {
      wx.getImageInfo({
        src: 'http://' + app.globalData.IP + ':9588/download/sport1.jpg',
        success: function(res) {
          console.log(res)
          resolve(res);
        }
      })
    })
    Promise.all(
      [promise1]
    ).then(res => {
      const ctx = wx.createCanvasContext('shareCanvas')
      ctx.drawImage(res[0].path, 0, 0, 380, 516)
      ctx.setTextAlign('center')
      ctx.setFillStyle('purple')
      ctx.setFontSize(15)
      wx.getUserInfo({
        success: function(res) {
          console.log(res)
          var nickname = res.userInfo.nickName
          var avatarUrl = res.userInfo.avatarUrl
          ctx.fillText(nickname, 55, 420)
          var time = util.formatTime(new Date())
          ctx.fillText(time, 280, 50)
          var step = wx.getStorageSync("steplist")+"步"
          ctx.fillText(step, 280, 80)
          let promise2 = new Promise(function(resolve, reject) {
            wx.getImageInfo({
              src: avatarUrl,
              success: function(res) {
                console.log(res)
                resolve(res)
              }
            })
          })
          Promise.all([promise2]).then(res => {
            var avatarurl_width = 50;    //绘制的头像宽度
            var avatarurl_heigth = 50;   //绘制的头像高度
            var avatarurl_x = 30;   //绘制的头像在画布上的位置
            var avatarurl_y = 350;   //绘制的头像在画布上的位置
            ctx.arc(avatarurl_width / 2 + avatarurl_x, avatarurl_heigth / 2 + avatarurl_y, avatarurl_width / 2, 0, Math.PI * 2, false);
            ctx.clip();
            ctx.drawImage(res[0].path, avatarurl_x, avatarurl_y, avatarurl_width, avatarurl_heigth)
            ctx.restore();
            ctx.stroke()
            console.log("two")
            ctx.draw()
          })
        }
      })
    },
  )},
  change: function(event) {
    if (this.data.hidden1 == false) {
      this.setData({
        hidden1: true,
        hidden2: false,
        hidden3: true,
        hidden4: true,
        hidden5: true,
        hidden6: true,
      })
    } else if (this.data.hidden2 == false) {
      this.setData({
        hidden1: true,
        hidden2: true,
        hidden3: false,
        hidden4: true,
        hidden5: true,
        hidden6: true,
      })
    } else if (this.data.hidden3 == false) {
      this.setData({
        hidden1: true,
        hidden2: true,
        hidden3: true,
        hidden4: false,
        hidden5: true,
        hidden6: true,
      })
    } else if (this.data.hidden4 == false) {
      this.setData({
        hidden1: true,
        hidden2: true,
        hidden3: true,
        hidden4: true,
        hidden5: false,
        hidden6: true,
      })
    } else if (this.data.hidden5 == false) {
      this.setData({
        hidden1: true,
        hidden2: true,
        hidden3: true,
        hidden4: true,
        hidden5: true,
        hidden6: false,
      })
    } else if (this.data.hidden6 == false) {
      this.setData({
        hidden1: false,
        hidden2: true,
        hidden3: true,
        hidden4: true,
        hidden5: true,
        hidden6: true,
      })
    }
    console.log(event)
    let i = event.currentTarget.id
    console.log(i)
    let promise1
    if (i == 1) {
      promise1 = new Promise(function(resolve, reject) {
        wx.getImageInfo({
          src: 'http://' + app.globalData.IP + ':9588/download/sport1.jpg',
          success: function(res) {
            console.log(res)
            resolve(res);
          }
        })
      })
    } else if (i == 2) {
      promise1 = new Promise(function(resolve, reject) {
        wx.getImageInfo({
          src: 'http://' + app.globalData.IP + ':9588/download/sport2.jpg',
          success: function(res) {
            console.log(res)
            resolve(res);
          }
        })
      })
    } else if (i == 3) {
      promise1 = new Promise(function(resolve, reject) {
        wx.getImageInfo({
          src: 'http://' + app.globalData.IP + ':9588/download/sport3.jpg',
          success: function(res) {
            console.log(res)
            resolve(res);
          }
        })
      })
    } else if (i == 4) {
      promise1 = new Promise(function(resolve, reject) {
        wx.getImageInfo({
          src: 'http://' + app.globalData.IP + ':9588/download/sport4.jpg',
          success: function(res) {
            console.log(res)
            resolve(res);
          }
        })
      })
    } else if (i == 5) {
      promise1 = new Promise(function(resolve, reject) {
        wx.getImageInfo({
          src: 'http://' + app.globalData.IP + ':9588/download/sport5.jpg',
          success: function(res) {
            console.log(res)
            resolve(res);
          }
        })
      })
    } else if (i == 6) {
      promise1 = new Promise(function(resolve, reject) {
        wx.getImageInfo({
          src: 'http://' + app.globalData.IP + ':9588/download/sport6.jpg',
          success: function(res) {
            console.log(res)
            resolve(res);
          }
        })
      })
    }
    Promise.all(
      [promise1]
    ).then(res => {
      const ctx = wx.createCanvasContext('shareCanvas')
      ctx.drawImage(res[0].path, 0, 0, 380, 516)
      ctx.setTextAlign('center')
      ctx.setFillStyle('purple')
      ctx.setFontSize(15)
      wx.getUserInfo({
        success: function (res) {
          console.log(res)
          var nickname = res.userInfo.nickName
          var avatarUrl = res.userInfo.avatarUrl
          ctx.fillText(nickname, 55, 420)
          var time = util.formatTime(new Date())
          ctx.fillText(time, 280, 50)
          var step = wx.getStorageSync("steplist") + "步"
          ctx.fillText(step, 280, 80)
          let promise2 = new Promise(function (resolve, reject) {
            wx.getImageInfo({
              src: avatarUrl,
              success: function (res) {
                console.log(res)
                resolve(res)
              }
            })
          })
          Promise.all([promise2]).then(res => {
            var avatarurl_width = 50;    //绘制的头像宽度
            var avatarurl_heigth = 50;   //绘制的头像高度
            var avatarurl_x = 30;   //绘制的头像在画布上的位置
            var avatarurl_y = 350;   //绘制的头像在画布上的位置
            ctx.arc(avatarurl_width / 2 + avatarurl_x, avatarurl_heigth / 2 + avatarurl_y, avatarurl_width / 2, 0, Math.PI * 2, false);
            ctx.clip();
            ctx.drawImage(res[0].path, avatarurl_x, avatarurl_y, avatarurl_width, avatarurl_heigth)
            ctx.restore();
            ctx.stroke()
            console.log("two")
            ctx.draw()
          })
        }
      })
    })
  }
})