var utils = require('../../utils/util.js')
var app =getApp();
// pages/activity/activity_reward.js
Page({
    onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
      this.showBalance()
    wx.hideNavigationBarLoading() //完成停止加载
    wx.stopPullDownRefresh() //停止下拉刷新
    },
  /**
   * 页面的初始数据
   */
  data: {
    surplus:"",
    balance: '',
    limit_text:'少侠，何必沉迷，时光大好出门走走',
    productmap:[]
  },
  show: function() {
    if (this.data.surplus == 0) {
      wx.showToast({
        title: '次数不足',
        icon: "none",
      })
    } else {
      var t = this;
      wx.request({
        url: 'http://' + app.globalData.IP+':9588/wx/modifyBalance',
        data: {
          openId: wx.getStorageSync('openId'),
          balance: 10,
          algorithm: '-'
        },
        method: 'GET',
        success: function(res) {
          if (!res.data) {
            wx.showToast({
              title: '余额不足',
              icon: 'none'
            })
          } else {
            wx.request({
              url: 'http://' + app.globalData.IP+':9588/wx/openBox',
              success: function(data) {
                t.setData({
                  openBox: data.data
                })
                wx.showModal({
                  title: '燃力宝箱',
                  content: '恭喜获得' + data.data + '燃力币',
                  showCancel: false,
                  success: function(res) {
                    wx.request({
                      url: 'http://' + app.globalData.IP+':9588/wx/modifyBalance',
                      data: {
                        openId: wx.getStorageSync('openId'),
                        balance: data.data,
                        algorithm: '+'
                      },
                      method: 'GET',
                      success: function(res) {
                        if (res.data) {
                          t.setData({
                            surplus: --t.data.surplus
                          })
                          t.showBalance()
                        }
                      }
                    })
                  }
                })
              }
            })
          }
        }
      });
    }

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.showBalance(); 
    var t = this;
     wx.request({
       url: 'http://' + app.globalData.IP+':9588/wx/selectAll',
       data:{
         openId:wx.getStorageSync('openId'),
       },
       success: function (data) {
         t.setData({
           productmap: data.data.inviteuser,
           surplus: 10 - data.data.count
            
         })
         console.log(data.data.inviteuser)
         console.log(data.data.count);
       }
     })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  showBalance:function(){
    var t = this;
    wx.request({
      url: 'http://' + app.globalData.IP+':9588/wx/getUserInfo',
      data: {
        openId: wx.getStorageSync('openId')
      },
      success: function (data) {
        console.log(data)
        t.setData({
          balance: data.data.balance
        })
      }
    })

  }
})