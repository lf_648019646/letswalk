// pages/addr_set/addr_set.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
     name: "zhangsan",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  searchBox: function (e) {

    const that = this
    let address;
    that.setData({
    })
    console.log(e.detail.value.addr);
    wx.request({
      url: 'http://' + app.globalData.IP +'/wx/updateStatus',
      data: { //发送给后台的数据
        openId: wx.getStorageSync("openId"),
        status: "1",
        address: e.detail.value.addr
      },
      success: function () {
        wx.navigateTo({
          //url: '../addr/addr_adminstartion',
          url: '../addr_model/addr_model?',
        })
      }
    })
  }
})