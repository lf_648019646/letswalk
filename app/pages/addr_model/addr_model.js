// pages/addr_model/addr_model.js
var util = require('../../utils/util.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
   list: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  onLoad: function (e) {
    var that = this
    wx.request({
      url: 'http://'+ app.globalData.IP+':9588/wx/selectAddress',
      data: { //发送给后台的数据
        openId: wx.getStorageSync("openId"),
      },
      headers: {
        'Content-Type': 'application/json'
      },
      success: function (res) {
        console.log(res.data);
        console.log(res.data.length);
        for(var i=0; i<res.data.length;i++){
          let time = util.formatTime(new Date(res.data[i].createTime));
          res.data[i].createTime=time;
        } 
        that.setData({
          list: res.data,
          })
        wx.setStorageSync("add", res.data);
      }
      
    })
  

    // this.tager()



  },
  status_set:function(e){
    console.log(e.currentTarget.dataset.sta)
    console.log(123213124124)
    console.log(wx.getStorageSync("add")[0].status)
    console.log(wx.getStorageSync("add"))
    if (e.currentTarget.dataset.staut==0){
      wx.showToast({
        title: '当前地址已经是默认地址',
        icon: "none",
      })
      console.log("1231231")
    }else{
    wx.request({
      url: 'http://' + app.globalData.IP +':9588/wx/updateStatus',
      data: { //发送给后台的数据
        status:e.currentTarget.dataset.sta,
        openId: wx.getStorageSync("openId"),
      },
      headers: {
        'Content-Type': 'application/json'
      },
      success: function (res) {
        wx.redirectTo({
          //url: '../addr/addr_adminstartion',
          url: '../addr_model/addr_model',
        })
      
      }

    })
  }
  },
  del:function(e){
    wx.showModal({
      title: '是否确认删除',
      content: '确认吗',
      success:function(res){
        if(res.confirm){
          wx.request({
            url: 'http://' + app.globalData.IP +':9588/wx/delectAddress',
            data: { //发送给后台的数据
              id: e.currentTarget.dataset.sta,
            },
            headers: {
              'Content-Type': 'application/json'
            },
            success: function (res) {
              wx.redirectTo({
                //url: '../addr/addr_adminstartion',
                url: '../addr_model/addr_model',
              })

            }

          })
        }else if(res.confirm){

        }
      }
    })
  

  },
  update:function(e){
    wx.setStorageSync("addressId", e.currentTarget.dataset.sta)
    wx.redirectTo({
      url: '../addr_set/addr_set',
      //url: '../addr_model/addr_model',
    })

  },

  add: function () {
   
    wx.redirectTo({
      url: '../addr/addr_adminstartion',
     
      
      //url: '../addr_model/addr_model',
    })

  },

// tager:function(){
//   if (wx.getStorageSync("adresslist").length>0){
//     wx.navigateTo({
//       //url: '../addr/addr_adminstartion',
//       url: '../addr_model/addr_model',
//     })
//   } else {
//     wx.navigateTo({
//       url: '../addr/addr_adminstartion',
     
//     })
//    }
// }

})