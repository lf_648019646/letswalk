// pages/index/exchange.js
const util = require('../../utils/util.js')
var app = getApp();
Page({
  
  /**
   * 页面的初始数据
   */
  data: {
    listData: [
        {'userHead':'http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg',
        'userName':'xxx','dateTime': '2018-9-21 11:23'},
        {'userHead': 'http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg',
        'userName': 'aaa', 'dateTime': '2018-9-21 11:23'}
    ],
    imgUrls: [
      // 'http://' + app.globalData.IP+':9588/picture/ca.png',
      // 'http://' + app.globalData.IP+':9588/picture/carousel.jpg',
      // 'http://img06.tooopen.com/images/20160818/tooopen_sy_175866434296.jpg',
      // 'http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg',
      
    ],
    indicatorDots: false,
    autoplay: true,
    interval: 2000,
    duration: 1000,
    balance:"",
    rnum:"",
    nnum:"",
  },
  changeIndicatorDots: function (e) {
    this.setData({
      indicatorDots: !this.data.indicatorDots
    })
  },
  changeAutoplay: function (e) {
    this.setData({
      autoplay: !this.data.autoplay
    })
  },
  intervalChange: function (e) {
    this.setData({
      interval: e.detail.value
    })
  },
  durationChange: function (e) {
    this.setData({
      duration: e.detail.value
    })
  },
 
  


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    var that = this
    wx.request({
      url: 'http://' + app.globalData.IP+':9588/wx/selectinfo',
      data: { //发送给后台的数据
        productNameaa: wx.getStorageSync("productName"),
        openId: wx.getStorageSync('openId'),
      },

      headers: {
        'Content-Type': 'application/json'
      },
      success: function (res) {
        console.log(res.data)
        for (var i = 0; i < res.data.orderinfoAll.length; i++) {
          let time = util.formatTime(new Date(res.data.orderinfoAll[i].createTime));
          res.data.orderinfoAll[i].createTime = time;
        }
        wx.setStorageSync("order", res.data.productInfo)
        console.log(res.data)
        that.setData({
          list: res.data.orderinfoAll,
          product: res.data.productInfo,
          listData: res.data.orderinfoAll,
          imgUrls:res.data.picture,
          balance:res.data.userinfo.balance,
          
        })
        
        if (res.data.userinfo.balance >=res.data.productInfo.productPrice){
          that.setData({
            rnum:"燃力币充足可以兑换"
          })
          
        } else { 
          that.setData({
            nnum: "燃力币不足请继续奔跑"

          })
        }
      }

    });

    wx.setNavigationBarTitle({
      title: '商品详情',
    })


  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享 
   */
  onShareAppMessage: function () {

  },

  addre: function(e){
    wx.showModal({
      title: wx.getStorageSync("order").productName,
      content: '确认要兑换吗？',
      success:function(res){
        if(res.confirm){
          console.log(wx.getStorageSync("order"))
        
           wx.request({
             url: 'http://' + app.globalData.IP+':9588/wx/insertOrder',
             data: { //发送给后台的数据
               openId: wx.getStorageSync("openId"),
               productPrice:wx.getStorageSync("order").productPrice,
               productName: wx.getStorageSync("order").productName,
               productId: wx.getStorageSync("order").id


             },
             headers: {
               'Content-Type': 'application/json'
             },
             success: function (res) {
               
               wx.redirectTo({
                 //url: '../addr/addr_adminstartion',
                 url: '../index/exchange'
              })
               that.setData({
                 title: wx.getStorageSync("order").productName,

               })

            }

           })
        }else{

        }
      }
    })
  }
})