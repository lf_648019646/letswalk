// pages/addr/addr_adminstartion.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // openId:"",
    // address:"",
    // status:""
    result: ''

  },
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  },

  check:function(e){
    

  },
  
  searchBox:function(e){
   
    const that = this
    let address;
  
    console.log(e.detail.value.addr);
    if (e.detail.value.linkman == "") {
      that.setData({
        result: '联系人不能为空'
      })
    }else
    if (e.detail.value.telephone == "") {
      that.setData({
        result: '手机号码不能为空'
      })
    }else
    if (e.detail.value.addr == "") {
      that.setData({
        result: '地址栏不能为空'
      })
    } else if (e.detail.value.linkman != "" && e.detail.value.telephone != ""&&e.detail.value.addr != "") {
      that.setData({
        result: ''
      })

    wx.request({
      url: 'http://' + app.globalData.IP+':9588/wx/instertAddress',
      data: { //发送给后台的数据
        openId: wx.getStorageSync("openId"),
        status: "1",
        address: e.detail.value.addr
      },
      success:function(){
        wx.redirectTo({
          //url: '../addr/addr_adminstartion',
          url: '../addr_model/addr_model?',
        })
      }
    })
    }
  }
})