// pages/record/record.js
var util = require('../../utils/util.js');
var app = getApp();


Page({




  /**
   * 页面的初始数据
   */

  data: {
    items: [{
        "aa": "123",
        "title": "这是标题一"
      },
      {
        "aa": "456",
        "title": "这是标题二"
      }
    ],
      time: util.formatTime(new Date()),
    list: [],
    totalNum:0,
    steplist:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    // that = this;
    // wx.request({
    //   url: 'http://'+ app.globalData.IP+':9588/StepDetails_info',
    //   data: {//发送给后台的数据
    //     id: 1,
    //   },
    //   header: {
    //     "Content-Type": "application/x-www_form-urlencoded"
    //   },

    //   method: "get",
    //   success: function (res) {
    //   console.log(res.data);
    //   }

    // })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  onLoad: function() {
    var that = this
    wx.request({
      url: 'http://' + app.globalData.IP +':9588/wx/StepDetails_info',
      data: { //发送给后台的数据
        openId: wx.getStorageSync("openId"),
      },
      headers: {
        'Content-Type': 'application/json'
      },
      success: function(res) {
        //将获取到的json数据，存在名字叫list的这个数组中

        console.log(res.data.stepDetails);
        let totalNum = 0;
        for (var i = 0; i < res.data.stepDetails.length; i++) {
          let stepNum = res.data.stepDetails[i].stepNum;
          totalNum += stepNum;
          console.log(res.data.stepDetails[i].stepTime);
          let time = util.formatTime(new Date(res.data.stepDetails[i].stepTime));
          console.log(time);
          res.data.stepDetails[i].stepTime = time;
        }
        that.setData({
          list: res.data.stepDetails,
          totalNum: totalNum,
          steplist: wx.getStorageSync("steplist"),
        })
        // console.log(total)
      }
    })


  },


  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  // addre: function(){
  //   wx.navigateTo({
  //     url: '../addr_model/addr_model',
  //   })
  // }
})