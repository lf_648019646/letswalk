
const app = getApp()

Page({
 data: {
    encryptedData: {},
    iv: {},
    sessionkey: {}
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function() {

  },
  getUserInfo: function(e) {
    var that = this
    wx.setStorage({
      key: 'iv',
      data: e.detail.iv,
    })
    wx.setStorage({
      key: 'encryptedData',
      data: e.detail.encryptedData,
    })

    wx.switchTab({
      url: '../index/index',
     })
    }
})