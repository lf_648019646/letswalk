// pages/me/me.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

    currentUserInfo:"",
    ip: app.globalData.IP,
  },
  //地址判断 为空进入 url: '../addr/addr_adminstartion',
  //不为空进入url: '../addr_model/addr_model',
  addr_adminstartion:function(){
    wx.navigateTo({
      //url: '../addr/addr_adminstartion',
       url: '../addr_model/addr_model',
    })
  },
  rule:function(){
    wx.navigateTo({
      url: '../rule/rule',
    })
  },

  faq:function(){

    wx.navigateTo({
      url: '../faq/faq',
    })
  },
  contactus: function(){
     wx.navigateTo({
       url: '../contactus/contactus',
     })
  },
  set:function(){
    wx.navigateTo({
      url: '../set/set',
    })
  },

  record:function(){
wx.navigateTo({
  url: '../record/record',
})
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.checkAuth();
    wx.setNavigationBarTitle({
      title: '我的',
    })


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.checkAuth()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },


  checkAuth: function(){
    //进入页面判断是否授权
    var t = this;
    wx.getSetting({
      success: res => {
        t.setData({
          currentUserInfo: res.authSetting['scope.userInfo']
        })
        if (res.authSetting['scope.userInfo']) {
          //授权后请求获取ID和邀请人
          wx.request({
            url: 'http://' + app.globalData.IP +':9588/wx/getUserInfo',
            data: {
              openId: wx.getStorageSync("openId"),
            },
            success: function (data) {
              console.log(data)
            }

          })
        }
      }
    })
  },
 convertdata: function () {
    wx.navigateTo({
      url: '../convertdata/convertdata',
    })
  },



})