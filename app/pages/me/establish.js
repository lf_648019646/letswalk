// pages/me/establish.js
Page({
 
  /**
   * 页面的初始数据
   */
  data: {
    currentUserInfo: "",
    haswerun: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.checkAuth()
    
  },



  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    //进入页面判断是否授权
    this.checkAuth()

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /*获取授权信息 */
  getAuth: function (res) {
    console.log(res.detail.errMsg == "getUserInfo:ok")
    var t = this;

    if (res.detail.errMsg != "getUserInfo:ok") {
      console.log("获取用户信息失败")
      t.setData({
        currentUserInfo: false
      })
      wx.setStorage({
        key: 'currentUserInfo',
        data: false,
      })

    } else {
      console.log("获取用户信息成功")
      t.setData({
        currentUserInfo: true
      })
      wx.setStorage({
        key: 'currentUserInfo',
        data: true,
      })
    }
  },

  /* 步数授权按钮 */
  authorizationButton: function () {
    wx.getSetting({
      success: function (res) {
        if (!res.authSetting['scope.werun']) {
          //重新授权
          wx.openSetting({
            success: function (res) {
              haswerun: true
            }
          })
        }
        else{
          haswerun: flase
        }
      }
    })
  },


  //进入页面判断是否授权
  checkAuth: function () {
    var t = this;
    wx.getSetting({
      success: res => {
        t.setData({
          currentUserInfo: res.authSetting['scope.userInfo'],
          haswerun: res.authSetting['scope.werun']
        })
      }
    })
  }



})



