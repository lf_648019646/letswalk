// pages/convertdata/convertdata.js
var util = require('../../utils/util.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    listData: [],
    time: util.formatTime(new Date()),
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getconverdata()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getconverdata()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  getconverdata:function(){
    var t=this;
    wx.request({
      url: 'http://' + app.globalData.IP +':9588/wx/getconverdata',
      data:{
        openId:wx.getStorageSync("openId")
      },
      success:function(data){
        console.log(data)
        for (var i = 0; i < data.data.list.length; i++) {
          
          let time = util.formatTime(new Date(data.data.list[i].createTime));
          console.log(time);
          data.data.list[i].createTime = time;
        }
        t.setData({
          listData: data.data.list,
         })
        
      }
    })
  }
})