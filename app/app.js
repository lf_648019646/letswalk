//app.js
App({
  
  globalData: {
    IP: '192.168.3.153'
     //IP: '127.0.0.1'
  },
  
  onLaunch: function () {
    var thispage = this;
    wx.login({
      success: function(res) {
        console.log(thispage.globalData.IP)
        if (res.code) {
          //发起网络请求
          wx.request({
            url: 'http://' + thispage.globalData.IP+':9588/wx/wxlogin',
            data: {
              code: res.code
            },
            success: function (res) {
              wx.setStorage({
                key: 'sessionkey',
                data: res.data.sessionkey,
              });
              wx.setStorage({
                key: 'openId',
                data: res.data.openid,
              })
              var userinfo={
                openId: res.data.openid,
                balance : 0,
                inviterOpenId : "456",
                createTime : new Date
              };
              wx.request({
                method:"POST",
                url: 'http://' + thispage.globalData.IP+':9588/wx/addUser',
                data: userinfo,
                header: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                dataType:"json",
                success:function(data){
                  console.log(data)
                }
              })

            }
          })
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    });
    wx.getSetting({
      success: function(res) {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          wx.reLaunch({
            url: '/pages/index/index',
          })
        } else {
          wx.reLaunch({
            url: '/pages/getuser/getuser',
          })
        }
      }
    })
  },
})