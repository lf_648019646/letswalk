package com.example.letswalk.controller;

import com.example.letswalk.pojo.VirtualCurrency;
import com.example.letswalk.service.VirtualCurrencyService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * 虚拟币的控制层
 */
@Controller
public class VirtualCurrencyController {
 @Autowired
    VirtualCurrencyService virtualCurrencyService;
 @RequestMapping("virtual_currency")
    public  String selecrList(ModelMap modelMap){
     List<VirtualCurrency> list =  new ArrayList();
     list = virtualCurrencyService.selectList();
     modelMap.addAttribute("list",list);
     modelMap.addAttribute("num",list.size());
     modelMap.addAttribute("currectRole",   SecurityUtils.getSubject().getSession().getAttribute("currectRole"));
     return "virtual_currency";
 }
}
