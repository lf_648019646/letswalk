package com.example.letswalk.controller;

import com.example.letswalk.pojo.SysUser;
import com.example.letswalk.service.RoleService;
import com.example.letswalk.service.SysUserRoleService;
import com.example.letswalk.service.SysUserService;
import com.example.letswalk.util.MyDES;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authc.UsernamePasswordToken;
//import javax.mail.Session;
import org.apache.shiro.subject.Subject;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import org.apache.shiro.session.Session;
import org.apache.shiro.authc.AuthenticationException;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * 登录
 */
@Controller
public class LoginController {
    @Autowired
    SysUserService sysUserService;

    @Autowired
    RoleService roleService;



    @RequestMapping("/login")
    /*public String login(String username, String password, HttpServletRequest request) {
        System.out.println(username);
        //调用加密方法
        String pwd = MyDES.encryptBasedDes(password);
        Map<String, Object> map = new HashMap<String, Object>();
        System.out.println(pwd);
        map.put("nickname", username);
        map.put("pswd", pwd);
        SysUser sysUser;
        if (null !=sysUserService.selectByMap(map)){
            sysUser = sysUserService.selectByMap(map);
        }else {
            System.out.println("123124321412412421412");
            return "login";
        }
        if (sysUser.getUsername() != null) {
            if (sysUser.getStatus() != 0) {
                System.out.println(sysUser.getStatus());
                System.out.println("aaa");
                request.getSession().setAttribute("SysUser", sysUser);
                return "index";
            }
        }
        System.out.println("123");
        return "login";
    }*/
   public String login(@ModelAttribute(value="user") SysUser sysUser){
        return "login";
    }

    /**
     * shiro Shiro验证登录
     * @param model
     * @param
     * @param
     * @return
     */
    @RequestMapping("/wwlogin")
    public String wangweilogin(Model model, SysUser user,boolean rememberMe) {


        Subject subject = SecurityUtils.getSubject();
        if (user.getUsername().equals("")||user.getPassword().equals("")) {

            model.addAttribute("error", "账号/密码为空");
            return "login";

        }
        else {
            UsernamePasswordToken token = new UsernamePasswordToken(user.getUsername(), user.getPassword(),rememberMe);

            try {
                SysUser sysUserLogin=sysUserService.getByName(user.getUsername());
                if (sysUserLogin!=null) {
                    if(sysUserLogin.getStatus()==1){
                        subject.login(token);
                        Session session = subject.getSession();
                        session.setAttribute("subject", subject);
                        session.setAttribute("currectuser", user.getUsername());
                        session.setAttribute("currectRole", roleService.listRoles(sysUserLogin));
                        model.addAttribute("currectuser", user.getUsername());

                        return "index";
                    }else{
                        model.addAttribute("error", "账号已被禁用");
                        return "login";
                    }

                }else{
                    model.addAttribute("error", "账号密码错误");
                    return "login";
                }
            } catch (Exception e) {
                 e.printStackTrace();
                model.addAttribute("error", "账号密码错误");
                return "login";
            }

        }
    }

    /**
     * 测试传值
     * @param
     * @return
     */
    @RequestMapping(value = "/add", method= RequestMethod.POST)
    public String save(@ModelAttribute SysUser sysUser,Model model) {
        System.out.println(sysUser);
        return "";
    }



}
