package com.example.letswalk.controller;



import com.example.letswalk.pojo.Role;
import com.example.letswalk.pojo.SysMenu;
import com.example.letswalk.pojo.SysUser;
import com.example.letswalk.service.RoleService;
import com.example.letswalk.service.SysMenuRoleService;
import com.example.letswalk.service.SysMenuService;
import com.example.letswalk.service.SysUserService;
import com.example.letswalk.service.serviceimpl.SysUserServiceImpl;
import com.example.letswalk.util.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//专门用于显示页面的控制器
@Controller
public class PageController {

    @Autowired
    RoleService roleService;
    @Autowired
    SysMenuRoleService sysMenuRoleService;
    @Autowired
    SysMenuService sysMenuService;
    @Autowired
    SysUserService sysUserService;

    /**
     * 跳转到添加管理员页面
     * @return
     */
    @RequestMapping("admin-add")
    public String adminAdd(@ModelAttribute SysUser sysUser, Model model) {
        List<Role> roleList= roleService.list();
        model.addAttribute("roleList", roleList);
        return "admin-add";
    }


    /**
     * 跳转到主页面
     * @return
     */
    @RequestMapping("index")
    public String index() {
        return "index";
    }


    /**
     *跳转到用户列表页面
     * @return
     */
//    @RequestMapping("User")
//    public String user() {
//        return "User";
//    }
    /**
     *跳转到步数明细页面
     * @return
     */
    @RequestMapping("step_details")
    public String stepDetails() {
        return "step_details";
    }

    /**
     *跳转到商品上架页面
     * @return
     */
    @RequestMapping("commodity_shelves")
    public String commodityShelves() {
        return "commodity_shelves";
    }

    /**
     *跳转到商品分类页面
     * @return
     */
//    @RequestMapping("index")
//    public String index() {
//        return "index";
//    }

    /**
     *跳转到推荐商品页面
     * @return
     */
//    @RequestMapping("index")
//    public String index() {
//        return "index";
//    }


    /**
     *跳转到转盘设置页面
     * @return
     */
//    @RequestMapping("index")
//    public String index() {
//        return "index";
//    }

    /**
     *跳转到抽奖记录模块
     * @return
     */
    @RequestMapping("feedback-list")
    public String feedBack() {
        return "feedback-list";
    }

    /**
     *跳转到角色管理页面
     * @return
     */
    @RequestMapping("admin-role")
    public String adminRole() {
        return "admin-role";
    }
    

    /**
     *跳转到增加权限管理页面
     * @return
     */
    @RequestMapping("admin-permission-add")
    public String adminPermissionAdd() {
        return "admin-permission-add";
    }

    /**
     *跳转到登录页面
     * @return
     */
    @RequestMapping("logIn")
    public String login() {
        return "login";
    }


    /**
     *跳转到产品分类页面
     * @return
     */
    @RequestMapping("product_category")
    public String product_category() {
        return "product_category";
    }

    /**
     *跳转到产品列表页面
     * @return
     */
    @RequestMapping("product_category_list")
    public String product_category_list() {
        return "product_category_list";
    }


    /**
     *跳转到货币兑换记录页面
     * @return
     */
//    @RequestMapping("virtual_currency")
//    public String virtualCurrency() {
//        return "virtual_currency";
//    }


    /**
     * 跳转修改用户界面，查询所有权限
     * @return
     */
    @RequestMapping("goto_editrole")
    public String editadminrole(Model model,HttpServletRequest request) {
        Long id=Long.parseLong(request.getParameter("roleId"));
        Role role =roleService.get(id);
        model.addAttribute("role", role);


        List<SysMenu> allMenu = sysMenuService.list();

        model.addAttribute("allmenu",allMenu);

        return "admin-role-edit";
    }



    @RequestMapping("start")
    public String start() {
        return "login";
    }


    /**
     * 跳转到管理员修改页面
     * @param model
     * @param sysUser
     * @return
     */
    @RequestMapping("adminEdit")
    public String adminEdit(Model model, HttpServletRequest request,@ModelAttribute SysUser sysUser){
        List<Role> roleList= roleService.list();
        model.addAttribute("roleList", roleList);
        String currentUsername=sysUser.getUsername();
        sysUser=sysUserService.get(sysUser.getUserId());
        System.out.println(sysUser);
        model.addAttribute("sysUser",sysUser);
        if(currentUsername.equals(sysUser.getUsername())){
            model.addAttribute("currentUsername",currentUsername);
        }
        return "admin-edit";
    }

    /**
     * 权限不足时跳转
     * @return
     */
    @RequestMapping("unauthorized")
    public String noPerms() {
        return "unauthorized";
    }

}
