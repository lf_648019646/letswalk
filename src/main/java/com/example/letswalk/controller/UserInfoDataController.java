package com.example.letswalk.controller;

import com.example.letswalk.pojo.LotteryRecord;
import com.example.letswalk.pojo.Userinfo;
import com.example.letswalk.service.LotteryRecordService;
import com.example.letswalk.service.UserinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * 微信数据控制层
 */
@Controller
@ResponseBody
@RequestMapping("wx")
public class UserInfoDataController {
    @Autowired
    UserinfoService userinfoService;
  @Autowired
    LotteryRecordService lotteryRecordService;

    /**
     * 通过openId获取用户信息
     * @param openId
     * @return
     */
    @RequestMapping("getUserInfo")
    public Userinfo queryUser(String openId){
        if(openId!=null&&openId!=""){
            return userinfoService.selectUserByOpenid(openId);
        }
       return null;
    }

    /**
     * 添加微信用户
     * @param userinfo
     * @return
     */
    @RequestMapping(value = "addUser",method = RequestMethod.POST)
    public boolean addUser( Userinfo userinfo){
        Userinfo checkUser=userinfoService.selectUserByOpenid(userinfo.getOpenId());
        if(checkUser!=null){
            return false;
        }
        Date now=new Date();
        userinfo.setCreateTime(now);
        if(userinfo.getInviterOpenId()==null||userinfo.getInviterOpenId()==""){
            userinfo.setInviterOpenId("无");
        }
        return userinfoService.insetUser(userinfo);
    }

    /**
     * 通过openId修改余额信息
     * 通过algorithm判断算法
     * @param openId
     * @param balance
     * @param algorithm
     * @return
     */
    @RequestMapping("modifyBalance")
    public boolean modifyBalance(String openId,Integer balance,String algorithm){
        Userinfo oldUser=userinfoService.selectUserByOpenid(openId);
        //每次抽奖插入抽奖记录表
        if(balance!=10){
            int uid  = oldUser.getUserId();
            LotteryRecord lotteryRecord = new LotteryRecord();
            lotteryRecord.setUserId(uid);
            lotteryRecord.setWinningNum(balance);
            lotteryRecord.setUserName(openId);
            if(balance==100){
                Byte bye  = 1;
                lotteryRecord.setWinningType(bye);
            }
            if(balance==50){
                Byte Bby2 =2;
                lotteryRecord.setWinningType(Bby2);
            }
            if(balance>=0&&balance<10){
                Byte bye3 =3;
                lotteryRecord.setWinningType(bye3);
            }
            Date date = new Date();
            lotteryRecord.setWinTime(date);
            lotteryRecordService.insertLottery(lotteryRecord);
        }
        Integer oldBalance=oldUser.getBalance();
        if("+".equals(algorithm)){
            oldUser.setBalance(oldBalance+balance);
        }else if ("-".equals(algorithm)){
            if(oldBalance<balance){
                return false;
            }else{
                oldUser.setBalance(oldBalance-balance);
            }
        }
        return userinfoService.updateBalanceByOpenId(oldUser);
    }



}
