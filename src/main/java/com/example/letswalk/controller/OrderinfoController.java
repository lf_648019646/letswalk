package com.example.letswalk.controller;

import com.example.letswalk.pojo.OrderInfo;
import com.example.letswalk.pojo.ProductInfo;
import com.example.letswalk.pojo.StepDetails;
import com.example.letswalk.pojo.Userinfo;
import com.example.letswalk.service.OrderinfoService;
import com.example.letswalk.service.ProductInfoService;
import com.example.letswalk.service.StepDetailsService;
import com.example.letswalk.service.UserinfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 微信前台使用
 *订单信息控制层
 * liuxianlin
 */
@Controller
@ResponseBody
public class OrderinfoController {
    @Autowired
    OrderinfoService orderinfoService;
    OrderInfo orderInfo;
    @Autowired
    StepDetailsService stepDetailsService;
    StepDetails stepDetails;
    @Autowired
    UserinfoService userinfoService;

    @Autowired
    ProductInfoService productInfoService;


    @RequestMapping("wx/StepDetails_info")
    @ResponseBody
    public Map<String, Object> selectInfo(String openId){
        System.out.println(openId);
       int id =  userinfoService.selectuser(openId);
        Map<String, Object> map = new HashMap<>();

        List<StepDetails> list =stepDetailsService.selectinfoUser(id);
        orderInfo=  orderinfoService.selectInfo(id);
        map.put("stepDetails", list);
        System.out.println(list.size());
        map.put("orderInfo", orderInfo);
        System.out.println(id);
        return map;
    }

    @RequestMapping("wx/getconverdata")
    @ResponseBody
    public Map<String, Object> getconverdata(String openId){
        int id= userinfoService.selectuser(openId);
        System.out.println(id);
         Map map = new HashMap();
          List<OrderInfo> list = orderinfoService.getuserconverInfo(id);
        map.put("list",list);
        return map;
    }

    @RequestMapping("wx/insertOrder")
    @ResponseBody
    public String  insertOrd(String openId, int productPrice, String productName,Integer productId
){
        int id= userinfoService.selectuser(openId);
        Date date = new Date();
         orderInfo = new OrderInfo();
         orderInfo.setOrderStatus(0);
         orderInfo.setUserId(id);

//订单号
        Date a = new Date();
        SimpleDateFormat b = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String[] arr = b.format(a).split(" ");
        String[] arr1 = arr[0].split("-");
        String[] arr2 = arr[1].split(":");
        String order = arr1[0]+arr1[1]+arr1[2]+arr2[0]+arr2[1]+arr2[2];
        System.out.println(order);
         orderInfo.setLogisticsNum(order);
         orderInfo.setExchangeQuantity(1);
         orderInfo.setCreateTime(date);
         orderInfo.setProductName(productName);
         orderInfo.setProductPrice(productPrice);
         orderInfo.setProductId(productId);
         orderinfoService.insertOrderfo(orderInfo);
        ProductInfo productInfo= productInfoService.selectProductsInfo(productName).get(0);
         int num =productInfo.getProductSurolus()-1;
        productInfo.setProductSurolus(num);
        productInfoService.updatePro(productInfo);
        System.out.println(productInfo.getProductName());
         Userinfo userinfo = userinfoService.selectUserByOpenid(openId);
        int num2 = userinfo.getBalance()-productPrice;
          userinfo.setBalance(num2);
          userinfoService.updateBalanceByOpenId(userinfo);
        return  "123";
    }

}


