package com.example.letswalk.controller;


import com.example.letswalk.pojo.SysMenu;
import com.example.letswalk.service.SysMenuRoleService;
import com.example.letswalk.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  权限控制层
 *  返回数据
 */
@ResponseBody
@Controller
public class SysMenuEditController {

    @Autowired
    SysMenuService sysMenuService;

    @Autowired
    SysMenuRoleService sysMenuRoleService;



    /**
     * 删除单条权限
     * @param
     * @param menuId
     * @return
     */
    @PostMapping("deleteSysMenu")
    public Map delete(Long menuId) {
        Map map = new HashMap();
        sysMenuService.delete(menuId);
        sysMenuRoleService.deleteByMenuId(Integer.parseInt(menuId+""));

        List<SysMenu> ps = sysMenuService.list();
       map.put("SysMenuSize",ps.size());
        return map;
    }

    @RequestMapping("deleteMoreSysMenu")
    public Map deleteMore(@RequestParam("menuIds[]")Long[] menuIds){
        Map map = new HashMap();
            for(int i=0;i<menuIds.length;i++){
                if(sysMenuService.get(menuIds[i])==null){
                 continue;
                }else{
                    sysMenuService.delete(menuIds[i]);
                    sysMenuRoleService.deleteByMenuId(Integer.parseInt(menuIds[i]+""));
                }
            }
        List<SysMenu> ps = sysMenuService.list();
        map.put("SysMenuSize",ps.size());
        return map;
    }

    /**
     * 添加权限
     * @param sysMenu
     * @return
     */
    @RequestMapping("addSysMenu")
    public Map list(SysMenu sysMenu) {
        System.out.println(sysMenu+"            哇咔咔");
        Map map = new HashMap();
        if(sysMenuService.needInterceptor(sysMenu.getUrl())){
            map.put("code",0);
        }else{
            if(sysMenuService.add(sysMenu)){
                map.put("code",1);
            }else{
                map.put("code",0);
            }
        }
       return map;
    }

    @RequestMapping("queryTheParentDirectory")
    public List<SysMenu> queryTheParentDirectory(Integer type){
            List<SysMenu> list = sysMenuService.listParentDirectory(type);
            if(!list.isEmpty()){
                return list;
            }
            return null;


    }
}
