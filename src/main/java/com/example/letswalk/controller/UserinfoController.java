package com.example.letswalk.controller;

import com.example.letswalk.pojo.Userinfo;
import com.example.letswalk.service.UserinfoService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户信息控制层
 * liuxianlin
 */
@Controller
public class UserinfoController {
    @Autowired
    UserinfoService userinfoService;
    @RequestMapping("User")
    public String selectInfo(ModelMap modelMap){
        List<Userinfo> list = new ArrayList<Userinfo>();
        list = userinfoService.selectUserinfoLits();
        modelMap.addAttribute("list",list);
        modelMap.addAttribute("num",list.size());
        modelMap.addAttribute("currectRole",   SecurityUtils.getSubject().getSession().getAttribute("currectRole"));
       return "User";
    }


}
