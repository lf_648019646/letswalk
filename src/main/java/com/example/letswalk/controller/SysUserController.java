package com.example.letswalk.controller;

import com.example.letswalk.pojo.Role;
import com.example.letswalk.pojo.SysUser;
import com.example.letswalk.service.RoleService;
import com.example.letswalk.service.SysUserRoleService;
import com.example.letswalk.service.SysUserService;
import com.example.letswalk.util.MyDES;
import com.example.letswalk.util.Page;
import freemarker.template.SimpleHash;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 管理员控制层
 */

@Controller
public class SysUserController {

     @Autowired
    SysUserService sysUserService;

    @Autowired
    SysUserRoleService sysUserRoleService;

    @Autowired
    RoleService roleService;

    /**
     * 每页10行显示所有管理员信息
     * @param model
     * @param page
     * @return
     */
    @RequestMapping("SysUser_list")
     public String SysUserList(Model model, Page page){
         PageHelper.offsetPage(page.getStart(),page.getCount());
         List<SysUser> sysUserList = sysUserService.list();
        for(SysUser su:sysUserList){
            su.setRole(roleService.get(su.getDeptId()));
         }
         int total = (int) new PageInfo<>(sysUserList).getTotal();
         page.setTotal(total);
        model.addAttribute("page", page);
        model.addAttribute("userList",sysUserList);
        model.addAttribute("userListSize",sysUserList.size());
        model.addAttribute("currectuser",   SecurityUtils.getSubject().getSession().getAttribute("currectuser"));
        model.addAttribute("currectRole",   SecurityUtils.getSubject().getSession().getAttribute("currectRole"));

        return "admin-list";
     }


    /**
     * 修改管理员信息
     * @param sysUser
     * @param roleIds
     * @return
     */


    @RequestMapping("updateUser")
    public String update(SysUser sysUser,Integer[] roleIds){
        sysUserRoleService.setRoles(sysUser,roleIds);

        String password=sysUser.getPassword();
        //如果在修改的时候没有设置密码，就表示不改动密码
        if(sysUser.getPassword().length()!=0) {
            MyDES des = new MyDES();
            String encodedPassword = des.encryptBasedDes(password);
            sysUser.setPassword(encodedPassword);
        }
        else
            sysUser.setPassword(null);

        sysUserService.update(sysUser);

        return "redirect:listUser";

    }



    /**
     * 修改管理员状态
     * @param request
     * @return
     */
    @RequestMapping("updateStatus")
    public String updateStatus(HttpServletRequest request){
        Long userId=Long.parseLong(request.getParameter("userId"));
        String state = request.getParameter("status");
        Byte status;
        if("已启用".equals(state)){
            status=0;
        }else{
            status=1;
        }
        SysUser sysUser=new SysUser();
        sysUser.setStatus(status);
        sysUser.setUserId(userId);
        sysUserService.updateStatusByUserId(sysUser);
        return "forward:SysUser_list";
    }


}
