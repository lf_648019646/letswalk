package com.example.letswalk.controller;

import com.example.letswalk.pojo.StepDetails;
import com.example.letswalk.service.StepDetailsService;
import com.example.letswalk.service.UserinfoService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.xml.crypto.Data;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * 步数明细控制层
 */

@Controller
public class StepDetailsController {
    @Autowired
    StepDetailsService stepDetailsService;

    @Autowired
    UserinfoService userinfoService;

    @RequestMapping("selectLits")
    public  String  StedDetailsList(ModelMap modelMap){
      List<StepDetails> list= stepDetailsService.selectByPrimary();
       modelMap.addAttribute("list",list );
        modelMap.addAttribute("currectRole",   SecurityUtils.getSubject().getSession().getAttribute("currectRole"));
        return "step_details";
    }

    /**
     * 添加步数兑换记录
     */
    @RequestMapping(value = "wx/addStepRecord",method = RequestMethod.POST)
    @ResponseBody
    public void addStepRecord(StepDetails stepDetails, String openId){
       int userId= userinfoService.selectuser(openId);
       stepDetails.setUserId(userId);
       stepDetailsService.insertStepRecord(stepDetails);
    }

    @ResponseBody
    @RequestMapping(value="wx/stepCheck",method = RequestMethod.POST)
    public Integer checkStep(StepDetails stepDetails, String openId){
        int userId= userinfoService.selectuser(openId);
        stepDetails.setUserId(userId);
        List<StepDetails> list = stepDetailsService.selectStepDetailsNum(stepDetails, LocalDate.now());
        Integer convertibleStepNum=0;
        if(list!=null){
            for (StepDetails step:list) {
                convertibleStepNum+=step.getStepNum();
            }
        }
        return stepDetails.getStepNum()-convertibleStepNum;
    }

}
