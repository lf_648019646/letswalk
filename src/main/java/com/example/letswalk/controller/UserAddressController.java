package com.example.letswalk.controller;

import com.example.letswalk.pojo.UserAddress;
import com.example.letswalk.service.UserAddressService;
import com.example.letswalk.service.UserinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.Date;
import java.util.List;
/**
 * 微信前台使用用户地址控制层
 * liuxianlin
 */
@Controller
public class UserAddressController {
    @Autowired
    UserinfoService userinfoService;
    @Autowired
    UserAddressService userAddressService;
    UserAddress userAddress;
    @RequestMapping("wx/selectAddress")
    @ResponseBody
    //查询用户所有地址
    public List<UserAddress>  selectAddress( String openId){
        int id =  userinfoService.selectuser(openId);
        return userAddressService.selectAll(id);
    }
  //通过Id添加地址
    @RequestMapping("wx/instertAddress")
    @ResponseBody
    public List<UserAddress> insertAddress(String openId,String address, int status){
        int id =  userinfoService.selectuser(openId);
        Date date = new Date();
        userAddress = new UserAddress();
        userAddress.setAddress(address);
        userAddress.setUserId(id);
        userAddress.setStatus(status);
        userAddress.setCreateTime(date);
        userAddressService.insertAddress(userAddress);
        return  userAddressService.selectAll(id);
    }
    //通过id删除用户地址
    @RequestMapping("wx/delectAddress")
    @ResponseBody
  public int delectAddress(Integer id){
//        int uid =  userinfoService.selectuser(openId);
        userAddressService.delectAddress(id);
//        List <UserAddress> list= userAddressService.selectAll(id);
//        if(list!=null) {
//            userAddress = list.get(0);
//            userAddress.setStatus(0);
//                userAddressService.updateAddress(userAddress);
//            }
//        System.out.println(id);
        return  1;

    }


 //通过id修改地址信息
@RequestMapping("wx/updateAddress")
@ResponseBody
public UserAddress  updateAddress( Integer addressId,String address,int status ){
    userAddress = new UserAddress();
    userAddress = userAddressService.selectAddress(addressId);
    userAddress.setAddress(address);
    userAddress.setStatus(status);
    Date date = new Date();
    userAddress.setCreateTime(date);
    userAddressService.updateAddress(userAddress);
    return userAddress;
}

    //通过id修改地址信息
    @RequestMapping("wx/updateStatus")
    @ResponseBody
    public UserAddress  updateStatus( Integer status,String openId ){
        int id =  userinfoService.selectuser(openId);
      List <UserAddress> list = userAddressService.selectAll(id);
      if(list!=null) {
          for (UserAddress user : list) {
              user.setStatus(1);
              userAddressService.updateAddress(user);
          }
      }
//      if(list.size()==1){
//          for (UserAddress user : list) {
//            user.setStatus(0);
//            userAddressService.updateAddress(user);
//        }
   // }
        userAddress = new UserAddress();
        userAddress = userAddressService.selectAddress(status);
        userAddress.setStatus(0);
        userAddressService.updateAddress(userAddress);
        return userAddress;
    }
}