package com.example.letswalk.controller;

import com.example.letswalk.service.UserinfoService;
import com.example.letswalk.util.AesCbcUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@ResponseBody
@RequestMapping("wx")
public class wxuserController {

    @Autowired
    UserinfoService userinfoService;


    @RequestMapping("wxlogin")
    public Map getOpenIdAndKey(@RequestParam(value = "code",required=false) String code){

        String WX_URL = "https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code";
        //这三个参数就是之后要填上自己的值。
        //替换appid，appsecret，和code
        String requestUrl = WX_URL.replace("APPID", "wx62b7ad6dd6cac27e").//填写自己的appid
                replace("SECRET", "33a29dd5f82965d806f26fadc15700e3").replace("JSCODE", code).//填写自己的appsecret，
                replace("authorization_code", "authorization_code");

        //调用get方法发起get请求，并把返回值赋值给returnvalue
        String  returnvalue=GET(requestUrl);
        System.out.println(requestUrl);//打印发起请求的url
        System.out.println(returnvalue);//打印调用GET方法返回值
        //定义一个json对象。
        JSONObject convertvalue=new JSONObject();

        //将得到的字符串转换为json
        convertvalue=(JSONObject) JSON.parse(returnvalue);


        System.out.println("return openid is ："+(String)convertvalue.get("openid")); //打印得到的openid
        System.out.println("return sessionkey is ："+(String)convertvalue.get("session_key"));//打印得到的sessionkey，
        //把openid和sessionkey分别赋值给openid和sessionkey
        String openid=(String) convertvalue.get("openid");
        String sessionkey=(String) convertvalue.get("session_key");//定义两个变量存储得到的openid和session_key.
        Map map = new HashMap();
        map.put("openid",openid);
        map.put("sessionkey",sessionkey);
        return map;//返回openid
    }
    //发起get请求的方法。
     public static String GET(String url) {
        String result = "";
        BufferedReader in = null;
        InputStream is = null;
        InputStreamReader isr = null;
        try {
            URL realUrl = new URL(url);
            URLConnection conn = realUrl.openConnection();
            conn.connect();
            Map<String, List<String>> map = conn.getHeaderFields();
            is = conn.getInputStream();
            isr = new InputStreamReader(is);
            in = new BufferedReader(isr);
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            // 异常记录
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (is != null) {
                    is.close();
                }
                if (isr != null) {
                    isr.close();
                }
            } catch (Exception e2) {
                // 异常记录
            }
        }
        return result;
    }

//获取微信运动的信息
    @ResponseBody
    @RequestMapping("wxgetrundata")
        public  String decrypt(@RequestParam(value = "data",required=false)String data,@RequestParam(value = "key",required=false)String key,@RequestParam(value = "iv",required=false) String iv) throws Exception {
            //        initialize();         //被加密的数据
        return AesCbcUtil.decrypt(data,key,iv);
    }

}
