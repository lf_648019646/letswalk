package com.example.letswalk.controller;

import com.example.letswalk.pojo.LotteryRecord;
import com.example.letswalk.pojo.Role;
import com.example.letswalk.pojo.SysMenu;
import com.example.letswalk.pojo.SysUser;
import com.example.letswalk.service.RoleService;
import com.example.letswalk.service.SysMenuRoleService;
import com.example.letswalk.service.SysMenuService;
import com.example.letswalk.service.SysUserService;
import com.example.letswalk.util.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller

public class RoleController {

    @Autowired
    RoleService roleService;
    @Autowired
    SysMenuRoleService sysMenuRoleService;
    @Autowired
    SysMenuService sysMenuService;

    @Autowired
    SysUserService sysUserService;



    /**
     * 查询所有已有角色
     * @param model
     * @return
     */
    @RequestMapping("listRole")
    public String list(Model model,Page page){
        PageHelper.offsetPage(page.getStart(),page.getCount());
        List<Role> rs= roleService.list();

        int total = (int) new PageInfo<>(rs).getTotal();
        page.setTotal(total);
        model.addAttribute("rs", rs);

        Map<String,List<SysMenu>> role_permissions = new HashMap<>();

        for (Role role : rs) {

            List<SysMenu> ps = sysMenuService.list(role);

//            List<String> name = new ArrayList<>();
//            for (SysMenu sysMenu:ps){
//
//                  name.add(sysMenu.getName()) ;
//            }
            role_permissions.put(role.getRoleName(), ps);
        }


        Set<String> cueeectrole =  roleService.listRoleNames(SecurityUtils.getSubject().getSession().getAttribute("currectuser").toString());


        String currectrole[] = cueeectrole.toArray(new String[0]);



        model.addAttribute("cueeectrole",   currectrole);
        model.addAttribute("currectuser",   SecurityUtils.getSubject().getSession().getAttribute("currectuser"));
        model.addAttribute("role_menu", role_permissions);
        model.addAttribute("page", page);

        return "admin-role";
    }

    /**
     * 修改角色权限
     * @param model
     * @param id
     * @return
     */
    @RequestMapping("/goto_editRole")
    public String list(Model model,Long id,@RequestParam(value = "roleId",required = false) Long ids){
        Role role =roleService.get(ids);


        model.addAttribute("role", role);

        List<SysMenu> allMenu = sysMenuService.list();
        model.addAttribute("allmenu",allMenu);

        List<SysMenu> currentPermissions = sysMenuService.list(role);
        model.addAttribute("currentmenus", currentPermissions);

        return "admin-role-edit";
    }

    /**
     * 查询用户名
     * @param model
     * @param req
     * @return
     */
    @RequestMapping("/checkname")
    @ResponseBody
    public Map list(Model model,HttpServletRequest req){
        String uname = req.getParameter("username");
        System.out.println(uname);
        List<Role> rolelist =  roleService.list();
        List<String> rolename = new ArrayList<>();
        Map map = new HashMap();
        for (Role r:rolelist){
            rolename.add(r.getRoleName());
            }
        for (String name:rolename) {
            if (uname.equals(name)) {
                map.put("code",1);
                break;
            }else{
                map.put("code",2);
            }
        }
        return map;
    }


    /**
     * 修改角色信息
     * @param role
     * @param
     * @return
     */
    @RequestMapping("updateRole")
    public String update(Role role,int[] menu_id){

        role.setCreateTime(new Date());

        roleService.update(role);

        sysMenuRoleService.setSysMenu(role, menu_id);

        return "redirect:listRole";
    }

    /**
     * 跳转新增用户界面，查询所有权限
     * @return
     */
    @RequestMapping("adminrole-add")
    public String addadminrole(Model model) {
        List<SysMenu> allMenu = sysMenuService.list();

        model.addAttribute("allmenu",allMenu);

        return "admin-role-add";
    }
    /**
     * 新增角色
     * @param
     * @param role
     * @return
     */
    @RequestMapping("addRole")
    public String addRole(Role role,int[] menu_id){
        System.out.println(menu_id.toString());
        role.setCreateTime(new Date());
        roleService.add(role);

       Role lastrole = roleService.list().get( roleService.list().size()-1);

        sysMenuRoleService.setSysMenu(lastrole, menu_id);

        return "redirect:listRole";
    }


    /**
     * 删除角色
     * @param model
     * @param
     * @return
     */
    @RequestMapping("deleteRole")
    public String delete(Model model,HttpServletRequest request){
        Long id=Long.parseLong(request.getParameter("roleId"));
        roleService.delete(id);
        sysMenuRoleService.deleteByRole(id.intValue());
        return "redirect:listRole";
    }
    /**
     * 批量删除角色
     * @param
     * @param
     * @return
     */

    @ResponseBody
    @RequestMapping(value = "/deledata")
    public List<Role> batchShanc(@RequestParam(value = "ids[]",required = false) long ids[], ModelMap modelMap) {


        for (int i = 0; i < ids.length; i++) {
            roleService.delete(ids[i]);

            sysMenuRoleService.deleteByRole(ids[i]);
        }
        List<Role> list = roleService.list();
        modelMap.addAttribute("num",list.size());
        return list;
    }
}
