package com.example.letswalk.controller;

import com.example.letswalk.pojo.LotteryRecord;
import com.example.letswalk.service.LotteryRecordService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 抽奖记录控制层
 * Liuxianlin
 */
@Controller
public class LotteryRecordController {
    @Autowired
    LotteryRecordService lotteryRecordService;
    LotteryRecord lotteryRecord;
//查询所有的记录
    @RequestMapping("LotteryRecord_list")
    public String LotteryRecordList(ModelMap modelMap){
        List<LotteryRecord> list = new ArrayList();
        list =lotteryRecordService.LotteryRecordList();
        modelMap.addAttribute("lsita", list);
        modelMap.addAttribute("num",list.size());
        modelMap.addAttribute("currectRole",   SecurityUtils.getSubject().getSession().getAttribute("currectRole"));
        System.out.println(list.size());

        return  "feedback-list";
    }
//通过id删除记录
    @ResponseBody
    @RequestMapping(value = "/delete/{a}")
    public List DeleteRecord(@PathVariable("a") String id ,ModelMap modelMap){
        System.out.println(id);
        lotteryRecordService.deleteByPrimaryKey(Integer.parseInt(id));
        List<LotteryRecord> list = new ArrayList();
        list =lotteryRecordService.LotteryRecordList();
        int num = list.size();
        modelMap.addAttribute(num);
        return  list;

    }
    @ResponseBody
    @RequestMapping(value = "/deletes")
    public List<LotteryRecord> batchShanc(@RequestParam(value = "ids[]") Integer ids[], ModelMap modelMap) {
        for (int i = 0; i < ids.length; i++) {
            lotteryRecordService.deleteByPrimaryKey(ids[i]);
        }
        List<LotteryRecord> list = new ArrayList<>();
        list =lotteryRecordService.LotteryRecordList();
        modelMap.addAttribute("num",list.size());
        return list;
    }
}
