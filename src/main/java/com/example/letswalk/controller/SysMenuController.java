package com.example.letswalk.controller;

import com.example.letswalk.pojo.SysMenu;
import com.example.letswalk.service.SysMenuService;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
public class SysMenuController {
    @Autowired
    SysMenuService sysMenuService;


    /**
     * 进入权限管理页面
     * @param model
     * @return
     */
    @RequestMapping("admin_permission")
    public String list(Model model) {
        List<SysMenu> ps = sysMenuService.list();
        model.addAttribute("ps", ps);
        model.addAttribute("SysMenuSize",ps.size());
        model.addAttribute("currectuser",   SecurityUtils.getSubject().getSession().getAttribute("currectuser"));
        model.addAttribute("currectRole",   SecurityUtils.getSubject().getSession().getAttribute("currectRole"));
        return "admin-permission";
    }



    @RequestMapping("editSysMenu")
    public String list(Model model, long id) {
        SysMenu permission = sysMenuService.get(id);
        model.addAttribute("SysMenu", permission);
        return "editSysMenu";
    }


    @RequestMapping("updateSysMenu")
    public String update(SysMenu sysMenu) {

        sysMenuService.update(sysMenu);
        return "redirect:listSysMenu";
    }






//    @PostMapping("deleteMoreSysMenu")
//    public String deletes(@RequestParam("ids[]") Long[] ids) {
//        sysMenuService.deletes(ids);
//        return "redirect:admin-permission";
//    }
    /*private Long[] StringToInt(String[] arrs){
        Long[] ints = new Long[arrs.length];
        for(int i=0;i<arrs.length;i++){
            ints[i] = Long.parseLong(arrs[i]);
        }
        return ints;
    }*/
}
