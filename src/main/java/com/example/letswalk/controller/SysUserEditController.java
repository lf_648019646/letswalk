package com.example.letswalk.controller;


import com.example.letswalk.pojo.SysUser;
import com.example.letswalk.service.RoleService;
import com.example.letswalk.service.SysUserService;
import com.example.letswalk.util.MyDES;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 *  管理员控制层
 *  返回数据
 */
@ResponseBody
@Controller
public class SysUserEditController {


    @Autowired
    RoleService roleService;

    @Autowired
    SysUserService sysUserService;
    /**
     * 新增管理员
     * @param model
     * @return
     */
    @RequestMapping("addUser")
    public Map add(Model model, SysUser sysUser){
        System.out.println(sysUser);
        Map map = new HashMap();
        MyDES des = new MyDES();
        String encodedPassword = des.encryptBasedDes(sysUser.getPassword());
        sysUser.setPassword(encodedPassword);
        Calendar now = Calendar.getInstance();
        sysUser.setCreateTime(now.getTime());
        String result=sysUserService.add(sysUser);
        if(result.equals("success")){
            map.put("code",1);
        }else{
            map.put("code",0);
        }
        return map;
    }


    /**
     * 修改管理员信息
     * @param model
     * @param
     * @return
     */
    @RequestMapping("updateUserById")
    public Map edit(Model model,SysUser sysUser){
        System.out.println(sysUser);
        SysUser sysUserByQuery = sysUserService.getByName(sysUser.getUsername());
        Map map = new HashMap();
        MyDES des = new MyDES();
        if(sysUserByQuery!=null){//查询是否存在用户名
            if(sysUserByQuery.getUserId()==sysUser.getUserId()){//用户名存在 判断是否为自身原来用户名
                String password=sysUser.getPassword();
                //如果在修改的时候没有设置密码，就表示不改动密码
                if(sysUser.getPassword()!=null&&!sysUser.getPassword().equals("")) {
                    String encodedPassword = des.encryptBasedDes(password);
                    sysUser.setPassword(encodedPassword);
                }else{
                    sysUser.setPassword(sysUserService.get(sysUser.getUserId()).getPassword());
                }

                String result=sysUserService.update(sysUser);
                if(result.equals("success")){
                    map.put("code",1);
                }else{
                    map.put("code",0);
                }
            }else{
                map.put("code",0);
            }
        }else{
            String password=sysUser.getPassword();
            //如果在修改的时候没有设置密码，就表示不改动密码
            if(sysUser.getPassword()!=null&&!sysUser.getPassword().equals("")) {
                String encodedPassword = des.encryptBasedDes(password);
                sysUser.setPassword(encodedPassword);
            }else{
                sysUser.setPassword(sysUserService.get(sysUser.getUserId()).getPassword());
            }
            String result=sysUserService.update(sysUser);
            if(result.equals("success")){
                map.put("code",1);
            }else{
                map.put("code",0);
            }
        }
        return map;
    }

    /**
     * 删除管理员
     * 同时删除 权限对角色关系表 里的数据
     * @param model
     * @param
     * @return
     */
    @RequestMapping("deleteUser")
    public Map delete(Model model, HttpServletRequest request){
        Long id=Long.parseLong(request.getParameter("userId"));
        sysUserService.delete(id);
        Map map = new HashMap();
        List<SysUser> sysUserList = sysUserService.list();
        map.put("uSize",sysUserList.size());
        return map;
    }

    /**
     * 实时查询管理员信息
     * @param model
     * @param request
     * @return
     */
    @RequestMapping("realTimeQuery")
    public List<SysUser> realTimeQuery(Model model,HttpServletRequest request){
        String username=request.getParameter("username");
        String min=request.getParameter("min");
        String max=request.getParameter("max");
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Date datemin=null;
        Date datemax=null;
        if(min!=null&!min.equals("")){
            try{
                datemin =sdf.parse(min);
            }catch(ParseException pe){
                datemin = null;
            }
        }
        if(max!=null&!max.equals("")){
            try{
                datemax =sdf.parse(max);
            }catch(ParseException pe){
                datemax = null;
            }
        }
        List<SysUser> list=sysUserService.selectSysUserByUsernameAndDate(username,datemin,datemax);
        if(list!=null){
             for(SysUser su:list){
            su.setRole(roleService.get(su.getDeptId()));
            }
            return list;
        }
        return null;
    }

    /**
     * 批量删除管理员
     * @param
     * @param
     * @return
     */
    @RequestMapping(value = "deleteMoreUser",method = RequestMethod.POST)
    public Map deleteMore( @RequestParam(value = "beCheckedId[]") Long[] beCheckedId){
        for (int i=0;i<beCheckedId.length;i++){
            sysUserService.delete(beCheckedId[i]);
        }
        Map map = new HashMap();
        List<SysUser> sysUserList = sysUserService.list();
        map.put("uSize",sysUserList.size());
        return map;
    }
}
