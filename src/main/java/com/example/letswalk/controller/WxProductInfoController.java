package com.example.letswalk.controller;

import com.example.letswalk.pojo.LotteryRecord;
import com.example.letswalk.pojo.OrderInfo;
import com.example.letswalk.pojo.ProductInfo;
import com.example.letswalk.pojo.Userinfo;
import com.example.letswalk.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 产品信息控制层
 *liuxianliu
 */
@Controller
@RequestMapping("wx")
//查询商品
public class WxProductInfoController {
    @Autowired
    ProductInfoService productInfoService;
    @Autowired
    OrderinfoService orderinfoService;
    ProductInfo productInfo;
    @Autowired
    UserinfoService userinfoService;
    @Autowired
    LotteryRecordService lotteryRecordService;
    @Autowired
    StepDetailsService stepDetailsService;
    @ResponseBody
    @RequestMapping("selectAll")
    public Map select(String openId){
        List<ProductInfo> list = productInfoService.selectAllProduct();
        Map map =new HashMap();
        List signuser = new ArrayList();
        List newuser = new ArrayList();
        List pushuser = new ArrayList();
        List inviteuser = new ArrayList();


        for (ProductInfo info:list) {
            if ("0".equals(info.getProductRegion())){

                String[] split = info.getProductPicture().split(",");
                info.setProductPicture(split[0]);
                newuser.add(info);


            }
            if ("1".equals(info.getProductRegion())){
                String[] split = info.getProductPicture().split(",");
                info.setProductPicture(split[0]);
                inviteuser.add(info);
            }
            if ("2".equals(info.getProductRegion())){
                String[] split = info.getProductPicture().split(",");
                info.setProductPicture(split[0]);
                signuser.add(info);
            }
            if ("3".equals(info.getProductRegion())){
                String[] split = info.getProductPicture().split(",");
                info.setProductPicture(split[0]);
                pushuser.add(info);
            }
        }
        if(openId!=null&&openId!=""){
            List<LotteryRecord> listli = lotteryRecordService.LotteryRecordListByUserName(openId);
            int count =listli.size();
            map.put("count",count);
        }
        map.put("signuser",signuser);
        map.put("newuser",newuser);
        map.put("pushuser",pushuser);
        map.put("inviteuser",inviteuser);

        return map;
    }

    //打开宝箱
    @RequestMapping("openBox")
    @ResponseBody
    public Integer openBox(){
        List<ProductInfo> productName = productInfoService.selectAllProduct();
        List listname = new ArrayList();
     for (ProductInfo info: productName) {
            listname.add(info.getProductName());
   }
          Random random=new Random();
       //int i=random.nextInt(productName.size());
       // System.out.println(productName.get(i).getProductName());
          int i = random.nextInt(11);
          int j = random.nextInt(11);
            if (i==j && i==10) {
                return 100;
            }else if (i==j && i>8){
                return 50;
            }else if (i==j && i>5){
                return  20;
            }
        return random.nextInt(9)+1;
    }


    //通过id查询
    @RequestMapping("selectinfo")
    @ResponseBody
    //通过商品id查询信息
    public Map selectinfo(String productNameaa,String openId) {
        List<ProductInfo>  list1= productInfoService.selectProductsInfo(productNameaa);
        System.out.println(productNameaa);
       Userinfo userinfo = userinfoService.selectUserByOpenid(openId);
        productInfo= list1.get(0);
       Integer  productId= productInfo.getId();
        System.out.println(productId);
         Map<String,Object> map = new HashMap<>();
        List<OrderInfo> list = orderinfoService.selectAll(productId);
        map.put("productInfo", productInfo);
        map.put("orderinfoAll",list);
        List<Object> list3 =  new ArrayList();
        for (ProductInfo productInfo: list1) {
           String[] split = productInfo.getProductPicture().split(",");
           list3.add(split);
        }
        map.put("picture",list3);
        map.put("userinfo",userinfo);
        return  map;
    }

    //定时步数明细
  @RequestMapping("clerarStepDetalis")
   @ResponseBody
 // @Scheduled(cron = "0/150 * * * * *")
  @Scheduled(cron = "0 0 0 * * ?")
   public  boolean delectAll(){

      LocalDateTime localDateTime =LocalDateTime.now();
      System.out.println("当前时间为:" + localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

     return stepDetailsService.select();
//      return false;
  }
}
