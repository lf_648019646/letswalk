package com.example.letswalk.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 *通过ApplicationContextAware 获取未能注入bean的实例（URLPathMatchingFilter）
 */

@Component
public class SpringContextUtils implements ApplicationContextAware {
    private static ApplicationContext context;
  
    public void setApplicationContext(ApplicationContext context)
            throws BeansException {
        SpringContextUtils.context = context;  
    }  
  
    public static ApplicationContext getContext(){
        return context;  
    }  
}  