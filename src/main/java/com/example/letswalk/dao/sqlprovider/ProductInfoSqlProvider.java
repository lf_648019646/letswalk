package com.example.letswalk.dao.sqlprovider;

import com.example.letswalk.pojo.ProductInfo;
import com.example.letswalk.pojo.example.ProductInfoExample;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;


import java.util.List;
import java.util.Map;

public class ProductInfoSqlProvider {

    public String countByExample(ProductInfoExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("product_info");
        applyWhere(example, false);
        return SQL();
    }

    public String deleteByExample(ProductInfoExample example) {
        BEGIN();
        DELETE_FROM("product_info");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(ProductInfo record) {
        BEGIN();
        INSERT_INTO("product_info");
        
        if (record.getId() != null) {
            VALUES("id", "#{id,jdbcType=INTEGER}");
        }
        
        if (record.getProductName() != null) {
            VALUES("product_name", "#{productName,jdbcType=VARCHAR}");
        }
        
        if (record.getProductPrice() != null) {
            VALUES("product_price", "#{productPrice,jdbcType=INTEGER}");
        }
        
        if (record.getProductDetails() != null) {
            VALUES("product_details", "#{productDetails,jdbcType=VARCHAR}");
        }
        
        if (record.getProductSurolus() != null) {
            VALUES("product_surolus", "#{productSurolus,jdbcType=INTEGER}");
        }
        
        if (record.getProductPicture() != null) {
            VALUES("product_picture", "#{productPicture,jdbcType=VARCHAR}");
        }
        
        if (record.getStatus() != null) {
            VALUES("status", "#{status,jdbcType=TINYINT}");
        }
        
        if (record.getProductRegion() != null) {
            VALUES("product_region", "#{productRegion,jdbcType=VARCHAR}");
        }
        
        if (record.getProductCreateTime() != null) {
            VALUES("product_create_time", "#{productCreateTime,jdbcType=TIMESTAMP}");
        }
        
        return SQL();
    }

    public String selectByExample(ProductInfoExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("id");
        } else {
            SELECT("id");
        }
        SELECT("product_name");
        SELECT("product_price");
        SELECT("product_details");
        SELECT("product_surolus");
        SELECT("product_picture");
        SELECT("status");
        SELECT("product_region");
        SELECT("product_create_time");
        FROM("product_info");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        ProductInfo record = (ProductInfo) parameter.get("record");
        ProductInfoExample example = (ProductInfoExample) parameter.get("example");
        
        BEGIN();
        UPDATE("product_info");
        
        if (record.getId() != null) {
            SET("id = #{record.id,jdbcType=INTEGER}");
        }
        
        if (record.getProductName() != null) {
            SET("product_name = #{record.productName,jdbcType=VARCHAR}");
        }
        
        if (record.getProductPrice() != null) {
            SET("product_price = #{record.productPrice,jdbcType=VARCHAR}");
        }
        
        if (record.getProductDetails() != null) {
            SET("product_details = #{record.productDetails,jdbcType=VARCHAR}");
        }
        
        if (record.getProductSurolus() != null) {
            SET("product_surolus = #{record.productSurolus,jdbcType=INTEGER}");
        }
        
        if (record.getProductPicture() != null) {
            SET("product_picture = #{record.productPicture,jdbcType=VARCHAR}");
        }

        if (record.getStatus() != null) {
            SET("status = #{record.status,jdbcType=TINYINT}");
        }
        
        if (record.getProductRegion() != null) {
            SET("product_region = #{record.productRegion,jdbcType=VARCHAR}");
        }
        
        if (record.getProductCreateTime() != null) {
            SET("product_create_time = #{record.productCreateTime,jdbcType=TIMESTAMP}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("product_info");
        
        SET("id = #{record.id,jdbcType=INTEGER}");
        SET("product_name = #{record.productName,jdbcType=VARCHAR}");
        SET("product_price = #{record.productPrice,jdbcType=INTEGER}");
        SET("product_details = #{record.productDetails,jdbcType=VARCHAR}");
        SET("product_surolus = #{record.productSurolus,jdbcType=INTEGER}");
        SET("product_picture = #{record.productPicture,jdbcType=VARCHAR}");
        SET("status = #{record.status,jdbcType=TINYINT}");
        SET("product_region = #{record.productRegion,jdbcType=VARCHAR}");
        SET("product_create_time = #{record.productCreateTime,jdbcType=TIMESTAMP}");
        
        ProductInfoExample example = (ProductInfoExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByPrimaryKeySelective(ProductInfo record) {
        BEGIN();
        UPDATE("product_info");
        
        if (record.getProductName() != null) {
            SET("product_name = #{productName,jdbcType=VARCHAR}");
        }
        
        if (record.getProductPrice() != null) {
            SET("product_price = #{productPrice,jdbcType=VARCHAR}");
        }
        
        if (record.getProductDetails() != null) {
            SET("product_details = #{productDetails,jdbcType=VARCHAR}");
        }
        
        if (record.getProductSurolus() != null) {
            SET("product_surolus = #{productSurolus,jdbcType=INTEGER}");
        }
        
        if (record.getProductPicture() != null) {
            SET("product_picture = #{productPicture,jdbcType=VARCHAR}");
        }
        
        if (record.getStatus() != null) {
            SET("status = #{status,jdbcType=TINYINT}");
        }
        
        if (record.getProductRegion() != null) {
            SET("product_region = #{productRegion,jdbcType=VARCHAR}");
        }
        
        if (record.getProductCreateTime() != null) {
            SET("product_create_time = #{productCreateTime,jdbcType=TIMESTAMP}");
        }
        
        WHERE("id = #{id,jdbcType=INTEGER}");
        
        return SQL();
    }

    protected void applyWhere(ProductInfoExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<ProductInfoExample.Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            ProductInfoExample.Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<ProductInfoExample.Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    ProductInfoExample.Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}