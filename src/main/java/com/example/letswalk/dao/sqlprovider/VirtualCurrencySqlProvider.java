package com.example.letswalk.dao.sqlprovider;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import com.example.letswalk.pojo.VirtualCurrency;
import com.example.letswalk.pojo.example.VirtualCurrencyExample.Criteria;
import com.example.letswalk.pojo.example.VirtualCurrencyExample.Criterion;
import com.example.letswalk.pojo.example.VirtualCurrencyExample;
import java.util.List;
import java.util.Map;

/**
 * 虚拟币sql语句供应
 */
public class VirtualCurrencySqlProvider {

    public String countByExample(VirtualCurrencyExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("virtual_currency");
        applyWhere(example, false);
        return SQL();
    }

    public String deleteByExample(VirtualCurrencyExample example) {
        BEGIN();
        DELETE_FROM("virtual_currency");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(VirtualCurrency record) {
        BEGIN();
        INSERT_INTO("virtual_currency");
        
        if (record.getVirtualId() != null) {
            VALUES("virtual_id", "#{virtualId,jdbcType=INTEGER}");
        }
        
        if (record.getUserId() != null) {
            VALUES("user_id", "#{userId,jdbcType=INTEGER}");
        }
        
        if (record.getCash() != null) {
            VALUES("cash", "#{cash,jdbcType=INTEGER}");
        }
        
        if (record.getCashTime() != null) {
            VALUES("cash_time", "#{cashTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getCashStep() != null) {
            VALUES("cash_step", "#{cashStep,jdbcType=INTEGER}");
        }
        
        return SQL();
    }

    public String selectByExample(VirtualCurrencyExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("virtual_id");
        } else {
            SELECT("virtual_id");
        }
        SELECT("user_id");
        SELECT("cash");
        SELECT("cash_time");
        SELECT("cash_step");
        FROM("virtual_currency");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        VirtualCurrency record = (VirtualCurrency) parameter.get("record");
        VirtualCurrencyExample example = (VirtualCurrencyExample) parameter.get("example");
        
        BEGIN();
        UPDATE("virtual_currency");
        
        if (record.getVirtualId() != null) {
            SET("virtual_id = #{record.virtualId,jdbcType=INTEGER}");
        }
        
        if (record.getUserId() != null) {
            SET("user_id = #{record.userId,jdbcType=INTEGER}");
        }
        
        if (record.getCash() != null) {
            SET("cash = #{record.cash,jdbcType=INTEGER}");
        }
        
        if (record.getCashTime() != null) {
            SET("cash_time = #{record.cashTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getCashStep() != null) {
            SET("cash_step = #{record.cashStep,jdbcType=INTEGER}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("virtual_currency");
        
        SET("virtual_id = #{record.virtualId,jdbcType=INTEGER}");
        SET("user_id = #{record.userId,jdbcType=INTEGER}");
        SET("cash = #{record.cash,jdbcType=INTEGER}");
        SET("cash_time = #{record.cashTime,jdbcType=TIMESTAMP}");
        SET("cash_step = #{record.cashStep,jdbcType=INTEGER}");
        
        VirtualCurrencyExample example = (VirtualCurrencyExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByPrimaryKeySelective(VirtualCurrency record) {
        BEGIN();
        UPDATE("virtual_currency");
        
        if (record.getUserId() != null) {
            SET("user_id = #{userId,jdbcType=INTEGER}");
        }
        
        if (record.getCash() != null) {
            SET("cash = #{cash,jdbcType=INTEGER}");
        }
        
        if (record.getCashTime() != null) {
            SET("cash_time = #{cashTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getCashStep() != null) {
            SET("cash_step = #{cashStep,jdbcType=INTEGER}");
        }
        
        WHERE("virtual_id = #{virtualId,jdbcType=INTEGER}");
        
        return SQL();
    }

    protected void applyWhere(VirtualCurrencyExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}