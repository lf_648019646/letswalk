package com.example.letswalk.dao.sqlprovider;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import com.example.letswalk.pojo.OrderInfo;
import com.example.letswalk.pojo.example.OrderInfoExample.Criteria;
import com.example.letswalk.pojo.example.OrderInfoExample.Criterion;
import com.example.letswalk.pojo.example.OrderInfoExample;
import java.util.List;
import java.util.Map;

/**
 * 订单信息sql语句供应
 */
public class OrderInfoSqlProvider {

    public String countByExample(OrderInfoExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("order_info");
        applyWhere(example, false);
        return SQL();
    }

    public String deleteByExample(OrderInfoExample example) {
        BEGIN();
        DELETE_FROM("order_info");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(OrderInfo record) {
        BEGIN();
        INSERT_INTO("order_info");
        
        if (record.getOrderId() != null) {
            VALUES("order_id", "#{orderId,jdbcType=INTEGER}");
        }
        
        if (record.getUserId() != null) {
            VALUES("user_id", "#{userId,jdbcType=INTEGER}");
        }
        
        if (record.getProductId() != null) {
            VALUES("product_id", "#{productId,jdbcType=INTEGER}");
        }
        
        if (record.getOrderStatus() != null) {
            VALUES("order_status", "#{orderStatus,jdbcType=INTEGER}");
        }
        
        if (record.getLogisticsNum() != null) {
            VALUES("logistics_num", "#{logisticsNum,jdbcType=VARCHAR}");
        }
        
        if (record.getCreateTime() != null) {
            VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getEndTime() != null) {
            VALUES("end_time", "#{endTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getProductName() != null) {
            VALUES("product_name", "#{productName,jdbcType=VARCHAR}");
        }
        
        if (record.getProductPrice() != null) {
            VALUES("product_price", "#{productPrice,jdbcType=INTEGER}");
        }
        
        if (record.getExchangeQuantity() != null) {
            VALUES("exchange_quantity", "#{exchangeQuantity,jdbcType=INTEGER}");
        }
        
        return SQL();
    }

    public String selectByExample(OrderInfoExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("order_id");
        } else {
            SELECT("order_id");
        }
        SELECT("user_id");
        SELECT("product_id");
        SELECT("order_status");
        SELECT("logistics_num");
        SELECT("create_time");
        SELECT("end_time");
        SELECT("product_name");
        SELECT("product_price");
        SELECT("exchange_quantity");
        FROM("order_info");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        OrderInfo record = (OrderInfo) parameter.get("record");
        OrderInfoExample example = (OrderInfoExample) parameter.get("example");
        
        BEGIN();
        UPDATE("order_info");
        
        if (record.getOrderId() != null) {
            SET("order_id = #{record.orderId,jdbcType=INTEGER}");
        }
        
        if (record.getUserId() != null) {
            SET("user_id = #{record.userId,jdbcType=INTEGER}");
        }
        
        if (record.getProductId() != null) {
            SET("product_id = #{record.productId,jdbcType=INTEGER}");
        }
        
        if (record.getOrderStatus() != null) {
            SET("order_status = #{record.orderStatus,jdbcType=INTEGER}");
        }
        
        if (record.getLogisticsNum() != null) {
            SET("logistics_num = #{record.logisticsNum,jdbcType=VARCHAR}");
        }
        
        if (record.getCreateTime() != null) {
            SET("create_time = #{record.createTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getEndTime() != null) {
            SET("end_time = #{record.endTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getProductName() != null) {
            SET("product_name = #{record.productName,jdbcType=VARCHAR}");
        }
        
        if (record.getProductPrice() != null) {
            SET("product_price = #{record.productPrice,jdbcType=INTEGER}");
        }
        
        if (record.getExchangeQuantity() != null) {
            SET("exchange_quantity = #{record.exchangeQuantity,jdbcType=INTEGER}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("order_info");
        
        SET("order_id = #{record.orderId,jdbcType=INTEGER}");
        SET("user_id = #{record.userId,jdbcType=INTEGER}");
        SET("product_id = #{record.productId,jdbcType=INTEGER}");
        SET("order_status = #{record.orderStatus,jdbcType=INTEGER}");
        SET("logistics_num = #{record.logisticsNum,jdbcType=VARCHAR}");
        SET("create_time = #{record.createTime,jdbcType=TIMESTAMP}");
        SET("end_time = #{record.endTime,jdbcType=TIMESTAMP}");
        SET("product_name = #{record.productName,jdbcType=VARCHAR}");
        SET("product_price = #{record.productPrice,jdbcType=INTEGER}");
        SET("exchange_quantity = #{record.exchangeQuantity,jdbcType=INTEGER}");
        
        OrderInfoExample example = (OrderInfoExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByPrimaryKeySelective(OrderInfo record) {
        BEGIN();
        UPDATE("order_info");
        
        if (record.getUserId() != null) {
            SET("user_id = #{userId,jdbcType=INTEGER}");
        }
        
        if (record.getProductId() != null) {
            SET("product_id = #{productId,jdbcType=INTEGER}");
        }
        
        if (record.getOrderStatus() != null) {
            SET("order_status = #{orderStatus,jdbcType=INTEGER}");
        }
        
        if (record.getLogisticsNum() != null) {
            SET("logistics_num = #{logisticsNum,jdbcType=VARCHAR}");
        }
        
        if (record.getCreateTime() != null) {
            SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getEndTime() != null) {
            SET("end_time = #{endTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getProductName() != null) {
            SET("product_name = #{productName,jdbcType=VARCHAR}");
        }
        
        if (record.getProductPrice() != null) {
            SET("product_price = #{productPrice,jdbcType=INTEGER}");
        }
        
        if (record.getExchangeQuantity() != null) {
            SET("exchange_quantity = #{exchangeQuantity,jdbcType=INTEGER}");
        }
        
        WHERE("order_id = #{orderId,jdbcType=INTEGER}");
        
        return SQL();
    }

    protected void applyWhere(OrderInfoExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}