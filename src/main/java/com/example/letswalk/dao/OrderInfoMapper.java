package com.example.letswalk.dao;

import com.example.letswalk.pojo.*;
import com.example.letswalk.pojo.example.*;
import com.example.letswalk.dao.sqlprovider.*;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

/**
 * 订单详情Dao方法
 */
@Mapper
@Repository
public interface OrderInfoMapper {
    @SelectProvider(type=OrderInfoSqlProvider.class, method="countByExample")
    int countByExample(OrderInfoExample example);

    @DeleteProvider(type=OrderInfoSqlProvider.class, method="deleteByExample")
    int deleteByExample(OrderInfoExample example);

    /**
     * 根据表主键ID
     * @param orderId
     * @return int
     */
    @Delete({
        "delete from order_info",
        "where order_id = #{orderId,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer orderId);

    /**
     * 添加表所有字段
     * @param record
     * @return int
     */
    @Insert({
        "insert into order_info (order_id, user_id, ",
        "product_id, order_status, ",
        "logistics_num, create_time, ",
        "end_time, product_name, ",
        "product_price, exchange_quantity)",
        "values (#{orderId,jdbcType=INTEGER}, #{userId,jdbcType=INTEGER}, ",
        "#{productId,jdbcType=INTEGER}, #{orderStatus,jdbcType=INTEGER}, ",
        "#{logisticsNum,jdbcType=VARCHAR}, #{createTime,jdbcType=TIMESTAMP}, ",
        "#{endTime,jdbcType=TIMESTAMP}, #{productName,jdbcType=VARCHAR}, ",
        "#{productPrice,jdbcType=INTEGER}, #{exchangeQuantity,jdbcType=INTEGER})"
    })
    int insert(OrderInfo record);


    @InsertProvider(type=OrderInfoSqlProvider.class, method="insertSelective")
    int insertSelective(OrderInfo record);

    @SelectProvider(type=OrderInfoSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="order_id", property="orderId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER),
        @Result(column="product_id", property="productId", jdbcType=JdbcType.INTEGER),
        @Result(column="order_status", property="orderStatus", jdbcType=JdbcType.INTEGER),
        @Result(column="logistics_num", property="logisticsNum", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="end_time", property="endTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="product_name", property="productName", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_price", property="productPrice", jdbcType=JdbcType.INTEGER),
        @Result(column="exchange_quantity", property="exchangeQuantity", jdbcType=JdbcType.INTEGER)
    })
    List<OrderInfo> selectByExample(OrderInfoExample example);

    /**
     * 根据表主键ID查询所有字段
     * @param orderId
     * @return OrderInfo
     */
    @Select({
            "select",
            "order_id, user_id, product_id, order_status, logistics_num, create_time, end_time, ",
            "product_name, product_price, exchange_quantity",
            "from order_info",
            "where order_id = #{orderId,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column="order_id", property="orderId", jdbcType=JdbcType.INTEGER, id=true),
            @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER),
            @Result(column="product_id", property="productId", jdbcType=JdbcType.INTEGER),
            @Result(column="order_status", property="orderStatus", jdbcType=JdbcType.INTEGER),
            @Result(column="logistics_num", property="logisticsNum", jdbcType=JdbcType.VARCHAR),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="end_time", property="endTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="product_name", property="productName", jdbcType=JdbcType.VARCHAR),
            @Result(column="product_price", property="productPrice", jdbcType=JdbcType.INTEGER),
            @Result(column="exchange_quantity", property="exchangeQuantity", jdbcType=JdbcType.INTEGER)
    })
    OrderInfo selectByPrimaryKey(Integer orderId);



    /**
     * 根据用户ID查询所有字段
     * @param userId
     * @return List<OrderInfo></OrderInfo>
     */
    @Select({
            "select",
            "order_id, user_id, product_id, order_status, logistics_num, create_time, end_time, ",
            "product_name, product_price, exchange_quantity",
            "from order_info",
            "where user_id = #{userId,jdbcType=INTEGER}"
    })
    @Results({
            @Result(column="order_id", property="orderId", jdbcType=JdbcType.INTEGER, id=true),
            @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER),
            @Result(column="product_id", property="productId", jdbcType=JdbcType.INTEGER),
            @Result(column="order_status", property="orderStatus", jdbcType=JdbcType.INTEGER),
            @Result(column="logistics_num", property="logisticsNum", jdbcType=JdbcType.VARCHAR),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="end_time", property="endTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="product_name", property="productName", jdbcType=JdbcType.VARCHAR),
            @Result(column="product_price", property="productPrice", jdbcType=JdbcType.INTEGER),
            @Result(column="exchange_quantity", property="exchangeQuantity", jdbcType=JdbcType.INTEGER)
    })
    List<OrderInfo> selectByuserId(Integer orderId);


    @UpdateProvider(type=OrderInfoSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") OrderInfo record, @Param("example") OrderInfoExample example);

    @UpdateProvider(type=OrderInfoSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") OrderInfo record, @Param("example") OrderInfoExample example);

    @UpdateProvider(type=OrderInfoSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(OrderInfo record);

    /**
     * 根据表主键ID修改字段value
     * @param record
     * @return
     */
    @Update({
        "update order_info",
        "set user_id = #{userId,jdbcType=INTEGER},",
          "product_id = #{productId,jdbcType=INTEGER},",
          "order_status = #{orderStatus,jdbcType=INTEGER},",
          "logistics_num = #{logisticsNum,jdbcType=VARCHAR},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "end_time = #{endTime,jdbcType=TIMESTAMP},",
          "product_name = #{productName,jdbcType=VARCHAR},",
          "product_price = #{productPrice,jdbcType=INTEGER},",
          "exchange_quantity = #{exchangeQuantity,jdbcType=INTEGER}",
        "where order_id = #{orderId,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(OrderInfo record);



    }