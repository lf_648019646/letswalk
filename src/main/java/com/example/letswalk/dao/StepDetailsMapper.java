package com.example.letswalk.dao;

import com.example.letswalk.dao.sqlprovider.StepDetailsSqlProvider;
import com.example.letswalk.pojo.StepDetails;
import com.example.letswalk.pojo.example.StepDetailsExample;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 步数明细Dao方法
 */
@Mapper
@Repository
public interface StepDetailsMapper {
    @SelectProvider(type=StepDetailsSqlProvider.class, method="countByExample")
    int countByExample(StepDetailsExample example);

    @DeleteProvider(type=StepDetailsSqlProvider.class, method="deleteByExample")
    int deleteByExample(StepDetailsExample example);

    /**
     * 根据主键ID删除信息
     * @param stepId
     * @return int
     */
    @Delete({
        "delete from step_details",
        "where step_id = #{stepId,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer stepId);

    /**
     * 添加信息
     * @param record
     * @return int
     */
    @Insert({
        "insert into step_details (step_id, type, ",
        "step_num, step_time, ",
        "user_id)",
        "values (#{stepId,jdbcType=INTEGER}, #{type,jdbcType=INTEGER}, ",
        "#{stepNum,jdbcType=INTEGER}, #{stepTime,jdbcType=DATE}, ",
        "#{userId,jdbcType=INTEGER})"
    })
    int insert(StepDetails record);

    @InsertProvider(type=StepDetailsSqlProvider.class, method="insertSelective")
    int insertSelective(StepDetails record);

    @SelectProvider(type=StepDetailsSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="step_id", property="stepId", jdbcType= JdbcType.INTEGER, id=true),
        @Result(column="type", property="type", jdbcType= JdbcType.INTEGER),
        @Result(column="step_num", property="stepNum", jdbcType= JdbcType.INTEGER),
        @Result(column="step_time", property="stepTime", jdbcType= JdbcType.DATE),
        @Result(column="user_id", property="userId", jdbcType= JdbcType.INTEGER)
    })
    List<StepDetails> selectByExample(StepDetailsExample example);

    /**
     * 根据主键ID查询信息
     * @param stepId
     * @return StepDetails
     */
    @Select({
        "select",
        "step_id, type, step_num, step_time, user_id",
        "from step_details",
        "where step_id = #{stepId,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="step_id", property="stepId", jdbcType= JdbcType.INTEGER, id=true),
        @Result(column="type", property="type", jdbcType= JdbcType.INTEGER),
        @Result(column="step_num", property="stepNum", jdbcType= JdbcType.INTEGER),
        @Result(column="step_time", property="stepTime", jdbcType= JdbcType.DATE),
        @Result(column="user_id", property="userId", jdbcType= JdbcType.INTEGER)
    })
    StepDetails selectByPrimaryKey(Integer stepId);

    @UpdateProvider(type=StepDetailsSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") StepDetails record, @Param("example") StepDetailsExample example);

    @UpdateProvider(type=StepDetailsSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") StepDetails record, @Param("example") StepDetailsExample example);

    @UpdateProvider(type=StepDetailsSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(StepDetails record);

    /**
     * 根据主键ID修改信息
     * @param record
     * @return
     */
    @Update({
        "update step_details",
        "set type = #{type,jdbcType=INTEGER},",
          "step_num = #{stepNum,jdbcType=INTEGER},",
          "step_time = #{stepTime,jdbcType=DATE},",
          "user_id = #{userId,jdbcType=INTEGER}",
        "where step_id = #{stepId,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(StepDetails record);

    /**
     * 查询步数信息所有信息
     *
     * @return List<StepDetails>
     */
    @Select({
            "select",
            "step_id, type, step_num, step_time, user_id",
            "from step_details",
    })
    @Results({
            @Result(column="step_id", property="stepId", jdbcType= JdbcType.INTEGER, id=true),
            @Result(column="type", property="type", jdbcType= JdbcType.INTEGER),
            @Result(column="step_num", property="stepNum", jdbcType= JdbcType.INTEGER),
            @Result(column="step_time", property="stepTime", jdbcType= JdbcType.DATE),
            @Result(column="user_id", property="userId", jdbcType= JdbcType.INTEGER)
    })
    List<StepDetails> selectByPrimaryKeyList();
//清空表中的数据
    @Delete({
            "delete from step_details"
    })
    boolean delete();
}

