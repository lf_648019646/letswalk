package com.example.letswalk.dao;

import com.example.letswalk.pojo.*;
import com.example.letswalk.pojo.example.*;
import com.example.letswalk.dao.sqlprovider.*;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

/**
 * 部门Dao方法
 */
@Mapper
@Repository
public interface SysDeptMapper {
    @SelectProvider(type=SysDeptSqlProvider.class, method="countByExample")
    int countByExample(SysDeptExample example);

    @DeleteProvider(type=SysDeptSqlProvider.class, method="deleteByExample")
    int deleteByExample(SysDeptExample example);

    /**
     * 根据主键ID删除信息
     * @param deptId
     * @return int
     */
    @Delete({
        "delete from sys_dept",
        "where dept_id = #{deptId,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long deptId);

    /**
     * 添加部门信息
     * @param record
     * @return int
     */
    @Insert({
        "insert into sys_dept (dept_id, parent_id, ",
        "name, oeder_num, ",
        "del_flag)",
        "values (#{deptId,jdbcType=BIGINT}, #{parentId,jdbcType=BIGINT}, ",
        "#{name,jdbcType=VARCHAR}, #{oederNum,jdbcType=INTEGER}, ",
        "#{delFlag,jdbcType=TINYINT})"
    })
    int insert(SysDept record);

    @InsertProvider(type=SysDeptSqlProvider.class, method="insertSelective")
    int insertSelective(SysDept record);

    @SelectProvider(type=SysDeptSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="dept_id", property="deptId", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="parent_id", property="parentId", jdbcType=JdbcType.BIGINT),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="oeder_num", property="oederNum", jdbcType=JdbcType.INTEGER),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    List<SysDept> selectByExample(SysDeptExample example);

    /**
     * 根据主键ID查询信息
     * @param deptId
     * @return SysDept
     */
    @Select({
        "select",
        "dept_id, parent_id, name, oeder_num, del_flag",
        "from sys_dept",
        "where dept_id = #{deptId,jdbcType=BIGINT}"
    })
    @Results({
        @Result(column="dept_id", property="deptId", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="parent_id", property="parentId", jdbcType=JdbcType.BIGINT),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="oeder_num", property="oederNum", jdbcType=JdbcType.INTEGER),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    SysDept selectByPrimaryKey(Long deptId);

    @UpdateProvider(type=SysDeptSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") SysDept record, @Param("example") SysDeptExample example);

    @UpdateProvider(type=SysDeptSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") SysDept record, @Param("example") SysDeptExample example);

    @UpdateProvider(type=SysDeptSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(SysDept record);

    /**
     * 根据主键ID修改信息
     * @param record
     * @return
     */
    @Update({
        "update sys_dept",
        "set parent_id = #{parentId,jdbcType=BIGINT},",
          "name = #{name,jdbcType=VARCHAR},",
          "oeder_num = #{oederNum,jdbcType=INTEGER},",
          "del_flag = #{delFlag,jdbcType=TINYINT}",
        "where dept_id = #{deptId,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(SysDept record);
}