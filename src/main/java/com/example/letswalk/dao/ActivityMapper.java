package com.example.letswalk.dao;

import com.example.letswalk.pojo.*;
import com.example.letswalk.pojo.example.*;
import com.example.letswalk.dao.sqlprovider.*;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

/**
 * 活动DAO层方法
 */
@Mapper
@Repository
public interface ActivityMapper {

    @SelectProvider(type=ActivitySqlProvider.class, method="countByExample")
    int countByExample(ActivityExample example);

    @DeleteProvider(type=ActivitySqlProvider.class, method="deleteByExample")
    int deleteByExample(ActivityExample example);

    /**
     * 通过主键ID删除
     * @param id
     * @return int
     */
    @Delete({
        "delete from activity",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    /**
     * 添加插入活动所有字段
     * @param record
     * @return int
     */
    @Insert({
        "insert into activity (id, begin_time, ",
        "end_time, winning_details, ",
        "create_time)",
        "values (#{id,jdbcType=INTEGER}, #{beginTime,jdbcType=TIMESTAMP}, ",
        "#{endTime,jdbcType=TIMESTAMP}, #{winningDetails,jdbcType=VARCHAR}, ",
        "#{createTime,jdbcType=TIMESTAMP})"
    })
    int insert(Activity record);

    /**
     * 选择性添加
     * @param record
     * @return int
     */
    @InsertProvider(type=ActivitySqlProvider.class, method="insertSelective")
    int insertSelective(Activity record);


    @SelectProvider(type=ActivitySqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="begin_time", property="beginTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="end_time", property="endTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="winning_details", property="winningDetails", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<Activity> selectByExample(ActivityExample example);


    /**
     * 通过主键查询
     * @param id
     * @return Activity
     */
    @Select({
        "select",
        "id, begin_time, end_time, winning_details, create_time",
        "from activity",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="begin_time", property="beginTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="end_time", property="endTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="winning_details", property="winningDetails", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    Activity selectByPrimaryKey(Integer id);

    @UpdateProvider(type=ActivitySqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Activity record, @Param("example") ActivityExample example);

    @UpdateProvider(type=ActivitySqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Activity record, @Param("example") ActivityExample example);

    @UpdateProvider(type=ActivitySqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Activity record);

    /**
     * 选择性修改
     * @param record
     * @return int
     */
    @Update({
        "update activity",
        "set begin_time = #{beginTime,jdbcType=TIMESTAMP},",
          "end_time = #{endTime,jdbcType=TIMESTAMP},",
          "winning_details = #{winningDetails,jdbcType=VARCHAR},",
          "create_time = #{createTime,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Activity record);
}