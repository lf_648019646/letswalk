package com.example.letswalk.dao;

import com.example.letswalk.pojo.*;
import com.example.letswalk.pojo.example.*;
import com.example.letswalk.dao.sqlprovider.*;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

/**
 * 系统用户（管理员）Dao方法
 */
@Mapper
@Repository
public interface SysUserMapper {
    @SelectProvider(type=SysUserSqlProvider.class, method="countByExample")
    int countByExample(SysUserExample example);

    @DeleteProvider(type=SysUserSqlProvider.class, method="deleteByExample")
    int deleteByExample(SysUserExample example);

    /**
     * 根据表ID删除信息
     * @param userId
     * @return int
     */
    @Delete({
            "delete from sys_user",
            "where user_id = #{userId,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long userId);

    /**
     * 添加表信息
     * @param record
     * @return int
     */
    @Insert({
            "insert into sys_user (user_id, username, ",
            "password, email, ",
            "mobile, status, ",
            "dept_id, create_time)",
            "values (#{userId,jdbcType=BIGINT}, #{username,jdbcType=VARCHAR}, ",
            "#{password,jdbcType=VARCHAR}, #{email,jdbcType=VARCHAR}, ",
            "#{mobile,jdbcType=VARCHAR}, #{status,jdbcType=TINYINT}, ",
            "#{deptId,jdbcType=BIGINT}, #{createTime,jdbcType=TIMESTAMP})"
    })
    int insert(SysUser record);

    @InsertProvider(type=SysUserSqlProvider.class, method="insertSelective")
    int insertSelective(SysUser record);

    @SelectProvider(type=SysUserSqlProvider.class, method="selectByExample")
    @Results({
            @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT, id=true),
            @Result(column="username", property="username", jdbcType=JdbcType.VARCHAR),
            @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
            @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
            @Result(column="mobile", property="mobile", jdbcType=JdbcType.VARCHAR),
            @Result(column="status", property="status", jdbcType=JdbcType.TINYINT),
            @Result(column="dept_id", property="deptId", jdbcType=JdbcType.BIGINT),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<SysUser> selectByExample(SysUserExample example);

    /**
     * 根据表ID查询信息
     * @param userId
     * @return SysUser
     */
    @Select({
            "select",
            "user_id, username, password, email, mobile, status, dept_id, create_time",
            "from sys_user",
            "where user_id = #{userId,jdbcType=BIGINT}"
    })
    @Results({
            @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT, id=true),
            @Result(column="username", property="username", jdbcType=JdbcType.VARCHAR),
            @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
            @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
            @Result(column="mobile", property="mobile", jdbcType=JdbcType.VARCHAR),
            @Result(column="status", property="status", jdbcType=JdbcType.TINYINT),
            @Result(column="dept_id", property="deptId", jdbcType=JdbcType.BIGINT),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
    })
    SysUser selectByPrimaryKey(Long userId);

    /**
     * 根据表ID查询role
     * @param userId
     * @return List<Role>
     */
    @Select({
            "select",
            "user_id, username, password, email, mobile, status, dept_id, create_time",
            "from sys_user",
            "where user_id = #{userId,jdbcType=BIGINT}"
    })
    @Results({
            @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT, id=true),
            @Result(column="username", property="username", jdbcType=JdbcType.VARCHAR),
            @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
            @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
            @Result(column="mobile", property="mobile", jdbcType=JdbcType.VARCHAR),
            @Result(column="status", property="status", jdbcType=JdbcType.TINYINT),
            @Result(column="dept_id", property="deptId", jdbcType=JdbcType.BIGINT),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(property="roleList",column="user_id",javaType=List.class,
                    many=@Many(select="com.lsj.test.mybatis.annotation.mapper.SysUserService.selectRoleBySysUsserId"))
    })
    List<Role> selectRoleById(Long userId);


    /**
     * 根据UserName,password查询SysUser
     * @param
     * @return List<Role>
     */
    @Select({
            "select",
            "user_id, username, password, email, mobile, status, dept_id, create_time",
            "from sys_user",
            "where username = #{nickname,jdbcType=BIGINT} and password = #{pswd,jdbcType=BIGINT}"
    })
    @Results({
            @Result(column="user_id", property="userId", jdbcType=JdbcType.BIGINT, id=true),
            @Result(column="username", property="username", jdbcType=JdbcType.VARCHAR),
            @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
            @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
            @Result(column="mobile", property="mobile", jdbcType=JdbcType.VARCHAR),
            @Result(column="status", property="status", jdbcType=JdbcType.TINYINT),
            @Result(column="dept_id", property="deptId", jdbcType=JdbcType.BIGINT),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
    })
    SysUser selectUserByMap(Map<String,Object> map);



    @UpdateProvider(type=SysUserSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") SysUser record, @Param("example") SysUserExample example);

    @UpdateProvider(type=SysUserSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("re  cord") SysUser record, @Param("example") SysUserExample example);

    @UpdateProvider(type=SysUserSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(SysUser record);

    /**
     * 根据表ID修改信息
     * @param record
     * @return int
     */
    @Update({
            "update sys_user",
            "set username = #{username,jdbcType=VARCHAR},",
            "password = #{password,jdbcType=VARCHAR},",
            "email = #{email,jdbcType=VARCHAR},",
            "mobile = #{mobile,jdbcType=VARCHAR},",
            "status = #{status,jdbcType=TINYINT},",
            "dept_id = #{deptId,jdbcType=BIGINT}",
            "where user_id = #{userId,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(SysUser record);


    /**
     * 根据表ID修改状态
     * @param record
     * @return
     */
    @Update({
            "update sys_user",
            "set status = #{status,jdbcType=TINYINT}",
            "where user_id = #{userId,jdbcType=BIGINT}"
    })
    int updateStatusByPrimaryKey(SysUser record);
}