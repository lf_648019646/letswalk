package com.example.letswalk.dao;

import com.example.letswalk.pojo.*;
import com.example.letswalk.pojo.example.*;
import com.example.letswalk.dao.sqlprovider.*;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

/**
 * 签到记录Dao方法
 */
@Mapper
@Repository
public interface SignInRecordMapper {
    @SelectProvider(type=SignInRecordSqlProvider.class, method="countByExample")
    int countByExample(SignInRecordExample example);

    @DeleteProvider(type=SignInRecordSqlProvider.class, method="deleteByExample")
    int deleteByExample(SignInRecordExample example);

    /**
     * 根据主键ID删除信息
     * @param id
     * @return int
     */
    @Delete({
        "delete from sign_in_record",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    /**
     * 添加信息
     * @param record
     * @return int
     */
    @Insert({
        "insert into sign_in_record (id, sign_in_time, ",
        "user_id, sign_in_day)",
        "values (#{id,jdbcType=INTEGER}, #{signInTime,jdbcType=TIMESTAMP}, ",
        "#{userId,jdbcType=INTEGER}, #{signInDay,jdbcType=INTEGER})"
    })
    int insert(SignInRecord record);

    @InsertProvider(type=SignInRecordSqlProvider.class, method="insertSelective")
    int insertSelective(SignInRecord record);

    @SelectProvider(type=SignInRecordSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="sign_in_time", property="signInTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER),
        @Result(column="sign_in_day", property="signInDay", jdbcType=JdbcType.INTEGER)
    })
    List<SignInRecord> selectByExample(SignInRecordExample example);

    /**
     * 根据主键ID查询信息
     * @param id
     * @return SignInRecord
     */
    @Select({
        "select",
        "id, sign_in_time, user_id, sign_in_day",
        "from sign_in_record",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="sign_in_time", property="signInTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER),
        @Result(column="sign_in_day", property="signInDay", jdbcType=JdbcType.INTEGER)
    })
    SignInRecord selectByPrimaryKey(Integer id);

    @UpdateProvider(type=SignInRecordSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") SignInRecord record, @Param("example") SignInRecordExample example);

    @UpdateProvider(type=SignInRecordSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") SignInRecord record, @Param("example") SignInRecordExample example);

    @UpdateProvider(type=SignInRecordSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(SignInRecord record);

    /**
     * 根据主键ID修改信息
     * @param record
     * @return int
     */
    @Update({
        "update sign_in_record",
        "set sign_in_time = #{signInTime,jdbcType=TIMESTAMP},",
          "user_id = #{userId,jdbcType=INTEGER},",
          "sign_in_day = #{signInDay,jdbcType=INTEGER}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(SignInRecord record);
}