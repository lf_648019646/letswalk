package com.example.letswalk.dao;

import com.example.letswalk.pojo.*;
import com.example.letswalk.pojo.example.*;
import com.example.letswalk.dao.sqlprovider.*;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

/**
 * 用户地址Dao方法
 */
@Mapper
@Repository
public interface UserAddressMapper {
    @SelectProvider(type=UserAddressSqlProvider.class, method="countByExample")
    int countByExample(UserAddressExample example);

    @DeleteProvider(type=UserAddressSqlProvider.class, method="deleteByExample")
    int deleteByExample(UserAddressExample example);

    /**
     * 根据表ID删除信息
     * @param addressId
     * @return
     */
    @Delete({
        "delete from user_address",
        "where address_id = #{addressId,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer addressId);

    /**
     * 添加地址
     * @param record
     * @return int
     */
    @Insert({
        "insert into user_address (address_id, address, ",
        "status, user_id, ",
        "create_time)",
        "values (#{addressId,jdbcType=INTEGER}, #{address,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=INTEGER}, #{userId,jdbcType=INTEGER}, ",
        "#{createTime,jdbcType=TIMESTAMP})"
    })
    int insert(UserAddress record);

    @InsertProvider(type=UserAddressSqlProvider.class, method="insertSelective")
    int insertSelective(UserAddress record);

    @SelectProvider(type=UserAddressSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="address_id", property="addressId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="address", property="address", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.INTEGER),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<UserAddress> selectByExample(UserAddressExample example);

    /**
     * 根据表ID查询信息
     * @param addressId
     * @return UserAddress
     */
    @Select({
        "select",
        "address_id, address, status, user_id, create_time",
        "from user_address",
        "where address_id = #{addressId,jdbcType=INTEGER}",
        "ORDER BY status ASC"
    })
    @Results({
        @Result(column="address_id", property="addressId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="address", property="address", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.INTEGER),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    UserAddress selectByPrimaryKey(Integer addressId);

    @UpdateProvider(type=UserAddressSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") UserAddress record, @Param("example") UserAddressExample example);

    @UpdateProvider(type=UserAddressSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") UserAddress record, @Param("example") UserAddressExample example);

    @UpdateProvider(type=UserAddressSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(UserAddress record);

    /**
     * 根据表ID修改信息
     * @param record
     * @return int
     */
    @Update({
        "update user_address",
        "set address = #{address,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=INTEGER},",
          "user_id = #{userId,jdbcType=INTEGER},",
          "create_time = #{createTime,jdbcType=TIMESTAMP}",
        "where address_id = #{addressId,jdbcType=INTEGER}",
            "ORDER BY status ASC"
    })
    int updateByPrimaryKey(UserAddress record);
}