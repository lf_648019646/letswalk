package com.example.letswalk.dao;

import com.example.letswalk.pojo.*;
import com.example.letswalk.pojo.example.*;
import com.example.letswalk.dao.sqlprovider.*;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

/**
 * 虚拟币Dao方法
 */
@Mapper
@Repository
public interface VirtualCurrencyMapper {
    @SelectProvider(type=VirtualCurrencySqlProvider.class, method="countByExample")
    int countByExample(VirtualCurrencyExample example);

    @DeleteProvider(type=VirtualCurrencySqlProvider.class, method="deleteByExample")
    int deleteByExample(VirtualCurrencyExample example);

    /**
     * 根据表ID删除信息
     * @param virtualId
     * @return int
     */
    @Delete({
        "delete from virtual_currency",
        "where virtual_id = #{virtualId,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer virtualId);

    /**
     * 添加虚拟币信息
     * @param record
     * @return int
     */
    @Insert({
        "insert into virtual_currency (virtual_id, user_id, ",
        "cash, cash_time, ",
        "cash_step)",
        "values (#{virtualId,jdbcType=INTEGER}, #{userId,jdbcType=INTEGER}, ",
        "#{cash,jdbcType=INTEGER}, #{cashTime,jdbcType=TIMESTAMP}, ",
        "#{cashStep,jdbcType=INTEGER})"
    })
    int insert(VirtualCurrency record);

    @InsertProvider(type=VirtualCurrencySqlProvider.class, method="insertSelective")
    int insertSelective(VirtualCurrency record);

    @SelectProvider(type=VirtualCurrencySqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="virtual_id", property="virtualId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER),
        @Result(column="cash", property="cash", jdbcType=JdbcType.INTEGER),
        @Result(column="cash_time", property="cashTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="cash_step", property="cashStep", jdbcType=JdbcType.INTEGER)
    })
    List<VirtualCurrency> selectByExample(VirtualCurrencyExample example);

    /**
     * 根据表ID查询信息
     * @param virtualId
     * @return VirtualCurrency
     */
    @Select({
        "select",
        "virtual_id, user_id, cash, cash_time, cash_step",
        "from virtual_currency",
        "where virtual_id = #{virtualId,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="virtual_id", property="virtualId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER),
        @Result(column="cash", property="cash", jdbcType=JdbcType.INTEGER),
        @Result(column="cash_time", property="cashTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="cash_step", property="cashStep", jdbcType=JdbcType.INTEGER)
    })
    VirtualCurrency selectByPrimaryKey(Integer virtualId);

    @UpdateProvider(type=VirtualCurrencySqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") VirtualCurrency record, @Param("example") VirtualCurrencyExample example);

    @UpdateProvider(type=VirtualCurrencySqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") VirtualCurrency record, @Param("example") VirtualCurrencyExample example);

    @UpdateProvider(type=VirtualCurrencySqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(VirtualCurrency record);

    /**
     * 根据表ID修改信息
     * @param record
     * @return int
     */
    @Update({
        "update virtual_currency",
        "set user_id = #{userId,jdbcType=INTEGER},",
          "cash = #{cash,jdbcType=INTEGER},",
          "cash_time = #{cashTime,jdbcType=TIMESTAMP},",
          "cash_step = #{cashStep,jdbcType=INTEGER}",
        "where virtual_id = #{virtualId,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(VirtualCurrency record);

    /**
     * 查询所有的信息
     * @return List<VirtualCurrency>
     */
    @Select({
            "select",
            "virtual_id, user_id, cash, cash_time, cash_step",
            "from virtual_currency"
    })
    @Results({
            @Result(column="virtual_id", property="virtualId", jdbcType=JdbcType.INTEGER, id=true),
            @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER),
            @Result(column="cash", property="cash", jdbcType=JdbcType.INTEGER),
            @Result(column="cash_time", property="cashTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="cash_step", property="cashStep", jdbcType=JdbcType.INTEGER)
    })
    List<VirtualCurrency> selectList();
}