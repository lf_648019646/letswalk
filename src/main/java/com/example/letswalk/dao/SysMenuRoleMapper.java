package com.example.letswalk.dao;


import com.example.letswalk.pojo.*;
import com.example.letswalk.pojo.example.*;
import com.example.letswalk.dao.sqlprovider.*;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

/**
 * 角色目录关系表
 */
@Mapper
@Repository
public interface SysMenuRoleMapper {
    @SelectProvider(type=SysMenuRoleSqlProvider.class, method="countByExample")
    int countByExample(SysMenuRoleExample example);

    @DeleteProvider(type=SysMenuRoleSqlProvider.class, method="deleteByExample")
    int deleteByExample(SysMenuRoleExample example);

    /**
     * 根据(关系表)ID删除信息
     * @param id
     * @return  int
     */
    @Delete({
        "delete from sys_menu_role",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    /**
     * （关系表）添加信息
     * @param record
     * @return int
     */
    @Insert({
        "insert into sys_menu_role (id, menu_id, ",
        "role_id)",
        "values (#{id,jdbcType=INTEGER}, #{menuId,jdbcType=INTEGER}, ",
        "#{roleId,jdbcType=INTEGER})"
    })
    int insert(SysMenuRole record);

    @InsertProvider(type=SysMenuRoleSqlProvider.class, method="insertSelective")
    int insertSelective(SysMenuRole record);


    /**
     * 根据条件查询关系
     * @param example
     * @return
     */
    @SelectProvider(type=SysMenuRoleSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="menu_id", property="menuId", jdbcType=JdbcType.INTEGER),
        @Result(column="role_id", property="roleId", jdbcType=JdbcType.INTEGER)
    })
     List<SysMenuRole> selectByExample(SysMenuRoleExample example);

    /**
     * 根据（关系表）ID查询信息
     * @param id
     * @return SysMenuRole
     */
    @Select({
        "select",
        "id, menu_id, role_id",
        "from sys_menu_role",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="menu_id", property="menuId", jdbcType=JdbcType.INTEGER),
        @Result(column="role_id", property="roleId", jdbcType=JdbcType.INTEGER)
    })
    SysMenuRole selectByPrimaryKey(Integer id);

    @UpdateProvider(type=SysMenuRoleSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") SysMenuRole record, @Param("example") SysMenuRoleExample example);

    @UpdateProvider(type=SysMenuRoleSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") SysMenuRole record, @Param("example") SysMenuRoleExample example);

    @UpdateProvider(type=SysMenuRoleSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(SysMenuRole record);

    /**
     * 根据(关系表）ID修改信息
     * @param record
     * @return int
     */
    @Update({
        "update sys_menu_role",
        "set menu_id = #{menuId,jdbcType=INTEGER},",
          "role_id = #{roleId,jdbcType=INTEGER}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(SysMenuRole record);
}