package com.example.letswalk.dao;


import com.example.letswalk.dao.sqlprovider.ProductInfoSqlProvider;
import com.example.letswalk.pojo.ProductInfo;
import com.example.letswalk.pojo.example.ProductInfoExample;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface ProductInfoMapper {
    @SelectProvider(type=ProductInfoSqlProvider.class, method="countByExample")
    int countByExample(ProductInfoExample example);

    @DeleteProvider(type=ProductInfoSqlProvider.class, method="deleteByExample")
    int deleteByExample(ProductInfoExample example);

    @Delete({
        "delete from product_info",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into product_info (id, product_name, ",
        "product_price, product_details, ",
        "product_surolus, product_picture, ",
        "status, product_region, ",
        "product_create_time)",
        "values (#{id,jdbcType=INTEGER}, #{productName,jdbcType=VARCHAR}, ",
        "#{productPrice,jdbcType=INTEGER}, #{productDetails,jdbcType=VARCHAR}, ",
        "#{productSurolus,jdbcType=INTEGER}, #{productPicture,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT}, #{productRegion,jdbcType=VARCHAR}, ",
        "#{productCreateTime,jdbcType=TIMESTAMP})"
    })
    int insert(ProductInfo record);

    @InsertProvider(type=ProductInfoSqlProvider.class, method="insertSelective")
    int insertSelective(ProductInfo record);

    @SelectProvider(type=ProductInfoSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="product_name", property="productName", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_price", property="productPrice", jdbcType=JdbcType.INTEGER),
        @Result(column="product_details", property="productDetails", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_surolus", property="productSurolus", jdbcType=JdbcType.INTEGER),
        @Result(column="product_picture", property="productPicture", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.TINYINT),
        @Result(column="product_region", property="productRegion", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_create_time", property="productCreateTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<ProductInfo> selectByExample(ProductInfoExample example);

    @Select({
        "select",
        "id, product_name, product_price, product_details, product_surolus, product_picture, ",
        "status, product_region, product_create_time",
        "from product_info",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="product_name", property="productName", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_price", property="productPrice", jdbcType=JdbcType.INTEGER),
        @Result(column="product_details", property="productDetails", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_surolus", property="productSurolus", jdbcType=JdbcType.INTEGER),
        @Result(column="product_picture", property="productPicture", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.TINYINT),
        @Result(column="product_region", property="productRegion", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_create_time", property="productCreateTime", jdbcType=JdbcType.TIMESTAMP)
    })
    ProductInfo selectByPrimaryKey(Integer id);

    @UpdateProvider(type=ProductInfoSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") ProductInfo record, @Param("example") ProductInfoExample example);

    @UpdateProvider(type=ProductInfoSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") ProductInfo record, @Param("example") ProductInfoExample example);

    @UpdateProvider(type=ProductInfoSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(ProductInfo record);

    @Update({
        "update product_info",
        "set product_name = #{productName,jdbcType=VARCHAR},",
          "product_price = #{productPrice,jdbcType=INTEGER},",
          "product_details = #{productDetails,jdbcType=VARCHAR},",
          "product_surolus = #{productSurolus,jdbcType=INTEGER},",
          "product_picture = #{productPicture,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "product_region = #{productRegion,jdbcType=VARCHAR},",
          "product_create_time = #{productCreateTime,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(ProductInfo record);
}