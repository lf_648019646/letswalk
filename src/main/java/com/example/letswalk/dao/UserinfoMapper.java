package com.example.letswalk.dao;

import com.example.letswalk.pojo.*;
import com.example.letswalk.pojo.example.*;
import com.example.letswalk.dao.sqlprovider.*;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

/**
 * 用户信息Dao方法
 */
@Mapper
@Repository
public interface UserinfoMapper {
    @SelectProvider(type=UserinfoSqlProvider.class, method="countByExample")
    int countByExample(UserinfoExample example);

    @DeleteProvider(type=UserinfoSqlProvider.class, method="deleteByExample")
    int deleteByExample(UserinfoExample example);

    /**
     * 根据表ID删除信息
     * @param userId
     * @return int
     */
    @Delete({
        "delete from userinfo",
        "where user_id = #{userId,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer userId);

    /**
     * 添加用户信息
     * @param record
     * @return int
     */
    @Insert({
        "insert into userinfo (user_id, open_id, ",
        "balance, inviter_open_id, ",
        "create_time)",
        "values (#{userId,jdbcType=INTEGER}, #{openId,jdbcType=VARCHAR}, ",
        "#{balance,jdbcType=INTEGER}, #{inviterOpenId,jdbcType=VARCHAR}, ",
        "#{createTime,jdbcType=TIMESTAMP})"
    })
    int insert(Userinfo record);

    @InsertProvider(type=UserinfoSqlProvider.class, method="insertSelective")
    int insertSelective(Userinfo record);

    @SelectProvider(type=UserinfoSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="open_id", property="openId", jdbcType=JdbcType.VARCHAR),
        @Result(column="balance", property="balance", jdbcType=JdbcType.INTEGER),
        @Result(column="inviter_open_id", property="inviterOpenId", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<Userinfo> selectByExample(UserinfoExample example);

    /**
     * 根据表ID查询信息
     * @param userId
     * @return UserInfo
     */
    @Select({
        "select",
        "user_id, open_id, balance, inviter_open_id, create_time",
        "from userinfo",
        "where user_id = #{userId,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="open_id", property="openId", jdbcType=JdbcType.VARCHAR),
        @Result(column="balance", property="balance", jdbcType=JdbcType.INTEGER),
        @Result(column="inviter_open_id", property="inviterOpenId", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    Userinfo selectByPrimaryKey(Integer userId);

    @UpdateProvider(type=UserinfoSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Userinfo record, @Param("example") UserinfoExample example);

    @UpdateProvider(type=UserinfoSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Userinfo record, @Param("example") UserinfoExample example);

    @UpdateProvider(type=UserinfoSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Userinfo record);

    /**
     * 根据表ID修改信息
     * @param record
     * @return int
     */
    @Update({
        "update userinfo",
        "set open_id = #{openId,jdbcType=VARCHAR},",
          "balance = #{balance,jdbcType=INTEGER},",
          "inviter_open_id = #{inviterOpenId,jdbcType=VARCHAR},",
          "create_time = #{createTime,jdbcType=TIMESTAMP}",
        "where user_id = #{userId,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Userinfo record);

    /**
     * 根据表中所有信息

     * @return List<UserInfo>
     */
    @Select({
            "select",
            "user_id, open_id, balance, inviter_open_id, create_time",
            "from userinfo"
    })
    @Results({
            @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER, id=true),
            @Result(column="open_id", property="openId", jdbcType=JdbcType.VARCHAR),
            @Result(column="balance", property="balance", jdbcType=JdbcType.INTEGER),
            @Result(column="inviter_open_id", property="inviterOpenId", jdbcType=JdbcType.VARCHAR),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<Userinfo> selectByPrimaryKeyLits();

    /**
     * 根据表open_id查询user_id
     * @param openId
     * @return int
     */
    @Select({
            "select",
            "user_id",
            "from userinfo",
            "where open_id = #{openId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER, id=true),
    })
   int selectUserId(String openId);


    /**
     * 根据表open_id查询user
     * @param openId
     * @return int
     */
    @Select({
            "select",
            "user_id, open_id, balance, inviter_open_id, create_time",
            "from userinfo",
            "where open_id = #{openId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="user_id", property="userId", jdbcType=JdbcType.INTEGER, id=true),
            @Result(column="open_id", property="openId", jdbcType=JdbcType.VARCHAR),
            @Result(column="balance", property="balance", jdbcType=JdbcType.INTEGER),
            @Result(column="inviter_open_id", property="inviterOpenId", jdbcType=JdbcType.VARCHAR),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    Userinfo selectUserByOpenId(String openId);
}