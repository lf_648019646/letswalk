package com.example.letswalk.dao;

import com.example.letswalk.dao.sqlprovider.LotteryRecordSqlProvider;
import com.example.letswalk.pojo.LotteryRecord;
import com.example.letswalk.pojo.example.LotteryRecordExample;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 中奖记录DAO方法
 */
@Mapper
@Repository
public interface LotteryRecordMapper {
    @SelectProvider(type=LotteryRecordSqlProvider.class, method="countByExample")
    int countByExample(LotteryRecordExample example);

    @DeleteProvider(type=LotteryRecordSqlProvider.class, method="deleteByExample")
    int deleteByExample(LotteryRecordExample example);

    /**
     * 根据抽奖记录表主键ID进行删除
     * @param id
     * @return int
     */
    @Delete({
        "delete from lottery_record",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    /**
     * 添加插入表的所有字段
     * @param record
     * @return int
     */
    @Insert({
        "insert into lottery_record (id, user_id, ",
        "user_name, winning_type, ",
        "winning_num, win_time)",
        "values (#{id,jdbcType=INTEGER}, #{userId,jdbcType=INTEGER}, ",
        "#{userName,jdbcType=VARCHAR}, #{winningType,jdbcType=TINYINT}, ",
        "#{winningNum,jdbcType=INTEGER}, #{winTime,jdbcType=TIMESTAMP})"
    })

    int insert(LotteryRecord record);

    /**
     * 字段选择性添加
     * @param record
     * @return int
     */
    @InsertProvider(type=LotteryRecordSqlProvider.class, method="insertSelective")
    int insertSelective(LotteryRecord record);

    @SelectProvider(type=LotteryRecordSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType= JdbcType.INTEGER, id=true),
        @Result(column="user_id", property="userId", jdbcType= JdbcType.INTEGER),
        @Result(column="user_name", property="userName", jdbcType= JdbcType.VARCHAR),
        @Result(column="winning_type", property="winningType", jdbcType= JdbcType.TINYINT),
        @Result(column="winning_num", property="winningNum", jdbcType= JdbcType.INTEGER),
        @Result(column="win_time", property="winTime", jdbcType= JdbcType.TIMESTAMP)
    })
    List<LotteryRecord> selectByExample(LotteryRecordExample example);

    /**
     * 根据主键ID查询
     * @param id
     * @return
     */
    @Select({
        "select",
        "id, user_id, user_name, winning_type, winning_num, win_time",
        "from lottery_record",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType= JdbcType.INTEGER, id=true),
        @Result(column="user_id", property="userId", jdbcType= JdbcType.INTEGER),
        @Result(column="user_name", property="userName", jdbcType= JdbcType.VARCHAR),
        @Result(column="winning_type", property="winningType", jdbcType= JdbcType.TINYINT),
        @Result(column="winning_num", property="winningNum", jdbcType= JdbcType.INTEGER),
        @Result(column="win_time", property="winTime", jdbcType= JdbcType.TIMESTAMP)
    })
    LotteryRecord selectByPrimaryKey(Integer id);

    @UpdateProvider(type=LotteryRecordSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") LotteryRecord record, @Param("example") LotteryRecordExample example);

    @UpdateProvider(type=LotteryRecordSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") LotteryRecord record, @Param("example") LotteryRecordExample example);

    @UpdateProvider(type=LotteryRecordSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(LotteryRecord record);

    /**
     * 根据表主键ID对字段的修改
     * @param record
     * @return
     */
    @Update({
        "update lottery_record",
        "set user_id = #{userId,jdbcType=INTEGER},",
          "user_name = #{userName,jdbcType=VARCHAR},",
          "winning_type = #{winningType,jdbcType=TINYINT},",
          "winning_num = #{winningNum,jdbcType=INTEGER},",
          "win_time = #{winTime,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(LotteryRecord record);

    /**
     * 查询所有记录
     * @return List<LotteryRecord>
     */
    @Select({
            "select",
            "id, user_id, user_name, winning_type, winning_num, win_time",
            "from lottery_record"
    })
    @Results({
            @Result(column="id", property="id", jdbcType= JdbcType.INTEGER, id=true),
            @Result(column="user_id", property="userId", jdbcType= JdbcType.INTEGER),
            @Result(column="user_name", property="userName", jdbcType= JdbcType.VARCHAR),
            @Result(column="winning_type", property="winningType", jdbcType= JdbcType.TINYINT),
            @Result(column="winning_num", property="winningNum", jdbcType= JdbcType.INTEGER),
            @Result(column="win_time", property="winTime", jdbcType= JdbcType.TIMESTAMP)
    })
    List<LotteryRecord> selectList();
}