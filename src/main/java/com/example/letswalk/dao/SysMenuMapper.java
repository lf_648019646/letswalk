package com.example.letswalk.dao;

import com.example.letswalk.pojo.*;
import com.example.letswalk.pojo.example.*;
import com.example.letswalk.dao.sqlprovider.*;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

/**
 * 菜单Dao方法
 */
@Mapper
@Repository
public interface SysMenuMapper{
    @SelectProvider(type=SysMenuSqlProvider.class, method="countByExample")
    int countByExample(SysMenuExample example);

    @DeleteProvider(type=SysMenuSqlProvider.class, method="deleteByExample")
    int deleteByExample(SysMenuExample example);

    /**
     * 根据表ID删除信息
     * @param menuId
     * @return int
     */
    @Delete({
            "delete from sys_menu",
            "where menu_id = #{menuId,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long menuId);

    /**
     * 添加表信息
     * @param record
     * @return int
     */
    @Insert({
            "insert into sys_menu (menu_id, name, ",
            "url, type, pid)",
            "values (#{menuId,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
            "#{url,jdbcType=VARCHAR}, #{type,jdbcType=INTEGER}, #{pid,jdbcType=INTEGER})"
    })
    int insert(SysMenu record);

    @InsertProvider(type=SysMenuSqlProvider.class, method="insertSelective")
    int insertSelective(SysMenu record);

    @SelectProvider(type=SysMenuSqlProvider.class, method="selectByExample")
    @Results({
            @Result(column="menu_id", property="menuId", jdbcType=JdbcType.BIGINT, id=true),
            @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
            @Result(column="url", property="url", jdbcType=JdbcType.VARCHAR),
            @Result(column="type", property="type", jdbcType=JdbcType.INTEGER),
            @Result(column="pid", property="pid", jdbcType=JdbcType.INTEGER)
    })
    List<SysMenu> selectByExample(SysMenuExample example);

    /**
     * 根据表ID查询信息
     * @param menuId
     * @return SysMenu
     */
    @Select({
            "select",
            "menu_id, name, url, type, pid",
            "from sys_menu",
            "where menu_id = #{menuId,jdbcType=BIGINT}"
    })
    @Results({
            @Result(column="menu_id", property="menuId", jdbcType=JdbcType.BIGINT, id=true),
            @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
            @Result(column="url", property="url", jdbcType=JdbcType.VARCHAR),
            @Result(column="type", property="type", jdbcType=JdbcType.INTEGER),
            @Result(column="pid", property="pid", jdbcType=JdbcType.INTEGER)
    })
    SysMenu selectByPrimaryKey(Long menuId);

    /**
     * 根据UserID查询Menu
     * @param menuId
     * @return List<SysMenu>
     */
    @Select({
            "select * from sys_menu where menu_id in (SELECT menu_id from sys_menu_role where role_id in(SELECT role_id from sys_user_role where user_id= #{UserID,jdbcType=BIGINT}))"

    })
    @Results({
            @Result(column="menu_id", property="menuId", jdbcType=JdbcType.BIGINT, id=true),
            @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
            @Result(column="url", property="url", jdbcType=JdbcType.VARCHAR),
            @Result(column="type", property="type", jdbcType=JdbcType.INTEGER),
            @Result(column="pid", property="pid", jdbcType=JdbcType.INTEGER)
    })
    List<SysMenu> selectByUserID(Long menuId);


    @UpdateProvider(type=SysMenuSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") SysMenu record, @Param("example") SysMenuExample example);

    @UpdateProvider(type=SysMenuSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") SysMenu record, @Param("example") SysMenuExample example);

    @UpdateProvider(type=SysMenuSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(SysMenu record);

    /**
     * 根据表ID修改信息
     * @param record
     * @return int
     */
    @Update({
            "update sys_menu",
            "set name = #{name,jdbcType=VARCHAR},",
            "url = #{url,jdbcType=VARCHAR},",
            "type = #{type,jdbcType=INTEGER},",
            "pid = #{pid,jdbcType=INTEGER}",
            "where menu_id = #{menuId,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(SysMenu record);
}