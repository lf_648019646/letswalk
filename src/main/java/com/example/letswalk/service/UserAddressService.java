package com.example.letswalk.service;

import com.example.letswalk.pojo.UserAddress;
import com.example.letswalk.pojo.example.UserAddressExample;

import java.util.List;

/**
 * 用户地址service层
 */
public interface UserAddressService {

    List<UserAddress> selectAll(Integer id);

    int  insertAddress( UserAddress userAddress);

    int  delectAddress(Integer id);

    int  updateAddress(UserAddress userAddress);

    UserAddress selectAddress(Integer addressId);
}
