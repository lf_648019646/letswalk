package com.example.letswalk.service;

import com.example.letswalk.pojo.Userinfo;

import java.util.List;

/**
 * 用户信息的service
 * liuxianlin
 */

public interface UserinfoService {

    /**
     * 查询所有用户ID
     * @return
     */
    List<Userinfo> selectUserinfoLits();

    /**
     * 通过openID查询用户Id
     * @param openId
     * @return
     */
    int selectuser(String openId);

    /**
     * 添加用户
     * @param userinfo
     * @return
     */
    Boolean insetUser(Userinfo userinfo);

    /**
     * 通过openId修改用户余额
     * @param
     * @return
     */
    Boolean updateBalanceByOpenId(Userinfo userinfo);

    /**
     * 通过openId查询user信息
     * @param openId
     * @return
     */
    Userinfo selectUserByOpenid(String openId);


}
