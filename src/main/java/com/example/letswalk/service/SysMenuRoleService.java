package com.example.letswalk.service;

import com.example.letswalk.pojo.Role;

public interface SysMenuRoleService {
    /***
     * 通过Role,MenuId 设置Menu
     * @param role
     * @param SysMenuIds
     */
    public void setSysMenu(Role role, int[] SysMenuIds);

    /**
     * 通过RoleId删除Menu 删除角色的权限
     * @param roleId
     */
    public void deleteByRole(long roleId);

    /**
     * 通过MenuId删除Role 删除权限被赋予的角色
     * @param SysMenuId
     */
    public void deleteByMenu(long SysMenuId);

    /**
     * 通过MenuId解除Role权限
     * @param menuId
     * @return
     */
    public Boolean deleteByMenuId(Integer menuId);

}
