package com.example.letswalk.service.serviceimpl;

import com.example.letswalk.dao.LotteryRecordMapper;
import com.example.letswalk.pojo.LotteryRecord;
import com.example.letswalk.pojo.example.LotteryRecordExample;
import com.example.letswalk.service.LotteryRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 抽奖记录服务实现类
 * liuxianlin
 */
@Service
public class LotteryRecordServiceImpl implements LotteryRecordService {

    @Autowired
    LotteryRecordMapper lotteryRecordMapper;


    @Override
    public int deleteByPrimaryKey(Integer id) {
       return lotteryRecordMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<LotteryRecord>  LotteryRecordList() {
        return lotteryRecordMapper.selectList();
    }

    @Override
    public List<LotteryRecord> LotteryRecordListByUserName(String openId) {
        LotteryRecordExample example=new LotteryRecordExample();
        example.createCriteria().andUserNameEqualTo(openId);
        return lotteryRecordMapper.selectByExample(example);
    }

    @Override
    public int insertLottery(LotteryRecord lotteryRecord) {
        return  lotteryRecordMapper.insert(lotteryRecord);
    }
}
