package com.example.letswalk.service.serviceimpl;

import com.example.letswalk.dao.SysMenuMapper;
import com.example.letswalk.dao.SysMenuRoleMapper;
import com.example.letswalk.pojo.Role;
import com.example.letswalk.pojo.SysMenu;
import com.example.letswalk.pojo.SysMenuRole;
import com.example.letswalk.pojo.example.SysMenuExample;
import com.example.letswalk.pojo.example.SysMenuRoleExample;
import com.example.letswalk.service.RoleService;
import com.example.letswalk.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.*;
import java.util.List;

@Service
public class SysMenuServiceImpl implements SysMenuService{

    @Autowired
    SysMenuMapper sysMenuMapper;

    @Autowired
    RoleService roleService;

    @Autowired
    SysMenuRoleMapper sysMenuRoleMapper;

    @Autowired
    SysMenuMapper getSysMenuMapper;





    /**
     *获取全部SysMenu的方法
     * */
    @Override
    public List<SysMenu> list() {
        SysMenuExample example =new SysMenuExample();
        example.setOrderByClause("type asc");
        return  sysMenuMapper.selectByExample(example);
    }

    /**
     * 新增SysMenu
     * param:menu
     * */
    @Override
    public Boolean add(SysMenu menu) {
        int i=sysMenuMapper.insert(menu);
        if(i>0){
            return true;
        }
        return false;
    }

    /**
     * 删除Menu的方法
     * @param id
     */
    @Override
    public String delete(Long id) {
        int i = sysMenuMapper.deleteByPrimaryKey(id);
        return "";
    }

    /**
     * pilaingshanchu
     * @param id
     */
    @Override
    public void deletes(Long[] id) {
        Arrays.asList(id).forEach(
                e->delete(e)
        );
    }

    /**
     * 通过ID获取Menu
     * @param id
     * @return SysMenu
     */
    @Override
    public SysMenu get(Long id) {
        return sysMenuMapper.selectByPrimaryKey(id);
    }


    /**
     * 通过userID获取当前ID全部sysmenu的方式
     * @param id
     * @return List<SysMenu>
     */
    @Override
    public List<SysMenu> selectMenuByUserID(Long id) {
        return sysMenuMapper.selectByUserID(id);
    }

    /**
     * 通过menu对象修改menu
     * @param menu
     */
    @Override
    public void update(SysMenu menu) {
        sysMenuMapper.updateByPrimaryKey(menu);
    }

    /**
     * 通过用户名获取权限
     * @param userName
     * @return Set<String>
     */
    @Override
    public Set<String> listPermissions(String userName) {
        Set<String> result = new HashSet<>();
        List<Role> roles = roleService.listRoles(userName);

        List<SysMenuRole> rolePermissions = new ArrayList<>();

        for (Role role : roles) {
            SysMenuRoleExample example = new SysMenuRoleExample();
            example.createCriteria().andRoleIdEqualTo(role.getRoleId().intValue());
            List<SysMenuRole> rps= sysMenuRoleMapper.selectByExample(example);
            rolePermissions.addAll(rps);
        }

        for (SysMenuRole sysMenuRole : rolePermissions) {
            SysMenu M = getSysMenuMapper.selectByPrimaryKey(sysMenuRole.getMenuId().longValue());
            result.add(M.getName());
        }

        return result;
    }

    @Override
    public List<SysMenu> list(Role role) {
        List<SysMenu> result = new ArrayList<>();
        SysMenuRoleExample example = new SysMenuRoleExample();
        example.createCriteria().andRoleIdEqualTo(role.getRoleId().intValue());
        List<SysMenuRole> rps = sysMenuRoleMapper.selectByExample(example);
        for (SysMenuRole sysMenuRole : rps) {
            result.add(sysMenuMapper.selectByPrimaryKey(sysMenuRole.getMenuId().longValue()));
    }

        return result;
}
    /**
     * 根据传入的Url进行拦截，如果存在就拦截
     * @param requestURI
     * @return
     */
    @Override
    public boolean needInterceptor(String requestURI) {
        List<SysMenu> ps = list();
        for (SysMenu p : ps) {
            if (p.getUrl().equals(requestURI)){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }

    @Override
    public Set<String> listPermissionURLs(String userName) {
        Set<String> result = new HashSet<>();
        List<Role> roles = roleService.listRoles(userName);

        List<SysMenuRole> rolePermissions = new ArrayList<>();

        for (Role role : roles) {
            SysMenuRoleExample example = new SysMenuRoleExample();
            example.createCriteria().andRoleIdEqualTo(role.getRoleId().intValue());
            List<SysMenuRole> rps = sysMenuRoleMapper.selectByExample(example);
            rolePermissions.addAll(rps);
        }

        for (SysMenuRole sysMenuRole : rolePermissions) {
            SysMenu p = sysMenuMapper.selectByPrimaryKey(sysMenuRole.getMenuId().longValue());
            result.add(p.getUrl());
        }

        return result;
    }

    @Override
    public List<SysMenu> listParentDirectory(Integer type) {
        SysMenuExample example=new SysMenuExample();
        int parentType=type-1;
        example.createCriteria().andTypeEqualTo(parentType);
        List<SysMenu> result=sysMenuMapper.selectByExample(example);
        return result;
    }


}
