package com.example.letswalk.service.serviceimpl;

import com.example.letswalk.dao.UserAddressMapper;
import com.example.letswalk.pojo.UserAddress;
import com.example.letswalk.pojo.example.UserAddressExample;
import com.example.letswalk.service.UserAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * liuxianlin
 */
@Service
public class UserAddressServiceImpl  implements UserAddressService {


    @Autowired
    UserAddressMapper userAddressMapper;

    @Override
    public List<UserAddress> selectAll(Integer id) {
        UserAddressExample example = new UserAddressExample();
        example.createCriteria().andUserIdEqualTo(id);
        example.setOrderByClause("status ASC");
        return userAddressMapper.selectByExample(example);
    }
    @Override
    public int insertAddress(UserAddress userAddress) {
        return userAddressMapper.insert(userAddress);
    }

    @Override
    public int delectAddress(Integer id) {
        return userAddressMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateAddress(UserAddress userAddress) {
        return userAddressMapper.updateByPrimaryKey(userAddress);
    }

    @Override
    public UserAddress selectAddress(Integer addressId) {
        return userAddressMapper.selectByPrimaryKey(addressId);
    }
}
