package com.example.letswalk.service.serviceimpl;

import com.example.letswalk.dao.UserinfoMapper;
import com.example.letswalk.pojo.Userinfo;
import com.example.letswalk.pojo.example.UserinfoExample;
import com.example.letswalk.service.UserinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
/**
 * 用户信息服务层
 * liuxianlin
 */
@Service
public class UserinfoServiceImpl implements UserinfoService {
    @Autowired
    UserinfoMapper userinfoMapper;

    @Override
    public List<Userinfo> selectUserinfoLits() {
        return  userinfoMapper.selectByPrimaryKeyLits();
    }

    @Override
    public int selectuser(String openId) {
        return userinfoMapper.selectUserId(openId);
    }

    @Override
    public Boolean insetUser(Userinfo userinfo) {
       int i = userinfoMapper.insertSelective(userinfo);
       if(i>0){
           return true;
       }
        return false;

    }

    @Override
    public Boolean updateBalanceByOpenId(Userinfo userinfo) {
       UserinfoExample example=new UserinfoExample();
       example.createCriteria().andOpenIdEqualTo(userinfo.getOpenId());
       int i = userinfoMapper.updateByExampleSelective(userinfo,example);
       if(i>0){
           return true;
       }
        return false;

    }

    @Override
    public Userinfo selectUserByOpenid(String openId) {
        return userinfoMapper.selectUserByOpenId(openId);

    }

}
