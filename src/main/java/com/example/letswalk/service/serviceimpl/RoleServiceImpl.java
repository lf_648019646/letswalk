package com.example.letswalk.service.serviceimpl;


import com.example.letswalk.dao.RoleMapper;
import com.example.letswalk.dao.SysUserRoleMapper;
import com.example.letswalk.pojo.Role;
import com.example.letswalk.pojo.SysUser;
import com.example.letswalk.pojo.SysUserRole;
import com.example.letswalk.pojo.example.RoleExample;
import com.example.letswalk.pojo.example.SysUserRoleExample;
import com.example.letswalk.service.RoleService;
import com.example.letswalk.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 系统角色服务实现类
 * 获取角色类的方法
 * Auther:wangwei
 *  */
@Service
public class RoleServiceImpl implements RoleService{

    @Autowired
    RoleMapper roleMapper;

    @Autowired
    SysUserService sysUserService;

    @Autowired
    SysUserRoleMapper sysUserRoleMapper;


    /**
     *加载时获取全部Role的方法
     * */
    @Override
    public List<Role> list() {
        RoleExample example =new RoleExample();
        example.setOrderByClause("role_id asc");
        return roleMapper.selectByExample(example);
    }
    /**
     * 增加Role的方法
     * @param role
     */
    @Override
    public void add(Role role) {
        roleMapper.insert(role);
    }
    /**
     * 通过ID删除Role的方法
     * @param id
     */
    @Override
    public void delete(Long id) {
        roleMapper.deleteByPrimaryKey(id);
    }
    /**
     * 通过ID获取Role的方式
     * @param id
     * @return
     */
    @Override
    public Role get(Long id) {
        return roleMapper.selectByPrimaryKey(id);
    }
    /**
     * 通过Role修改Role
     * @param role
     */
    @Override
    public void update(Role role) {
        roleMapper.updateByPrimaryKey(role);
    }


    /**
     * 通过username查询角色
     * @param userName
     * @return List<Role>
     */
    @Override
    public Set<String> listRoleNames(String userName) {
        Set<String> result = new HashSet<>();
        List<Role> roles = listRoles(userName);
        for (Role role : roles) {
            result.add(role.getRoleName());
        }
        return result;
    }



    /**
     * 通过User对象查 拥有的角色
     * @param userName
     * @return List<Role>
     */
    @Override
    public List<Role> listRoles(String userName) {
        List<Role> roles = new ArrayList<>();
        SysUser user = sysUserService.getByName(userName);
        if(null==user)
            return roles;

        roles = listRoles(user);
        return roles;
    }

    /**
     *
     * @param user
     * @return List<Role>
     */
    @Override
    public List<Role> listRoles(SysUser user) {
        List<Role> roles = new ArrayList<>();

        SysUserRoleExample example = new SysUserRoleExample();

        example.createCriteria().andUserIdEqualTo(user.getUserId().intValue());
        List<SysUserRole> userRoles= sysUserRoleMapper.selectByExample(example);

        for (SysUserRole userRole : userRoles) {
            Role role=roleMapper.selectByPrimaryKey(userRole.getRoleId().longValue());
            roles.add(role);
        }
        return roles;
    }
}
