package com.example.letswalk.service.serviceimpl;

import com.example.letswalk.dao.VirtualCurrencyMapper;
import com.example.letswalk.pojo.VirtualCurrency;
import com.example.letswalk.service.VirtualCurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *虚拟币服务层
 * liuxianlin
 */
@Service
public class VirtualCurrencyServiceImpl implements VirtualCurrencyService {
    @Autowired
    VirtualCurrencyMapper virtualCurrencyMapper;
    @Override
    public List<VirtualCurrency> selectList() {
        return virtualCurrencyMapper.selectList();
    }
}
