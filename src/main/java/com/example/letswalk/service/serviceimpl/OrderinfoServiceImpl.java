package com.example.letswalk.service.serviceimpl;

import com.example.letswalk.dao.OrderInfoMapper;
import com.example.letswalk.pojo.OrderInfo;
import com.example.letswalk.pojo.example.OrderInfoExample;
import com.example.letswalk.service.OrderinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderinfoServiceImpl  implements OrderinfoService{
@Autowired
    OrderInfoMapper orderInfoMapper;

    @Override
    public OrderInfo selectInfo(Integer id) {
        return  orderInfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<OrderInfo> getuserconverInfo(Integer id) {

        return orderInfoMapper.selectByuserId(id);
    }

    @Override
    public List<OrderInfo> selectAll(Integer ProductId) {
        OrderInfoExample example = new OrderInfoExample();
        example.createCriteria().andProductIdEqualTo(ProductId);
        return   orderInfoMapper.selectByExample(example);


    }

    @Override
    public int insertOrderfo(OrderInfo orderInfo) {
        return  orderInfoMapper.insert(orderInfo);
    }
}
