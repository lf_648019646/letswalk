package com.example.letswalk.service.serviceimpl;

import com.example.letswalk.dao.StepDetailsMapper;
import com.example.letswalk.pojo.StepDetails;
import com.example.letswalk.pojo.example.StepDetailsExample;
import com.example.letswalk.service.StepDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

@Service
public class StepDetailsServiceImpl implements StepDetailsService {

@Autowired
StepDetailsMapper stepDetailsMapper;


    @Override
    public List<StepDetails> selectByPrimary() {
        return stepDetailsMapper.selectByPrimaryKeyList();
    }

    @Override
    public List<StepDetails> selectinfoUser(Integer id) {
        StepDetailsExample example = new StepDetailsExample();
        example.createCriteria().andUserIdEqualTo(id);
        return stepDetailsMapper.selectByExample(example);
    }

    @Override
    public boolean insertStepRecord(StepDetails stepDetails) {
        int i= stepDetailsMapper.insert(stepDetails);
        if(i>0){
            return true;
        }
        return false;
    }

    @Override
    public List<StepDetails> selectStepDetailsNum(StepDetails stepDetails, LocalDate today) {
        StepDetailsExample example = new StepDetailsExample();
        example.createCriteria().andTypeEqualTo(stepDetails.getType()).andUserIdEqualTo(stepDetails.getUserId())
                                .andStepTimeBetween(new Date(today.atStartOfDay().toInstant(ZoneOffset.of("+8")).toEpochMilli()),stepDetails.getStepTime());
        return stepDetailsMapper.selectByExample(example);
    }

    @Override
    public StepDetails selectinfo(Integer id) {
        return stepDetailsMapper.selectByPrimaryKey(id);}

    @Override
    public boolean select() {
        return stepDetailsMapper.delete();
    }
}
