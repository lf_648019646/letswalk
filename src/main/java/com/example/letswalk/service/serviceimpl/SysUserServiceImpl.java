package com.example.letswalk.service.serviceimpl;

import com.example.letswalk.dao.SysUserMapper;
import com.example.letswalk.pojo.Role;
import com.example.letswalk.pojo.SysUser;
import com.example.letswalk.pojo.example.SysUserExample;
import com.example.letswalk.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 系统用户服务实现类
 * 获取用户相关的方法
 * Auther:wangwei
 * */
@Service
public class SysUserServiceImpl implements SysUserService{


    @Autowired
    SysUserMapper sysUserMapper;

    /**
     * 通过用户ID获取当前用户的全部角色
     * @param id
     * @return List<Role>
     */
    @Override
    public List<Role> selectRoleBySysUserId(Long id) {
        return sysUserMapper.selectRoleById(id);
    }

    /**
     *  通过账号密码获取sys对象
     * @param map
     * @return SysUser
     */
    @Override
    public SysUser selectByMap(Map<String, Object> map) {
        return sysUserMapper.selectUserByMap(map);
    }


    /**
     *获取全部SysUser的方法
     * */
    @Override
    public List<SysUser> list() {
        SysUserExample example =new SysUserExample();
        example.setOrderByClause("user_id asc");
        return sysUserMapper.selectByExample(example);
    }
    /**
     * 新增SysUser 成功返回success
     *              失败返回fail
     * param:sysUser
     * */
    @Override
    public String add(SysUser sysUser) {
        if(getByName(sysUser.getUsername())==null){
            int result=sysUserMapper.insert(sysUser);
            if(result>0){
                return "success";
            }
        }
        return "fail";
    }
    /**
     * 删除User的方法
     * @param id
     */
    @Override
    public void delete(Long id) {
        sysUserMapper.deleteByPrimaryKey(id);
    }

    /**
     * 通过ID获取User
     * @param id
     * @return SysMenu
     */
    @Override
    public SysUser get(Long id) {
        return sysUserMapper.selectByPrimaryKey(id);
    }
    /**
     * 通过User对象修改User
     * @param sysUser
     */
    @Override
    public String update(SysUser sysUser) {
        int i=sysUserMapper.updateByPrimaryKey(sysUser);
        if(i>0){
            return "success";
        }
        return "fail";
    }

    /**
     *处理添加结果
     * @param sysUser
     * @return String
     */
    @Override
    public String insertSysUser(SysUser sysUser) {
        if(getByName(sysUser.getUsername())==null){
            int i = sysUserMapper.insert(sysUser);
            if(i>0){
                return "success";
            }
        }
        return "fail";
    }

    /**
     * 判断用户名是否已存在
     * @param username
     * @return
     */
    @Override
    public List<SysUser> selectByUsername(String username) {
        SysUserExample example = new SysUserExample();
        example.createCriteria().andUsernameEqualTo(username);
        return sysUserMapper.selectByExample(example);

    }

    /**
     * 判断邮箱是否已存在
     * @param email
     * @return list
     */

    @Override
    public List<SysUser> selectByEmail(String email) {
        SysUserExample example = new SysUserExample();
        example.createCriteria().andEmailEqualTo(email);
        return sysUserMapper.selectByExample(example);
    }

    /**
     * 判断电话号码是否已存在
     * @param mobile
     * @return list
     */
    @Override
    public List<SysUser> selectByMobile(String mobile) {
        SysUserExample example = new SysUserExample();
        example.createCriteria().andMobileEqualTo(mobile);
        return sysUserMapper.selectByExample(example);
    }
    /**
     * 通过用户名查询当前用户
     * @param name
     * @return
     */
    @Override
    public SysUser getByName(String name) {
        SysUserExample example = new SysUserExample();
        example.createCriteria().andUsernameEqualTo(name);
        List<SysUser> users = sysUserMapper.selectByExample(example);
        if(users.isEmpty())
            return null;
        return users.get(0);
    }

    @Override
    public void updateStatusByUserId(SysUser sysUser) {
        sysUserMapper.updateStatusByPrimaryKey(sysUser);
    }

    @Override
    public List<SysUser> selectSysUserByUsernameAndDate(String username,Date min,Date max) {
        SysUserExample example = new SysUserExample();
        if(min==null&&max==null){
            example.createCriteria().andUsernameLike("%"+username+"%");
        }else{
            if(min!=null&&max==null){
                example.createCriteria().andUsernameLike("%"+username+"%").andCreateTimeGreaterThanOrEqualTo(min);
            }else if(min==null&&max!=null){
                example.createCriteria().andUsernameLike("%"+username+"%").andCreateTimeLessThanOrEqualTo(max);
            }else{
                example.createCriteria().andUsernameLike("%"+username+"%").andCreateTimeBetween(min,new Date(max.getTime()+1*24*60*60*1000));
            }
        }
        example.setOrderByClause("user_id asc");
        List<SysUser> userInfo = sysUserMapper.selectByExample(example);
        if(userInfo.isEmpty()){
            return null;
        }
        return userInfo;
    }
}

