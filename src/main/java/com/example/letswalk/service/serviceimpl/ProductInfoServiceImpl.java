package com.example.letswalk.service.serviceimpl;

import com.example.letswalk.dao.ProductInfoMapper;
import com.example.letswalk.pojo.ProductInfo;
import com.example.letswalk.pojo.example.ProductInfoExample;
import com.example.letswalk.service.ProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 商品信息逻辑处理层
 * author:neal
 */
@Service
public class ProductInfoServiceImpl implements ProductInfoService{
    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Override
    public int deleteProductInfo(String productName) {
        ProductInfoExample example = new ProductInfoExample();
        example.createCriteria().andProductNameEqualTo(productName);
        return productInfoMapper.deleteByExample(example);
    }


    @Override
    public int insertProductInfo(ProductInfo productInfo) {
        return productInfoMapper.insertSelective(productInfo);
    }

    @Override
    public List<ProductInfo> selectAllProduct() {
        ProductInfoExample example = new ProductInfoExample();
        example.createCriteria();
        return productInfoMapper.selectByExample(example);
    }


    @Override
    public int deleteProductsInfo(List<String> productName) {
        ProductInfoExample example = new ProductInfoExample();
        example.createCriteria().andProductNameIn(productName);
        return productInfoMapper.deleteByExample(example);
    }


    @Override
    public List<ProductInfo> selectByRegion(String index) {
        ProductInfoExample example = new ProductInfoExample();
        example.createCriteria().andProductRegionEqualTo(index);
        return productInfoMapper.selectByExample(example);
    }


    @Override
    public List<ProductInfo> selectByCreateTime(Date date) {
        ProductInfoExample example = new ProductInfoExample();
        example.createCriteria().andProductCreateTimeEqualTo(date);
        return productInfoMapper.selectByExample(example);
    }

    @Override
    public int updateProductInfo(ProductInfo productInfo,Date oldDate) {
        ProductInfoExample example = new ProductInfoExample();
        example.createCriteria().andProductCreateTimeEqualTo(oldDate);
        return productInfoMapper.updateByExample(productInfo,example);
    }

    @Override
    public ProductInfo selectProductInfo(Integer id) {
        return   productInfoMapper.selectByPrimaryKey(id);

    }

    @Override
    public List<ProductInfo> selectProductsInfo(String productName) {
        ProductInfoExample example = new ProductInfoExample();
        example.createCriteria().andProductNameEqualTo(productName);
        return productInfoMapper.selectByExample(example);

    }

    @Override
    public int updatePro(ProductInfo productInfo) {
        return productInfoMapper.updateByPrimaryKey(productInfo);
    }
}
