package com.example.letswalk.service;

import com.example.letswalk.pojo.ProductInfo;

import java.util.Date;
import java.util.List;
/**
 * 商品信息处理
 */
public interface ProductInfoService {
    int insertProductInfo(ProductInfo productInfo);

    List<ProductInfo> selectAllProduct();

    int deleteProductInfo(String productName);

    int deleteProductsInfo(List<String> productName);

    List<ProductInfo> selectByRegion(String index);

    List<ProductInfo> selectByCreateTime(Date date);

    int updateProductInfo(ProductInfo productInfo,Date oldDate);

    ProductInfo selectProductInfo(Integer id);

    List<ProductInfo> selectProductsInfo( String  productName);

    int  updatePro(ProductInfo productInfo);
}
