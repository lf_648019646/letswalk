package com.example.letswalk.service;

import com.example.letswalk.pojo.Role;
import com.example.letswalk.pojo.SysUser;

import java.util.Date;
import java.util.List;
import java.util.Map;
/**
 * 系统用户服务层
 * 获取用户相关的接口
 * Auther:wangwei
 * */
public interface SysUserService {
    /**
     * 通过用户ID获取当前用户的全部角色
     * @param id
     * @return List<Role>
     */
    List<Role> selectRoleBySysUserId(Long id);

    /**
     *  通过账号密码获取sys对象
     * @param map
     * @return SysUser
     */
    SysUser selectByMap(Map<String,Object> map);

    /**
     *获取全部SysUser的方法
     * */
    List<SysUser> list();
    /**
     * 新增SysUser
     * param:sysUser
     * */
    String add(SysUser sysUser);

    /**
     * 删除User的方法
     * @param id
     */
    void delete(Long id);

    /**
     * 通过ID获取User
     * @param id
     * @return SysMenu
     */
    SysUser get(Long id);



    /**
     * 通过User对象修改User
     * @param sysUser
     */
    String update(SysUser sysUser);





    /**
     * 添加帐户
     * @param sysUser
     * @return int
     */
    String insertSysUser(SysUser sysUser);

    /**
     *根据用户名查询用户信息
     * @param username
     * @return list
     */
    List<SysUser> selectByUsername(String username);

    /**
     * 根据邮箱查询用户信息
     * @param email
     * @return list
     */

    List<SysUser> selectByEmail(String email);

    /**
     * 根据电话号码查询用户信息
     * @param mobile
     * @return list
     */

    List<SysUser> selectByMobile(String mobile);

    /**
     * 通过用户名查询当前用户
     * @param name
     * @return
     */
     SysUser getByName(String name);

    /**
     * 通过用户ID修改状态
     * @param sysUser
     */
    void updateStatusByUserId(SysUser sysUser);

    /**
     * 通过用户名时间模糊查询
     * @param username
     * @return
     */
    List<SysUser> selectSysUserByUsernameAndDate(String username,Date min,Date max);

}
