package com.example.letswalk.service;

import com.example.letswalk.pojo.SysUser;

public interface SysUserRoleService {
    /**
     * 通过SysUser,RoleId 设置联系 设置一个用户有什么角色
     * @param sysUser
     * @param roleIds
     */
    public void setRoles(SysUser sysUser, Integer[] roleIds);

    /**
     * 通过UserId删除角色
     * @param userId
     */
    public void deleteByUser(int userId);

    /**
     * 通过RoleId删除角色被赋予的用户
     * @param roleId
     */
    public void deleteByRole(int roleId);
}
