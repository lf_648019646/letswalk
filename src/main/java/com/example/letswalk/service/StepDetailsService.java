package com.example.letswalk.service;

import com.example.letswalk.pojo.StepDetails;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface StepDetailsService {
    /**
     * 后台查询所有查询步数明细
     * @return
     */
    List<StepDetails> selectByPrimary();

    StepDetails selectinfo(Integer id);

    List<StepDetails> selectinfoUser(Integer id);

    boolean insertStepRecord(StepDetails stepDetails);

    List<StepDetails> selectStepDetailsNum(StepDetails stepDetails, LocalDate today);

    boolean select();


}
