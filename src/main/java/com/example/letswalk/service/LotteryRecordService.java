package com.example.letswalk.service;

import com.example.letswalk.pojo.LotteryRecord;

import java.util.List;

/**
 * 中奖记录服务层
 * liuxianlin
 */
public interface LotteryRecordService {
    /**
     *加载时获取全部记录的方法
     * */
    List<LotteryRecord> LotteryRecordList();

    List<LotteryRecord> LotteryRecordListByUserName(String openId);


    int deleteByPrimaryKey(Integer id);

    int  insertLottery(LotteryRecord lotteryRecord);

}
