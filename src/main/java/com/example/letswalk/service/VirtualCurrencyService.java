package com.example.letswalk.service;

import com.example.letswalk.pojo.VirtualCurrency;

import java.util.List;

/**
 * 虚拟币的service
 * liuxianlin
 */
public interface VirtualCurrencyService {
    List<VirtualCurrency> selectList();

}
