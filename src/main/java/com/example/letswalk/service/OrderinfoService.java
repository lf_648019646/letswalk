package com.example.letswalk.service;

import com.example.letswalk.pojo.OrderInfo;

import java.util.List;

/**
 * 订单信息查询
 */
public interface OrderinfoService {
    //订单Id获取
    OrderInfo selectInfo(Integer id);

    //用户Id获取
    List<OrderInfo> getuserconverInfo(Integer id);

    //通过商品获取订单记录
    List<OrderInfo>  selectAll( Integer ProductId);

    //新增订单

     int  insertOrderfo(OrderInfo orderInfo);
}
