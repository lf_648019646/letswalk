package com.example.letswalk.service;


import com.example.letswalk.pojo.Role;
import com.example.letswalk.pojo.SysMenu;
import java.util.List;
import java.util.Set;

/**
 * 系统目录服务层
 * 定义方法
 * author:wangwei
 *  */
public interface SysMenuService{
    /**
     *获取全部SysMenu的方法
     * */
    List<SysMenu> list();
    /**
     * 新增SysMenu
     * param:menu
     * */
    Boolean add(SysMenu menu);

    /**
     * 删除Menu的方法
     * @param id
     */
    String delete(Long id);

    /**
     * 批量删除
     * @param id
     */
    void deletes(Long[] id);

    /**
     * 通过ID获取Menu
     * @param id
     * @return SysMenu
     */
    SysMenu get(Long id);

    /**
     * 通过userID获取当前ID全部sysmenu的方式
     * @param id
     * @return List<SysMenu>
     */
    List<SysMenu> selectMenuByUserID(Long id);

    /**
     * 通过menu对象修改menu
     * @param menu
     */
    void update(SysMenu menu);

    /**
     * 通过用户名获取权限
     * @param userName
     * @return Set<String>
     */
    Set<String> listPermissions(String userName);

    /**
     *
     * @param role
     * @return
     */
    List<SysMenu> list(Role role);

    /**
     * 根据传入的Url进行拦截，如果存在就拦截
     * @param requestURI
     * @return
     */
    public boolean needInterceptor(String requestURI);

    /**
     * 用来获取某个用户所拥有的权限地址集合
     * @param userName
     * @return
     */
    public Set<String> listPermissionURLs(String userName);

    /**
     * 查询上级权限（目录）名称
     * @param type
     * @return
     */
    public List<SysMenu> listParentDirectory(Integer type);

}

