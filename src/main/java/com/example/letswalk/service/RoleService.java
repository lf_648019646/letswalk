package com.example.letswalk.service;







import com.example.letswalk.pojo.Role;
import com.example.letswalk.pojo.SysUser;
import org.apache.catalina.User;


import java.util.List;
import java.util.Set;


/**
 * 系统角色服务层
 * 获取角色类的接口
 * Auther:wangwei
 *  */
public interface RoleService  {
    /**
    *加载时获取全部Role的方法
    * */
    List<Role> list();

    /**
     * 增加Role的方法
     * @param role
     */
    void add(Role role);

    /**
     * 通过ID删除Role的方法
     * @param id
     */
    void delete(Long id);

    /**
     * 通过ID获取Role的方式
     * @param id
     * @return
     */
    Role get(Long id);

    /**
     * 通过Role修改Role
     * @param role
     */
    void update(Role role);


    /**
     * 通过username查询角色
     * @param userName
     * @return List<Role>
     */
     Set<String> listRoleNames(String userName);

    /**
     * 通过User名查 拥有的角色
     * @param userName
     * @return List<Role>
     */
     List<Role> listRoles(String userName);

    /**
     *　
     * @param user
     * @return List<Role>
     */
    List<Role> listRoles(SysUser user);

}
