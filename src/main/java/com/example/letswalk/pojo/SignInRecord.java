package com.example.letswalk.pojo;

import java.util.Date;

/**
 * 签到记录表实体类
 */
public class SignInRecord {
    private Integer id;

    private Date signInTime;

    private Integer userId;

    private Integer signInDay;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getSignInTime() {
        return signInTime;
    }

    public void setSignInTime(Date signInTime) {
        this.signInTime = signInTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSignInDay() {
        return signInDay;
    }

    public void setSignInDay(Integer signInDay) {
        this.signInDay = signInDay;
    }
}