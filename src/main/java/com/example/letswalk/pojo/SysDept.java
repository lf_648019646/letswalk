package com.example.letswalk.pojo;

/**
 * 部门表实体类
 */
public class SysDept {
    private Long deptId;

    private Long parentId;

    private String name;

    private Integer oederNum;

    private Byte delFlag;

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getOederNum() {
        return oederNum;
    }

    public void setOederNum(Integer oederNum) {
        this.oederNum = oederNum;
    }

    public Byte getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Byte delFlag) {
        this.delFlag = delFlag;
    }
}