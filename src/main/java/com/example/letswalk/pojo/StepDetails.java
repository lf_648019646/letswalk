package com.example.letswalk.pojo;

import java.util.Date;

/**
 *步数明细表实体类
 */
public class StepDetails {
    private Integer stepId;

    private Integer type;

    private Integer stepNum;

    private Date stepTime;

    private Integer userId;

    public Integer getStepId() {
        return stepId;
    }

    public void setStepId(Integer stepId) {
        this.stepId = stepId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStepNum() {
        return stepNum;
    }

    public void setStepNum(Integer stepNum) {
        this.stepNum = stepNum;
    }

    public Date getStepTime() {
        return stepTime;
    }

    public void setStepTime(Date stepTime) {
        this.stepTime = stepTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "StepDetails{" +
                "stepId=" + stepId +
                ", type=" + type +
                ", stepNum=" + stepNum +
                ", stepTime=" + stepTime +
                ", userId=" + userId +
                '}';
    }
}