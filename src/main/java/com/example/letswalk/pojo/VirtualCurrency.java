package com.example.letswalk.pojo;

import java.util.Date;

/**
 * 虚拟币表实体类
 */
public class VirtualCurrency {
    private Integer virtualId;

    private Integer userId;

    private Integer cash;

    private Date cashTime;

    private Integer cashStep;

    public Integer getVirtualId() {
        return virtualId;
    }

    public void setVirtualId(Integer virtualId) {
        this.virtualId = virtualId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCash() {
        return cash;
    }

    public void setCash(Integer cash) {
        this.cash = cash;
    }

    public Date getCashTime() {
        return cashTime;
    }

    public void setCashTime(Date cashTime) {
        this.cashTime = cashTime;
    }

    public Integer getCashStep() {
        return cashStep;
    }

    public void setCashStep(Integer cashStep) {
        this.cashStep = cashStep;
    }
}