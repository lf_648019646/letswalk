package com.example.letswalk.pojo;

import java.util.Date;

/**
 * 用户信息表实体类
 */
public class Userinfo {
    private Integer userId;

    private String openId;

    private Integer balance;

    private String inviterOpenId;

    private Date createTime;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId == null ? null : openId.trim();
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getInviterOpenId() {
        return inviterOpenId;
    }

    public void setInviterOpenId(String inviterOpenId) {
        this.inviterOpenId = inviterOpenId == null ? null : inviterOpenId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "Userinfo{" +
                "userId=" + userId +
                ", openId='" + openId + '\'' +
                ", balance=" + balance +
                ", inviterOpenId='" + inviterOpenId + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}