package com.example.letswalk.pojo;

import java.util.Date;

public class ProductInfo {
    private Integer id;

    private String productName;

    private Integer productPrice;

    private String productDetails;

    private Integer productSurolus;

    private String productPicture;

    private Byte status;

    private String productRegion;

    private Date productCreateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    public Integer getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Integer productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(String productDetails) {
        this.productDetails = productDetails == null ? null : productDetails.trim();
    }

    public Integer getProductSurolus() {
        return productSurolus;
    }

    public void setProductSurolus(Integer productSurolus) {
        this.productSurolus = productSurolus;
    }

    public String getProductPicture() {
        return productPicture;
    }

    public void setProductPicture(String productPicture) {
        this.productPicture = productPicture;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getProductRegion() {
        return productRegion;
    }

    public void setProductRegion(String productRegion) {
        this.productRegion = productRegion == null ? null : productRegion.trim();
    }

    public Date getProductCreateTime() {
        return productCreateTime;
    }

    public void setProductCreateTime(Date productCreateTime) {
        this.productCreateTime = productCreateTime;
    }

    @Override
    public String toString() {
        return "ProductInfo{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", productPrice=" + productPrice +
                ", productDetails='" + productDetails + '\'' +
                ", productSurolus=" + productSurolus +
                ", productPicture='" + productPicture + '\'' +
                ", status=" + status +
                ", productRegion='" + productRegion + '\'' +
                ", productCreateTime=" + productCreateTime +
                '}';
    }
}