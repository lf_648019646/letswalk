package com.example.letswalk.pojo.example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProductInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ProductInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andProductNameIsNull() {
            addCriterion("product_name is null");
            return (Criteria) this;
        }

        public Criteria andProductNameIsNotNull() {
            addCriterion("product_name is not null");
            return (Criteria) this;
        }

        public Criteria andProductNameEqualTo(String value) {
            addCriterion("product_name =", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotEqualTo(String value) {
            addCriterion("product_name <>", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameGreaterThan(String value) {
            addCriterion("product_name >", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameGreaterThanOrEqualTo(String value) {
            addCriterion("product_name >=", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLessThan(String value) {
            addCriterion("product_name <", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLessThanOrEqualTo(String value) {
            addCriterion("product_name <=", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLike(String value) {
            addCriterion("product_name like", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotLike(String value) {
            addCriterion("product_name not like", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameIn(List<String> values) {
            addCriterion("product_name in", values, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotIn(List<String> values) {
            addCriterion("product_name not in", values, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameBetween(String value1, String value2) {
            addCriterion("product_name between", value1, value2, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotBetween(String value1, String value2) {
            addCriterion("product_name not between", value1, value2, "productName");
            return (Criteria) this;
        }

        public Criteria andProductPriceIsNull() {
            addCriterion("product_price is null");
            return (Criteria) this;
        }

        public Criteria andProductPriceIsNotNull() {
            addCriterion("product_price is not null");
            return (Criteria) this;
        }

        public Criteria andProductPriceEqualTo(Integer value) {
            addCriterion("product_price =", value, "productPrice");
            return (Criteria) this;
        }

        public Criteria andProductPriceNotEqualTo(Integer value) {
            addCriterion("product_price <>", value, "productPrice");
            return (Criteria) this;
        }

        public Criteria andProductPriceGreaterThan(Integer value) {
            addCriterion("product_price >", value, "productPrice");
            return (Criteria) this;
        }

        public Criteria andProductPriceGreaterThanOrEqualTo(Integer value) {
            addCriterion("product_price >=", value, "productPrice");
            return (Criteria) this;
        }

        public Criteria andProductPriceLessThan(Integer value) {
            addCriterion("product_price <", value, "productPrice");
            return (Criteria) this;
        }

        public Criteria andProductPriceLessThanOrEqualTo(Integer value) {
            addCriterion("product_price <=", value, "productPrice");
            return (Criteria) this;
        }

        public Criteria andProductPriceIn(List<Integer> values) {
            addCriterion("product_price in", values, "productPrice");
            return (Criteria) this;
        }

        public Criteria andProductPriceNotIn(List<Integer> values) {
            addCriterion("product_price not in", values, "productPrice");
            return (Criteria) this;
        }

        public Criteria andProductPriceBetween(Integer value1, Integer value2) {
            addCriterion("product_price between", value1, value2, "productPrice");
            return (Criteria) this;
        }

        public Criteria andProductPriceNotBetween(Integer value1, Integer value2) {
            addCriterion("product_price not between", value1, value2, "productPrice");
            return (Criteria) this;
        }

        public Criteria andProductDetailsIsNull() {
            addCriterion("product_details is null");
            return (Criteria) this;
        }

        public Criteria andProductDetailsIsNotNull() {
            addCriterion("product_details is not null");
            return (Criteria) this;
        }

        public Criteria andProductDetailsEqualTo(String value) {
            addCriterion("product_details =", value, "productDetails");
            return (Criteria) this;
        }

        public Criteria andProductDetailsNotEqualTo(String value) {
            addCriterion("product_details <>", value, "productDetails");
            return (Criteria) this;
        }

        public Criteria andProductDetailsGreaterThan(String value) {
            addCriterion("product_details >", value, "productDetails");
            return (Criteria) this;
        }

        public Criteria andProductDetailsGreaterThanOrEqualTo(String value) {
            addCriterion("product_details >=", value, "productDetails");
            return (Criteria) this;
        }

        public Criteria andProductDetailsLessThan(String value) {
            addCriterion("product_details <", value, "productDetails");
            return (Criteria) this;
        }

        public Criteria andProductDetailsLessThanOrEqualTo(String value) {
            addCriterion("product_details <=", value, "productDetails");
            return (Criteria) this;
        }

        public Criteria andProductDetailsLike(String value) {
            addCriterion("product_details like", value, "productDetails");
            return (Criteria) this;
        }

        public Criteria andProductDetailsNotLike(String value) {
            addCriterion("product_details not like", value, "productDetails");
            return (Criteria) this;
        }

        public Criteria andProductDetailsIn(List<String> values) {
            addCriterion("product_details in", values, "productDetails");
            return (Criteria) this;
        }

        public Criteria andProductDetailsNotIn(List<String> values) {
            addCriterion("product_details not in", values, "productDetails");
            return (Criteria) this;
        }

        public Criteria andProductDetailsBetween(String value1, String value2) {
            addCriterion("product_details between", value1, value2, "productDetails");
            return (Criteria) this;
        }

        public Criteria andProductDetailsNotBetween(String value1, String value2) {
            addCriterion("product_details not between", value1, value2, "productDetails");
            return (Criteria) this;
        }

        public Criteria andProductSurolusIsNull() {
            addCriterion("product_surolus is null");
            return (Criteria) this;
        }

        public Criteria andProductSurolusIsNotNull() {
            addCriterion("product_surolus is not null");
            return (Criteria) this;
        }

        public Criteria andProductSurolusEqualTo(Integer value) {
            addCriterion("product_surolus =", value, "productSurolus");
            return (Criteria) this;
        }

        public Criteria andProductSurolusNotEqualTo(Integer value) {
            addCriterion("product_surolus <>", value, "productSurolus");
            return (Criteria) this;
        }

        public Criteria andProductSurolusGreaterThan(Integer value) {
            addCriterion("product_surolus >", value, "productSurolus");
            return (Criteria) this;
        }

        public Criteria andProductSurolusGreaterThanOrEqualTo(Integer value) {
            addCriterion("product_surolus >=", value, "productSurolus");
            return (Criteria) this;
        }

        public Criteria andProductSurolusLessThan(Integer value) {
            addCriterion("product_surolus <", value, "productSurolus");
            return (Criteria) this;
        }

        public Criteria andProductSurolusLessThanOrEqualTo(Integer value) {
            addCriterion("product_surolus <=", value, "productSurolus");
            return (Criteria) this;
        }

        public Criteria andProductSurolusIn(List<Integer> values) {
            addCriterion("product_surolus in", values, "productSurolus");
            return (Criteria) this;
        }

        public Criteria andProductSurolusNotIn(List<Integer> values) {
            addCriterion("product_surolus not in", values, "productSurolus");
            return (Criteria) this;
        }

        public Criteria andProductSurolusBetween(Integer value1, Integer value2) {
            addCriterion("product_surolus between", value1, value2, "productSurolus");
            return (Criteria) this;
        }

        public Criteria andProductSurolusNotBetween(Integer value1, Integer value2) {
            addCriterion("product_surolus not between", value1, value2, "productSurolus");
            return (Criteria) this;
        }

        public Criteria andProductPictureIsNull() {
            addCriterion("product_picture is null");
            return (Criteria) this;
        }

        public Criteria andProductPictureIsNotNull() {
            addCriterion("product_picture is not null");
            return (Criteria) this;
        }

        public Criteria andProductPictureEqualTo(String value) {
            addCriterion("product_picture =", value, "productPicture");
            return (Criteria) this;
        }

        public Criteria andProductPictureNotEqualTo(String value) {
            addCriterion("product_picture <>", value, "productPicture");
            return (Criteria) this;
        }

        public Criteria andProductPictureGreaterThan(String value) {
            addCriterion("product_picture >", value, "productPicture");
            return (Criteria) this;
        }

        public Criteria andProductPictureGreaterThanOrEqualTo(String value) {
            addCriterion("product_picture >=", value, "productPicture");
            return (Criteria) this;
        }

        public Criteria andProductPictureLessThan(String value) {
            addCriterion("product_picture <", value, "productPicture");
            return (Criteria) this;
        }

        public Criteria andProductPictureLessThanOrEqualTo(String value) {
            addCriterion("product_picture <=", value, "productPicture");
            return (Criteria) this;
        }

        public Criteria andProductPictureIn(List<String> values) {
            addCriterion("product_picture in", values, "productPicture");
            return (Criteria) this;
        }

        public Criteria andProductPictureNotIn(List<String> values) {
            addCriterion("product_picture not in", values, "productPicture");
            return (Criteria) this;
        }

        public Criteria andProductPictureBetween(String value1, String value2) {
            addCriterion("product_picture between", value1, value2, "productPicture");
            return (Criteria) this;
        }

        public Criteria andProductPictureNotBetween(String value1, String value2) {
            addCriterion("product_picture not between", value1, value2, "productPicture");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andProductRegionIsNull() {
            addCriterion("product_region is null");
            return (Criteria) this;
        }

        public Criteria andProductRegionIsNotNull() {
            addCriterion("product_region is not null");
            return (Criteria) this;
        }

        public Criteria andProductRegionEqualTo(String value) {
            addCriterion("product_region =", value, "productRegion");
            return (Criteria) this;
        }

        public Criteria andProductRegionNotEqualTo(String value) {
            addCriterion("product_region <>", value, "productRegion");
            return (Criteria) this;
        }

        public Criteria andProductRegionGreaterThan(String value) {
            addCriterion("product_region >", value, "productRegion");
            return (Criteria) this;
        }

        public Criteria andProductRegionGreaterThanOrEqualTo(String value) {
            addCriterion("product_region >=", value, "productRegion");
            return (Criteria) this;
        }

        public Criteria andProductRegionLessThan(String value) {
            addCriterion("product_region <", value, "productRegion");
            return (Criteria) this;
        }

        public Criteria andProductRegionLessThanOrEqualTo(String value) {
            addCriterion("product_region <=", value, "productRegion");
            return (Criteria) this;
        }

        public Criteria andProductRegionLike(String value) {
            addCriterion("product_region like", value, "productRegion");
            return (Criteria) this;
        }

        public Criteria andProductRegionNotLike(String value) {
            addCriterion("product_region not like", value, "productRegion");
            return (Criteria) this;
        }

        public Criteria andProductRegionIn(List<String> values) {
            addCriterion("product_region in", values, "productRegion");
            return (Criteria) this;
        }

        public Criteria andProductRegionNotIn(List<String> values) {
            addCriterion("product_region not in", values, "productRegion");
            return (Criteria) this;
        }

        public Criteria andProductRegionBetween(String value1, String value2) {
            addCriterion("product_region between", value1, value2, "productRegion");
            return (Criteria) this;
        }

        public Criteria andProductRegionNotBetween(String value1, String value2) {
            addCriterion("product_region not between", value1, value2, "productRegion");
            return (Criteria) this;
        }

        public Criteria andProductCreateTimeIsNull() {
            addCriterion("product_create_time is null");
            return (Criteria) this;
        }

        public Criteria andProductCreateTimeIsNotNull() {
            addCriterion("product_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andProductCreateTimeEqualTo(Date value) {
            addCriterion("product_create_time =", value, "productCreateTime");
            return (Criteria) this;
        }

        public Criteria andProductCreateTimeNotEqualTo(Date value) {
            addCriterion("product_create_time <>", value, "productCreateTime");
            return (Criteria) this;
        }

        public Criteria andProductCreateTimeGreaterThan(Date value) {
            addCriterion("product_create_time >", value, "productCreateTime");
            return (Criteria) this;
        }

        public Criteria andProductCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("product_create_time >=", value, "productCreateTime");
            return (Criteria) this;
        }

        public Criteria andProductCreateTimeLessThan(Date value) {
            addCriterion("product_create_time <", value, "productCreateTime");
            return (Criteria) this;
        }

        public Criteria andProductCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("product_create_time <=", value, "productCreateTime");
            return (Criteria) this;
        }

        public Criteria andProductCreateTimeIn(List<Date> values) {
            addCriterion("product_create_time in", values, "productCreateTime");
            return (Criteria) this;
        }

        public Criteria andProductCreateTimeNotIn(List<Date> values) {
            addCriterion("product_create_time not in", values, "productCreateTime");
            return (Criteria) this;
        }

        public Criteria andProductCreateTimeBetween(Date value1, Date value2) {
            addCriterion("product_create_time between", value1, value2, "productCreateTime");
            return (Criteria) this;
        }

        public Criteria andProductCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("product_create_time not between", value1, value2, "productCreateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}