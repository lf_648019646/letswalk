package com.example.letswalk.pojo.example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 虚拟币sql语句辅助类
 */
public class VirtualCurrencyExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public VirtualCurrencyExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andVirtualIdIsNull() {
            addCriterion("virtual_id is null");
            return (Criteria) this;
        }

        public Criteria andVirtualIdIsNotNull() {
            addCriterion("virtual_id is not null");
            return (Criteria) this;
        }

        public Criteria andVirtualIdEqualTo(Integer value) {
            addCriterion("virtual_id =", value, "virtualId");
            return (Criteria) this;
        }

        public Criteria andVirtualIdNotEqualTo(Integer value) {
            addCriterion("virtual_id <>", value, "virtualId");
            return (Criteria) this;
        }

        public Criteria andVirtualIdGreaterThan(Integer value) {
            addCriterion("virtual_id >", value, "virtualId");
            return (Criteria) this;
        }

        public Criteria andVirtualIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("virtual_id >=", value, "virtualId");
            return (Criteria) this;
        }

        public Criteria andVirtualIdLessThan(Integer value) {
            addCriterion("virtual_id <", value, "virtualId");
            return (Criteria) this;
        }

        public Criteria andVirtualIdLessThanOrEqualTo(Integer value) {
            addCriterion("virtual_id <=", value, "virtualId");
            return (Criteria) this;
        }

        public Criteria andVirtualIdIn(List<Integer> values) {
            addCriterion("virtual_id in", values, "virtualId");
            return (Criteria) this;
        }

        public Criteria andVirtualIdNotIn(List<Integer> values) {
            addCriterion("virtual_id not in", values, "virtualId");
            return (Criteria) this;
        }

        public Criteria andVirtualIdBetween(Integer value1, Integer value2) {
            addCriterion("virtual_id between", value1, value2, "virtualId");
            return (Criteria) this;
        }

        public Criteria andVirtualIdNotBetween(Integer value1, Integer value2) {
            addCriterion("virtual_id not between", value1, value2, "virtualId");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andCashIsNull() {
            addCriterion("cash is null");
            return (Criteria) this;
        }

        public Criteria andCashIsNotNull() {
            addCriterion("cash is not null");
            return (Criteria) this;
        }

        public Criteria andCashEqualTo(Integer value) {
            addCriterion("cash =", value, "cash");
            return (Criteria) this;
        }

        public Criteria andCashNotEqualTo(Integer value) {
            addCriterion("cash <>", value, "cash");
            return (Criteria) this;
        }

        public Criteria andCashGreaterThan(Integer value) {
            addCriterion("cash >", value, "cash");
            return (Criteria) this;
        }

        public Criteria andCashGreaterThanOrEqualTo(Integer value) {
            addCriterion("cash >=", value, "cash");
            return (Criteria) this;
        }

        public Criteria andCashLessThan(Integer value) {
            addCriterion("cash <", value, "cash");
            return (Criteria) this;
        }

        public Criteria andCashLessThanOrEqualTo(Integer value) {
            addCriterion("cash <=", value, "cash");
            return (Criteria) this;
        }

        public Criteria andCashIn(List<Integer> values) {
            addCriterion("cash in", values, "cash");
            return (Criteria) this;
        }

        public Criteria andCashNotIn(List<Integer> values) {
            addCriterion("cash not in", values, "cash");
            return (Criteria) this;
        }

        public Criteria andCashBetween(Integer value1, Integer value2) {
            addCriterion("cash between", value1, value2, "cash");
            return (Criteria) this;
        }

        public Criteria andCashNotBetween(Integer value1, Integer value2) {
            addCriterion("cash not between", value1, value2, "cash");
            return (Criteria) this;
        }

        public Criteria andCashTimeIsNull() {
            addCriterion("cash_time is null");
            return (Criteria) this;
        }

        public Criteria andCashTimeIsNotNull() {
            addCriterion("cash_time is not null");
            return (Criteria) this;
        }

        public Criteria andCashTimeEqualTo(Date value) {
            addCriterion("cash_time =", value, "cashTime");
            return (Criteria) this;
        }

        public Criteria andCashTimeNotEqualTo(Date value) {
            addCriterion("cash_time <>", value, "cashTime");
            return (Criteria) this;
        }

        public Criteria andCashTimeGreaterThan(Date value) {
            addCriterion("cash_time >", value, "cashTime");
            return (Criteria) this;
        }

        public Criteria andCashTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("cash_time >=", value, "cashTime");
            return (Criteria) this;
        }

        public Criteria andCashTimeLessThan(Date value) {
            addCriterion("cash_time <", value, "cashTime");
            return (Criteria) this;
        }

        public Criteria andCashTimeLessThanOrEqualTo(Date value) {
            addCriterion("cash_time <=", value, "cashTime");
            return (Criteria) this;
        }

        public Criteria andCashTimeIn(List<Date> values) {
            addCriterion("cash_time in", values, "cashTime");
            return (Criteria) this;
        }

        public Criteria andCashTimeNotIn(List<Date> values) {
            addCriterion("cash_time not in", values, "cashTime");
            return (Criteria) this;
        }

        public Criteria andCashTimeBetween(Date value1, Date value2) {
            addCriterion("cash_time between", value1, value2, "cashTime");
            return (Criteria) this;
        }

        public Criteria andCashTimeNotBetween(Date value1, Date value2) {
            addCriterion("cash_time not between", value1, value2, "cashTime");
            return (Criteria) this;
        }

        public Criteria andCashStepIsNull() {
            addCriterion("cash_step is null");
            return (Criteria) this;
        }

        public Criteria andCashStepIsNotNull() {
            addCriterion("cash_step is not null");
            return (Criteria) this;
        }

        public Criteria andCashStepEqualTo(Integer value) {
            addCriterion("cash_step =", value, "cashStep");
            return (Criteria) this;
        }

        public Criteria andCashStepNotEqualTo(Integer value) {
            addCriterion("cash_step <>", value, "cashStep");
            return (Criteria) this;
        }

        public Criteria andCashStepGreaterThan(Integer value) {
            addCriterion("cash_step >", value, "cashStep");
            return (Criteria) this;
        }

        public Criteria andCashStepGreaterThanOrEqualTo(Integer value) {
            addCriterion("cash_step >=", value, "cashStep");
            return (Criteria) this;
        }

        public Criteria andCashStepLessThan(Integer value) {
            addCriterion("cash_step <", value, "cashStep");
            return (Criteria) this;
        }

        public Criteria andCashStepLessThanOrEqualTo(Integer value) {
            addCriterion("cash_step <=", value, "cashStep");
            return (Criteria) this;
        }

        public Criteria andCashStepIn(List<Integer> values) {
            addCriterion("cash_step in", values, "cashStep");
            return (Criteria) this;
        }

        public Criteria andCashStepNotIn(List<Integer> values) {
            addCriterion("cash_step not in", values, "cashStep");
            return (Criteria) this;
        }

        public Criteria andCashStepBetween(Integer value1, Integer value2) {
            addCriterion("cash_step between", value1, value2, "cashStep");
            return (Criteria) this;
        }

        public Criteria andCashStepNotBetween(Integer value1, Integer value2) {
            addCriterion("cash_step not between", value1, value2, "cashStep");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}