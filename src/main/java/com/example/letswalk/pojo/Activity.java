package com.example.letswalk.pojo;

import java.util.Date;

/**
 * 活动表实体类
 */
public class Activity {
    private Integer id;

    private Date beginTime;

    private Date endTime;

    private String winningDetails;

    private Date createTime;

    @Override
    public String toString() {
        return "Activity{" +
                "id=" + id +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", winningDetails='" + winningDetails + '\'' +
                ", createTime=" + createTime +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getWinningDetails() {
        return winningDetails;
    }

    public void setWinningDetails(String winningDetails) {
        this.winningDetails = winningDetails == null ? null : winningDetails.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}