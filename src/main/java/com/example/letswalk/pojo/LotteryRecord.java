package com.example.letswalk.pojo;

import java.util.Date;

/**
 * 中奖记录表实体类
 */
public class LotteryRecord {
    private Integer id;

    private Integer userId;

    private String userName;

    private Byte winningType;

    private Integer winningNum;

    private Date winTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Byte getWinningType() {
        return winningType;
    }

    public void setWinningType(Byte winningType) {
        this.winningType = winningType;
    }

    public Integer getWinningNum() {
        return winningNum;
    }

    public void setWinningNum(Integer winningNum) {
        this.winningNum = winningNum;
    }

    public Date getWinTime() {
        return winTime;
    }

    public void setWinTime(Date winTime) {
        this.winTime = winTime;
    }
}