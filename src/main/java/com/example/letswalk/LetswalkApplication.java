package com.example.letswalk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.servlet.MultipartConfigElement;

@SpringBootApplication
@EnableScheduling
public class LetswalkApplication {

	public static void main(String[] args) {
		SpringApplication.run(LetswalkApplication.class, args);
		System.out.print("bbbb");
		System.out.print("cc");
		System.out.println("neal");
        System.out.println("asdf");
    }

	/**
	 * 文件上传大小限定
	 * ahthor:neal
	 * @return
	 */
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory config = new MultipartConfigFactory();
		config.setMaxFileSize("9000MB");
		config.setMaxRequestSize("9000MB");
		return config.createMultipartConfig();
	}
}
