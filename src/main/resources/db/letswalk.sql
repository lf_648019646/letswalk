/*
Navicat MySQL Data Transfer

Source Server         : lcoal
Source Server Version : 50623
Source Host           : localhost:3306
Source Database       : letswalk

Target Server Type    : MYSQL
Target Server Version : 50623
File Encoding         : 65001

Date: 2018-09-30 16:00:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `activity`
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `begin_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `winning_details` varchar(255) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity
-- ----------------------------

-- ----------------------------
-- Table structure for `lottery_record`
-- ----------------------------
DROP TABLE IF EXISTS `lottery_record`;
CREATE TABLE `lottery_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `winning_type` tinyint(4) NOT NULL COMMENT '0：谢谢惠顾 1：一等奖 2：二等奖 3：三等奖',
  `winning_num` int(11) NOT NULL COMMENT '0：零虚拟币 1 ：一百虚拟币 2： 五十虚拟币 3： 二十虚拟币',
  `win_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lottery_record
-- ----------------------------

-- ----------------------------
-- Table structure for `order_info`
-- ----------------------------
DROP TABLE IF EXISTS `order_info`;
CREATE TABLE `order_info` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_status` int(11) NOT NULL,
  `logistics_num` varchar(255) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_price` int(11) NOT NULL,
  `exchange_quantity` int(11) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order_info
-- ----------------------------

-- ----------------------------
-- Table structure for `product_info`
-- ----------------------------
DROP TABLE IF EXISTS `product_info`;
CREATE TABLE `product_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) DEFAULT NULL,
  `product_price` int(11) DEFAULT NULL,
  `product_details` varchar(255) DEFAULT NULL,
  `product_surolus` int(11) DEFAULT NULL,
  `product_picture` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `product_region` varchar(255) DEFAULT NULL,
  `product_create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_info
-- ----------------------------
INSERT INTO `product_info` VALUES ('24', '巧乐兹', '40', 'asdfghjklasdf', '40', 'C:\\fakepath\\qiaolezi1.jpg,C:\\fakepath\\qiaolezi2.jpg,C:\\fakepath\\qiaolezi3.jpg,', '1', '3', '2018-09-30 15:06:43');
INSERT INTO `product_info` VALUES ('46', '冰红茶', '60', '3213213213', '50', 'C:\\fakepath\\binghongcha1.jpg,C:\\fakepath\\binghongcha2.jpg,C:\\fakepath\\binghongcha3.jpg,', '1', '3', '2018-09-30 15:24:09');
INSERT INTO `product_info` VALUES ('37', '可乐', '50', '65416362634', '20', 'C:\\fakepath\\cole1.jpg,C:\\fakepath\\cole2.jpg,C:\\fakepath\\cole3.jpg,', '1', '3', '2018-09-30 15:25:03');
INSERT INTO `product_info` VALUES ('30', '哇哈哈', '30', '12312312312313', '20', 'C:\\fakepath\\wahaha1.jpg,C:\\fakepath\\wahaha2.jpg,C:\\fakepath\\wahaha3.jpg,', '1', '3', '2018-09-30 15:25:31');
INSERT INTO `product_info` VALUES ('33', '喜之郎', '50', 'xixixxixixixixixixixixixixixxiixixixx', '20', 'C:\\fakepath\\xizhilang1.jpg,C:\\fakepath\\xizhilang2.jpg,C:\\fakepath\\xizhilang3.jpg,', '1', '3', '2018-09-30 15:26:09');
INSERT INTO `product_info` VALUES ('36', '小布丁', '50', 'xixixxioadsifsidfsifsdfasd', '20', 'C:\\fakepath\\xiaobuding1.jpg,C:\\fakepath\\xiaobuding2.jpg,C:\\fakepath\\xiaobuding3.jpg,', '1', '3', '2018-09-30 15:26:32');
INSERT INTO `product_info` VALUES ('45', '王老吉', '20', '1524245545sdfsd', '30', 'C:\\fakepath\\wanglaoji1.jpg,C:\\fakepath\\wanglaoji2.jpg,C:\\fakepath\\wanglaoji3.jpg,', '1', '3', '2018-09-30 15:26:53');
INSERT INTO `product_info` VALUES ('63', 'iphonex', '500000', '21341564613', '10', 'C:\\fakepath\\iphonex1.jpg,C:\\fakepath\\iphonex2.jpg,C:\\fakepath\\iphonex3.jpg,', '1', '3', '2018-09-30 15:27:14');
INSERT INTO `product_info` VALUES ('65', 'iphone6', '500000', 'sasdfasdfsadfasd', '10', 'C:\\fakepath\\iphone61.jpg,C:\\fakepath\\iphone62.jpg,C:\\fakepath\\iphone63.jpg,', '1', '3', '2018-09-30 15:28:12');
INSERT INTO `product_info` VALUES ('85', 'iphone7s', '500000', 'aksjdbjksbfkjsdbfasj', '10', 'C:\\fakepath\\iphone7s1.jpg,C:\\fakepath\\iphone7s2.jpg,C:\\fakepath\\iphone7s3.jpg,', '1', '3', '2018-09-30 15:28:31');
INSERT INTO `product_info` VALUES ('86', 'iphone6s', '300000', '123132132132', '10', 'C:\\fakepath\\iphone6s1.jpg,C:\\fakepath\\iphone6s2.jpg,C:\\fakepath\\iphone6s3.jpg,', '1', '3', '2018-09-30 15:28:57');
INSERT INTO `product_info` VALUES ('87', 'ipad', '30', 'qqqqqqqqqqqqq', '1', 'C:\\fakepath\\ipad1.jpg,C:\\fakepath\\ipad2.jpg,C:\\fakepath\\ipad3.jpg,', '1', '3', '2018-09-30 15:29:42');

-- ----------------------------
-- Table structure for `product_partition`
-- ----------------------------
DROP TABLE IF EXISTS `product_partition`;
CREATE TABLE `product_partition` (
  `product_partition_id` int(11) NOT NULL AUTO_INCREMENT,
  `partition_region` varchar(255) DEFAULT NULL COMMENT '0：新人专区 1：邀请专区 2：推荐专区 3：签到专区',
  PRIMARY KEY (`product_partition_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_partition
-- ----------------------------

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('4', '老板', '2018-09-30 10:56:58');
INSERT INTO `role` VALUES ('3', '管理员', '2018-09-30 10:08:05');
INSERT INTO `role` VALUES ('5', '被剥削的员工1号', '2018-09-30 11:01:07');
INSERT INTO `role` VALUES ('6', '被剥削的员工2号', '2018-09-30 11:03:51');

-- ----------------------------
-- Table structure for `sign_in_record`
-- ----------------------------
DROP TABLE IF EXISTS `sign_in_record`;
CREATE TABLE `sign_in_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sign_in_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `sign_in_day` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sign_in_record
-- ----------------------------

-- ----------------------------
-- Table structure for `step_details`
-- ----------------------------
DROP TABLE IF EXISTS `step_details`;
CREATE TABLE `step_details` (
  `step_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL COMMENT '0：邀请 1：签到 2：活动 3 ：微信步数',
  `step_num` int(11) DEFAULT NULL,
  `step_time` date NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`step_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of step_details
-- ----------------------------
INSERT INTO `step_details` VALUES ('1', '3', '2000', '2018-09-27', '13');
INSERT INTO `step_details` VALUES ('4', '3', '2000', '2018-09-28', '13');

-- ----------------------------
-- Table structure for `sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '上级部门ID，一级部门为0',
  `parent_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '部门名称',
  `oeder_num` int(11) DEFAULT NULL,
  `del_flag` tinyint(4) NOT NULL COMMENT '是否删除 -1：已删除 0 ：正常',
  PRIMARY KEY (`dept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '商品上架', '/commodity_shelves', '2', '5');
INSERT INTO `sys_menu` VALUES ('3', '用户信息管理', '/', '1', '0');
INSERT INTO `sys_menu` VALUES ('4', '用户列表', '/User', '2', '3');
INSERT INTO `sys_menu` VALUES ('5', '商品管理信息', '/', '1', '0');
INSERT INTO `sys_menu` VALUES ('6', '跳转增加', '/adminrole-add', '1', '0');
INSERT INTO `sys_menu` VALUES ('7', '活动模块', '/', '1', '0');
INSERT INTO `sys_menu` VALUES ('9', '管理员设置', '/', '1', '0');
INSERT INTO `sys_menu` VALUES ('10', '权限管理', '/admin_permission', '2', '9');
INSERT INTO `sys_menu` VALUES ('11', '管理员列表', '/SysUser_list', '2', '9');
INSERT INTO `sys_menu` VALUES ('12', '抽奖记录', '/LotteryRecord_list', '2', '7');
INSERT INTO `sys_menu` VALUES ('13', '步数明细', '/selectLits', '2', '3');
INSERT INTO `sys_menu` VALUES ('14', '货币兑换记录', '/virtual_currency', '2', '3');
INSERT INTO `sys_menu` VALUES ('15', '角色管理', '/listRole', '2', '9');
INSERT INTO `sys_menu` VALUES ('16', '批量删除', '/deleteMoreSysMenu', '3', '10');
INSERT INTO `sys_menu` VALUES ('17', '进入角色修改', '/goto_editRole', '3', '15');
INSERT INTO `sys_menu` VALUES ('18', '删除角色', '/deleteRole', '3', '15');
INSERT INTO `sys_menu` VALUES ('19', '修改角色', '/updateRole', '3', '15');
INSERT INTO `sys_menu` VALUES ('20', '批量删除角色', '/deledata', '3', '15');
INSERT INTO `sys_menu` VALUES ('21', '跳转添加权限', '/admin-permission-add', '3', '10');
INSERT INTO `sys_menu` VALUES ('22', '新增角色', '/addRole', '3', '15');
INSERT INTO `sys_menu` VALUES ('23', '添加权限确认', '/addSysMenu', '3', '10');
INSERT INTO `sys_menu` VALUES ('24', '删除权限', '/deleteSysMenu', '3', '10');
INSERT INTO `sys_menu` VALUES ('25', '商品下架', '/comodity_frame', '2', '5');
INSERT INTO `sys_menu` VALUES ('26', '商品分类', '/product_category', '2', '5');
INSERT INTO `sys_menu` VALUES ('27', '修改管理员状态', '/updateStatus', '3', '11');
INSERT INTO `sys_menu` VALUES ('28', '新增管理员', '/addUser', '3', '11');
INSERT INTO `sys_menu` VALUES ('29', '修改管理员信息', '/updateUserById', '3', '11');
INSERT INTO `sys_menu` VALUES ('30', '删除管理员', '/deleteUser', '3', '11');
INSERT INTO `sys_menu` VALUES ('31', '查询管理员', '/realTimeQuery', '3', '11');
INSERT INTO `sys_menu` VALUES ('32', '批量删除', '/deleteMoreUser', '3', '11');
INSERT INTO `sys_menu` VALUES ('33', '提交上架', '/addProduct', '3', '1');
INSERT INTO `sys_menu` VALUES ('34', '下架单个商品', '/deleteProduct', '3', '25');
INSERT INTO `sys_menu` VALUES ('35', '下架多个商品', '/deleteProducts', '3', '25');
INSERT INTO `sys_menu` VALUES ('36', '修改商品', '/updateProduct', '3', '25');
INSERT INTO `sys_menu` VALUES ('37', '查找分区', '/selectRegion', '3', '26');

-- ----------------------------
-- Table structure for `sys_menu_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_role`;
CREATE TABLE `sys_menu_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu_role
-- ----------------------------
INSERT INTO `sys_menu_role` VALUES ('19', '3', '4');
INSERT INTO `sys_menu_role` VALUES ('5', '5', '3');
INSERT INTO `sys_menu_role` VALUES ('4', '3', '3');
INSERT INTO `sys_menu_role` VALUES ('6', '6', '3');
INSERT INTO `sys_menu_role` VALUES ('7', '7', '3');
INSERT INTO `sys_menu_role` VALUES ('8', '9', '3');
INSERT INTO `sys_menu_role` VALUES ('9', '1', '3');
INSERT INTO `sys_menu_role` VALUES ('10', '4', '3');
INSERT INTO `sys_menu_role` VALUES ('11', '10', '3');
INSERT INTO `sys_menu_role` VALUES ('12', '11', '3');
INSERT INTO `sys_menu_role` VALUES ('13', '12', '3');
INSERT INTO `sys_menu_role` VALUES ('14', '13', '3');
INSERT INTO `sys_menu_role` VALUES ('15', '14', '3');
INSERT INTO `sys_menu_role` VALUES ('16', '15', '3');
INSERT INTO `sys_menu_role` VALUES ('17', '25', '3');
INSERT INTO `sys_menu_role` VALUES ('18', '26', '3');
INSERT INTO `sys_menu_role` VALUES ('20', '5', '4');
INSERT INTO `sys_menu_role` VALUES ('21', '6', '4');
INSERT INTO `sys_menu_role` VALUES ('22', '7', '4');
INSERT INTO `sys_menu_role` VALUES ('23', '9', '4');
INSERT INTO `sys_menu_role` VALUES ('24', '1', '4');
INSERT INTO `sys_menu_role` VALUES ('25', '4', '4');
INSERT INTO `sys_menu_role` VALUES ('26', '10', '4');
INSERT INTO `sys_menu_role` VALUES ('27', '11', '4');
INSERT INTO `sys_menu_role` VALUES ('28', '12', '4');
INSERT INTO `sys_menu_role` VALUES ('29', '13', '4');
INSERT INTO `sys_menu_role` VALUES ('30', '14', '4');
INSERT INTO `sys_menu_role` VALUES ('31', '15', '4');
INSERT INTO `sys_menu_role` VALUES ('32', '25', '4');
INSERT INTO `sys_menu_role` VALUES ('33', '26', '4');
INSERT INTO `sys_menu_role` VALUES ('34', '3', '5');
INSERT INTO `sys_menu_role` VALUES ('35', '4', '5');
INSERT INTO `sys_menu_role` VALUES ('36', '13', '5');
INSERT INTO `sys_menu_role` VALUES ('37', '14', '5');
INSERT INTO `sys_menu_role` VALUES ('38', '5', '6');
INSERT INTO `sys_menu_role` VALUES ('39', '1', '6');
INSERT INTO `sys_menu_role` VALUES ('40', '25', '6');
INSERT INTO `sys_menu_role` VALUES ('41', '26', '6');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) NOT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '0：禁用  1：正常',
  `dept_id` bigint(20) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'nbgYr0DpdB8=', '240@qq.com', '123215125', '1', '3', '2018-09-13 10:16:20');
INSERT INTO `sys_user` VALUES ('2', 'nidaye', 'hx9U7Ck4HVM=', '65423154214@qq.com', '18302876052', '1', '3', '2018-09-28 17:47:54');

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1', '3');

-- ----------------------------
-- Table structure for `userinfo`
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `open_id` varchar(255) DEFAULT NULL,
  `balance` int(11) DEFAULT NULL,
  `inviter_open_id` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userinfo
-- ----------------------------
INSERT INTO `userinfo` VALUES ('1', '123', '0', '654654', '2018-09-21 17:50:13');
INSERT INTO `userinfo` VALUES ('10', 'bcEo8xLf8dptPeRI0rTRxA==', '0', '456', '2018-09-26 17:41:33');
INSERT INTO `userinfo` VALUES ('11', 'TF3aCT0RObJRSZCdIMK+Mg==', '0', '456', '2018-09-26 17:51:26');
INSERT INTO `userinfo` VALUES ('9', 'mv0KSItIKIkoa788II6+8Q==', '0', '456', '2018-09-25 16:56:40');
INSERT INTO `userinfo` VALUES ('12', 'S85PXWV7VIfgIAhlQes2HA==', '0', '456', '2018-09-26 17:53:43');
INSERT INTO `userinfo` VALUES ('13', 'o_XnH5QVD9NZ0I183xIemhspra-M', '107', '456', '2018-09-26 17:55:38');
INSERT INTO `userinfo` VALUES ('14', 'undefined', '0', '456', '2018-09-27 15:10:29');
INSERT INTO `userinfo` VALUES ('15', 'o_XnH5d7WZyKusUv7uPnPr4LXnjs', '0', '456', '2018-09-30 14:30:41');

-- ----------------------------
-- Table structure for `user_address`
-- ----------------------------
DROP TABLE IF EXISTS `user_address`;
CREATE TABLE `user_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1：默认地址 2:其他地址',
  `user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`address_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_address
-- ----------------------------

-- ----------------------------
-- Table structure for `virtual_currency`
-- ----------------------------
DROP TABLE IF EXISTS `virtual_currency`;
CREATE TABLE `virtual_currency` (
  `virtual_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `cash` int(11) DEFAULT NULL,
  `cash_time` datetime NOT NULL,
  `cash_step` int(11) DEFAULT NULL,
  PRIMARY KEY (`virtual_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of virtual_currency
-- ----------------------------
