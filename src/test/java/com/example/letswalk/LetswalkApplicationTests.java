package com.example.letswalk;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LetswalkApplicationTests {

	@Test
	public void contextLoads() throws URISyntaxException {
		String classPath=this.getClass().getResource("/static/picture/").getPath();
		URI uri = this.getClass().getResource("/static/picture/").toURI();
		Path path = Paths.get(uri).toAbsolutePath();
		System.out.println(classPath.substring(classPath.indexOf("/")));
	}

}
