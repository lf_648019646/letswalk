package com.example.letswalk;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class TimeTest {
    public static void main(String[] args) {
        System.out.println(LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli());
        System.out.println( LocalDate.now().atStartOfDay().toInstant(ZoneOffset.of("+8")).toEpochMilli());
    }
}
